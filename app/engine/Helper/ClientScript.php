<?php
namespace Engine\Helper;

class ClientScript{
    /**
     * The script is rendered in the head section right before the title element.
     */
    const POS_HEAD=0;
    /**
     * The script is rendered at the beginning of the body section.
     */
    const POS_BEGIN=1;
    /**
     * The script is rendered at the end of the body section.
     */
    const POS_END=2;

    /**
     * @var integer Where the scripts registered using {@link registerScript} will be inserted in the page.
     * This can be one of the CClientScript::POS_* constants.
     * Defaults to CClientScript::POS_READY.
     * @since 1.1.11
     */
    public $defaultScriptPosition=self::POS_END;

    /**
     * @var array the registered JavaScript code blocks (position, key => code)
     */
    public $scripts=array();

    /**
     * Registers a piece of javascript code.
     * @param string $id ID that uniquely identifies this piece of JavaScript code
     * @param string $script the javascript code
     * @param integer $position the position of the JavaScript code. Valid values include the following:
     * <ul>
     * <li>CClientScript::POS_HEAD : the script is inserted in the head section right before the title element.</li>
     * <li>CClientScript::POS_BEGIN : the script is inserted at the beginning of the body section.</li>
     * <li>CClientScript::POS_END : the script is inserted at the end of the body section.</li>
     * </ul>
     * @return CClientScript the CClientScript object itself (to support method chaining, available since version 1.1.5).
     */
    public function registerScript($id,$script,$position=null)
    {
        if($position===null)
            $position=$this->defaultScriptPosition;

        $this->scripts[$position][$id]=$script;
        return $this;
    }

    public function render($position){
        if (!isset($this->scripts[$position])) return;
        
        $scripts = $this->scripts[$position];
        $output = "";
        foreach($scripts as $script){
            $output .= $script . "\n";
        }
        return '<script>' . $output . '</script>';
    }
}
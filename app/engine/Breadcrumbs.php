<?php
/**
 * Created by PhpStorm.
 * User: Khanh
 * Date: 6/6/14
 * Time: 3:56 PM
 */

namespace Engine;


use Phalcon\DI;
use Phalcon\Mvc\Url;
use Phalcon\Tag;

class Breadcrumbs {

    /**
     * @var string the tag name for the breadcrumbs container tag. Defaults to 'div'.
     */
    public $tagName='ul';
    /**
     * @var array the HTML attributes for the breadcrumbs container tag.
     */
    public $htmlOptions=array('class'=>'page-breadcrumb breadcrumb');
    /**
     * @var boolean whether to HTML encode the link labels. Defaults to true.
     */
    public $encodeLabel=true;

    /**
     * @var array list of hyperlinks to appear in the breadcrumbs. If this property is empty,
     * the widget will not render anything. Each key-value pair in the array
     * will be used to generate a hyperlink by calling CHtml::link(key, value). For this reason, the key
     * refers to the label of the link while the value can be a string or an array (used to
     * create a URL). For more details, please refer to {@link CHtml::link}.
     * If an element's key is an integer, it means the element will be rendered as a label only (meaning the current page).
     *
     * The following example will generate breadcrumbs as "Home > Sample post > Edit", where "Home" points to the homepage,
     * "Sample post" points to the "index.php?r=post/view&id=12" page, and "Edit" is a label. Note that the "Home" link
     * is specified via {@link homeLink} separately.
     *
     * <pre>
     * array(
     *     'Sample post'=>array('post/view', 'id'=>12),
     *     'Edit',
     * )
     * </pre>
     */
    public $links=array();

    /**
     * @var array
     */
    public $actions=array();

    /**
     * @var string String, specifies how each active item is rendered. Defaults to
     * "<a href="{url}">{label}</a>", where "{label}" will be replaced by the corresponding item
     * label while "{url}" will be replaced by the URL of the item.
     * @since 1.1.11
     */
    public $activeLinkTemplate='<li>{prepend}<a href="{url}">{label}</a>{append}</li>';
    /**
     * @var string String, specifies how each inactive item is rendered. Defaults to
     * "<span>{label}</span>", where "{label}" will be replaced by the corresponding item label.
     * Note that inactive template does not have "{url}" parameter.
     * @since 1.1.11
     */
    public $inactiveLinkTemplate='<li><span>{label}</span></li>';
    /**
     * @var string the separator between links in the breadcrumbs. Defaults to ' &raquo; '.
     */
    public $separator='<i class="fa fa-angle-right"></i>';

    public function addParts($links) {
        $this->links = array_merge($this->links, $links);
    }
    /**
     * Renders the content of the portlet.
     */
    public function render()
    {
        if(empty($this->links))
            return;

        echo Tag::tagHtml($this->tagName,$this->htmlOptions)."\n";

        // actions links
        if (count($this->actions)) {
            $buttonsGroup = new ButtonsGroup(DI::getDefault());
            $buttonsGroup
                ->setListClass('btn-group btn-group-solid')
                ->setListTag('li')
                ->setListItemTag('a')
                ->setListItemClass('btn')
            ;

            $buttonsGroup->setItems($this->actions);
            echo $buttonsGroup->render();
        }

        // breadcrumbs links
        $links=array();

        $urlHelper = DI::getDefault()->getShared('url');

        $count = count($this->links);
        $i = 0;
        foreach($this->links as $label=>$url)
        {
            if(is_string($label) || is_array($url)) {
                $routeLink = array('for' => $url[0]);
                $prepend = $url[1];
                if (count($url) > 2) {
                    unset($url[0]);
                    unset($url[1]);
                    foreach($url as $key => $val) {
                        $routeLink[$key] = $val;
                    }
                }
                $params = array(
                    '{url}' => $urlHelper->get($routeLink),
                    '{label}' => $this->encodeLabel ? $label : $label,
                    '{prepend}' => '',
                    '{append}' => '',
                );
                if ($prepend) {
                    $params['{prepend}'] = '<i class="'.$prepend.'"></i>';
                }
                // this is not clear enough because embedding separator in <li> tag
                if ($i < $count - 1 && $this->separator) {
                    $params['{append}'] = $this->separator;
                }
                $links[] = strtr($this->activeLinkTemplate, $params);
            }
            else {
                // still a bug that do not add appender
                $links[] = str_replace('{label}', $this->encodeLabel ? $url : $url, $this->inactiveLinkTemplate);
            }

            $i++;
        }
        echo implode('', $links);

        echo Tag::tagHtmlClose($this->tagName);
    }
} 
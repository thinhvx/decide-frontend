<?php
/**
 * Created by PhpStorm.
 * User: thinhvoxuan
 * Date: 9/19/14
 * Time: 3:05 PM
 */

namespace Engine\Db\Model\BehaviorEvent;

use Engine\Exception;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;

class AbstractModelBehavior extends Behavior implements BehaviorInterface
{
    /**
     * This method receives the notifications from the EventsManager
     *
     * @param string $type
     * @param \Phalcon\Mvc\ModelInterface $model
     */
    public function notify($type, $model)
    {
        switch ($type) {
            /*
             * Event Create an Model
             */
            case 'beforeValidation':
                break;
            case 'beforeValidationOnCreate':
                break;
            case 'validation':
                break;
            case 'afterValidationOnCreate':
                break;
            case 'afterValidation':
                break;
            case 'beforeSave':
                break;
            case 'beforeCreate':
                break;
            case 'afterCreate':
                break;
            case 'afterSave':
                break;
            /*
             * Event Update an Model
             */
//            case 'beforeValidation':
//                break;
            case 'beforeValidationOnUpdate':
                break;
//            case 'validation':
//                break;
            case 'afterValidationOnUpdate':
                break;
//            case 'afterValidation';
//                break;
//            case 'beforeSave':
//                break;
            case 'beforeUpdate':
                break;
            case 'afterUpdate':
                break;
//            case 'afterSave':
//                break;
            /*
             * Event Read an Model
             */
            case '':
                break;
            /*
             * Event Delete an Model
             */
            case 'beforeDelete':
                break;
            case 'afterDelete':
                break;
            default:
        }
    }

    public function missingMethod($model, $method, $arguments=array()){
        if (method_exists($this, $method)){
            $arg = array_merge(array($model), $arguments);
            $r = call_user_func_array(array($this, $method), $arg);
            return $r;
        }
        throw new Exception('Do not have this function');
    }
}

<?php
/*
 +------------------------------------------------------------------------+
 | PhalconEye CMS                                                         |
 +------------------------------------------------------------------------+
 | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
 +------------------------------------------------------------------------+
 | This source file is subject to the New BSD License that is bundled     |
 | with this package in the file LICENSE.txt.                             |
 |                                                                        |
 | If you did not receive a copy of the license and are unable to         |
 | obtain it through the world-wide-web, please send an email             |
 | to license@phalconeye.com so we can send you a copy immediately.       |
 +------------------------------------------------------------------------+
 | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
 +------------------------------------------------------------------------+
*/

namespace Engine\Console\Command;

use Engine\Console\AbstractCommand;
use Engine\Console\CommandInterface;
use Engine\Console\ConsoleUtil;
use Phalcon\DI;
use Engine\Acl\Adapter\Database as AclDBAdapter;
use Phalcon\Acl as PhalconAcl;
use Core\Api\Acl as AclApi;

/**
 * Cache command.
 *
 * @CommandName(['acl'])
 * @CommandDescription('Acl management.')
 */
class Acl extends AbstractCommand implements CommandInterface
{

    protected $_acl;


    protected static $_roles = array(
        AclApi::DEFAULT_ROLE_GUEST => null,
        AclApi::DEFAULT_ROLE_USER => AclApi::DEFAULT_ROLE_GUEST,
        AclApi::DEFAULT_ROLE_MEMBER => AclApi::DEFAULT_ROLE_USER,
        AclApi::DEFAULT_ROLE_CONTENT_MANAGER => AclApi::DEFAULT_ROLE_USER,
        AclApi::DEFAULT_ROLE_ADMIN => null
    );

    /**
     * Rebuild ACL.
     *
     * @return void
     */
    public function rebuildAction()
    {
        $connection = $this->getDI()->getDb();
        $acl = new AclDBAdapter(array(
            'db' => $connection,
            'roles' => 'roles',
            'rolesInherits' => 'roles_inherits',
            'resources' => 'resources',
            'resourcesAccesses' => 'resources_accesses',
            'accessList' => 'access_list',
        ));

        $this->truncatedACLTable($connection);

        $acl->setDefaultAction(PhalconAcl::DENY);
        $this->_acl = $acl;

        $this->_addBaseRoles();

        $this->_addResources('Controller');
        $this->_addResources('Model');
    }


    protected function truncatedACLTable($connection){
        $connection->execute('DELETE FROM resources_accesses');
        $connection->execute('DELETE FROM resources');
        $connection->execute('DELETE FROM access_list');
        $connection->execute("INSERT INTO access_list VALUES('Admin', '*', '*', 1)");
    }
    /**
     * Scan all files from $from, add all resources, roles, access list from them
     *
     * @param $from
     */
    protected function _addResources($from)
    {
        $registry = $this->getDI()->get('registry');
        foreach ($registry->modules as $module) {
            $module = ucfirst($module);
            $modelsPath = $registry->directories->modules . $module . '/'.$from;
            if (file_exists($modelsPath)) {
                $files = scandir($modelsPath);
                foreach ($files as $file) {
                    if ($file == "." || $file == "..") {
                        continue;
                    }
                    if (!preg_match('/.*'.$from.'\.php$/', $file)) {
                        continue;
                    }
                    $class = sprintf('\%s\\'.$from.'\%s', $module, ucfirst(str_replace('.php', '', $file)));
                    $this->getObject($class);
                }
            }
        }
    }

    /**
     * Add default roles
     * $roles = array(
     *      role => baseRole
     * )
     */
    protected function _addBaseRoles() {
        foreach (self::$_roles as $role => $baseRole) {
            if (!empty($baseRole)) {
                if (strpos($baseRole, ',') !== FALSE) {
                    $baseRole = str_replace(' ', '', $baseRole);
                    $baseRoles = explode(',', $baseRole);
                    $this->_acl->addRole(new PhalconAcl\Role($role, $role));
                    foreach ($baseRoles as $baseRole) {
                        $this->_acl->addRole(new PhalconAcl\Role($baseRole, $baseRole));
                        $this->_acl->addInherit($role, $baseRole);
                    }
                } else {
                    $this->_acl->addRole(new PhalconAcl\Role($baseRole, $baseRole));
                    $this->_acl->addRole(new PhalconAcl\Role($role, $role), $baseRole);
                }
            } else {
                $this->_acl->addRole(new PhalconAcl\Role($role, $role));
            }
        }
    }

    /**
     * Add all resource from $objectName, where has @Acl annotations
     * Add all roles from $objectName, where @Acl annotations has 'roles' key
     *
     * @param $objectName
     */
    public function getObject($objectName)
    {
        $reader = new \Phalcon\Annotations\Adapter\Memory();
        $reflector = $reader->get($objectName);
        $methodAnnotations = $reflector->getMethodsAnnotations();

        foreach ($methodAnnotations as $method => $methodAnnotation) {
            if ($methodAnnotation && $methodAnnotation->has('Acl')) {

                $this->_acl->addResource($objectName, $method);
                $aclAnotation = $methodAnnotation->get('Acl');

                if ($aclAnotation->hasNamedArgument('roles')) {
                    $roles = $aclAnotation->getNamedArgument('roles');
                    $roles = str_replace(' ', '', $roles);
                    $roles = explode(',', $roles);

                    foreach ($roles as $role) {
                        if (strpos($role, '_') !== FALSE) {
                            $splittedRoleName = explode('_', $role);
                            $role = '';
                            foreach ($splittedRoleName as $elem) {
                                $role = $role.ucfirst($elem).'_';
                            }
                            $role = substr($role, 0, strlen($role) - 1);
                        } else {
                            $role = ucfirst($role);
                        }

                        if (!array_key_exists(strtolower($role), self::$_roles)){
                            print ConsoleUtil::error('Roles' . $role . '" doesn\'t exists.');
                            print ConsoleUtil::error('From object: '. $objectName);
                            die();
                        }

                        $this->_acl->allow($role, $objectName, $method);
                    }
                }
            }
        }
    }
}
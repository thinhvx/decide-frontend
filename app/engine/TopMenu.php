<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace Engine;

use Engine\Behaviour\DIBehaviour;
use Phalcon\DI;
use Phalcon\DiInterface;
use User\Model\User;

/**
 * Navigation.
 *
 * @category  PhalconEye
 * @package   Engine
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
class TopMenu
{
    use DIBehaviour {
        DIBehaviour::__construct as protected __DIConstruct;
    }

    protected $_topMenuTag = 'ul';
    protected $_topMenuCls = 'nav navbar-nav pull-right';

    protected $_dropdownUserTag = 'li';
    protected $_dropdownUserCls = 'dropdown dropdown-user';

    protected $_dropdownNotificationTag = 'li';
    protected $_dropdownNotificationCls = 'dropdown dropdown-extended dropdown-notification';

    protected $_menuDividerTag = '<li class="divider"></li>';

    protected $_userItems = array();
    protected $_notificationItems = array();

    /**
     * Navigation constructor.
     *
     * @param DiInterface $di Dependency injection.
     */
    public function __construct($di = null)
    {
        $this->__DIConstruct($di);
        $this->_activeItem = substr($this->getDI()->get('request')->get('_url'), 1);
    }

    /**
     * Render navigation.
     *
     * @return string
     */
    public function render()
    {
        $content = '';

        // short names
        $tmt = $this->_topMenuTag;
        $tmc = $this->_topMenuCls;

        $content = "<{$tmt} class='{$tmc}'>";
        $content .= $this->_renderNotificationItems();
        $content .= $this->_renderUserItems();
        $content .= "</{$tmt}>";

        return $content;
    }

    protected function _renderUserItems() {
        $lit = $this->_dropdownUserTag;
        $lic = $this->_dropdownUserCls;
        $content = "<$lit class='$lic'>";
        $content .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">';
        $content .= '   <img alt="" class="img-circle" height="24" src="/metronic/admin/layout/img/avatar3_small.jpg">';
        $content .= '   <span class="username">' . User::getViewer()->username . ' </span>';
        $content .= '   <i class="fa fa-angle-down"></i>';
        $content .= '</a>';
        $content .= '<ul class="dropdown-menu">';
        foreach($this->_userItems as $item) {
            if ($item === '-') {
                $content .= $this->_menuDividerTag;
            } else {
                $content .= sprintf('<li><a href="%s">', $item['href']);
                if (isset($item['prepend'])) {
                    $content .= $item['prepend'];
                }
                $content .= $item['text'];
                if (isset($item['append'])) {
                    $content .= $item['append'];
                }

                $content .= '</a></li>';
            }
        }
        $content .= "</ul>";
        $content .= "</$lit>";

        return $content;
    }

    protected function _renderNotificationItems() {
        $lit = $this->_dropdownNotificationTag;
        $lic = $this->_dropdownNotificationCls;
        $content = "<$lit class='$lic'>";
        $content .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">';
        $content .= '   <i class="fa fa-envelope"></i>';
        $content .= '   <span class="badge badge-default">4 </span>';
        $content .= '</a>';
        $content .= '<ul class="dropdown-menu extended">';
        $content .= sprintf('<li><p>You have %d new notifications</p></li>', count($this->_notificationItems));
        $content .= '    <li><ul class="dropdown-menu-list scroller" style="height: 250px;">';
        foreach($this->_notificationItems as $item) {
            if ($item === '-') {
                $content .= $this->_menuDividerTag;
            } else {
                $content .= sprintf('<li><a href="%s">', isset($item['href']) ? $item['href'] : '#');
                $content .= sprintf('   <span class="label label-sm label-icon label-%s">', isset($item['severity']) ? $item['severity'] : 'danger');
                if (isset($item['prepend'])) {
                    $content .= $item['prepend'];
                }
                $content .= '   </span>';
                $content .= $item['text'];
                if (isset($item['append'])) {
                    $content .= ' ' . $item['append'];
                }
                $content .= '</a></li>';
            }
        }
        $content .= '    </ul></li>';
        $content .= '    <li class="external"><a href="#">See all notifications <i class="m-icon-swapright"></i></a></li>';
        $content .= "</ul>";
        $content .= "</$lit>";

        return $content;
    }

    /**
     * @param array $userItems
     */
    public function setUserItems($userItems)
    {
        $this->_userItems = $userItems;
    }

    /**
     * @return array
     */
    public function getUserItems()
    {
        return $this->_userItems;
    }

    /**
     * @param array $notificationItems
     */
    public function setNotificationItems($notificationItems)
    {
        $this->_notificationItems = $notificationItems;
    }

    /**
     * @return array
     */
    public function getNotificationItems()
    {
        return $this->_notificationItems;
    }

}
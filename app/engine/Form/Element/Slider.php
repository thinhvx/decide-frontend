<?php
namespace Engine\Form\Element;

use Engine\Form\AbstractElement;
use Engine\Form\ElementInterface;

class Slider extends AbstractElement implements ElementInterface{
    public function getIdForSlider(){
        $name = $this->getName();
        $id = 'id_'.$name;
        return $id;
    }

    public function getIdForHiddenField(){
        $id = $this->getIdForSlider();
        return 'hidden_'.$id;
    }

    public function getNameForHiddenField(){
        return $this->getName();
    }

    public function getIdForValueText(){
        $id = $this->getIdForSlider();
        return 'text_'.$id;
    }

    public function getHtmlTemplate(){
        $output = <<<EOD
            <div id="{$this->getIdForSlider()}" class="slider bg-green">
											</div>
											<div class="slider-value col-md-4">
												 <span id="{$this->getIdForValueText()}">
												</span>
											</div>
            <input type="hidden" id="{$this->getIdForHiddenField()}" name="{$this->getNameForHiddenField()}" value="{$this->getValue()}" />

EOD;
        return $output;
    }

    public function render(){
        $asset = $this->getDI()->getAssets();
        //$asset->addCss('external/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.css');
        $asset->addJs('external/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js');

        $cs = $this->getDI()->get('clientScript');
        $value = $this->getValue();
        if (empty($value)) $value = \Sourcing\Model\Job::KEYWORD_MUST_HAVE;

        $script = <<<EOD
            $("#{$this->getIdForSlider()}").slider({
                isRTL: Metronic.isRTL(),
                value: {$value},
                min: 0,
                max: 2,
                step: 1,
                slide: function (event, ui) {
                    $("#{$this->getIdForHiddenField()}").val(ui.value);
                    switch (ui.value){
                        case 0:
                            text = 'Must Have';
                            break;
                        case 1:
                            text = 'Should Have';
                            break;
                        case 2:
                            text = 'Nice To Have';
                            break;
                    }
                    $("#{$this->getIdForValueText()}").html(text);
                }
            });
EOD;

        $cs->registerScript($this->getIdForSlider(), $script);

        return sprintf($this->getHtmlTemplate());
    }
}
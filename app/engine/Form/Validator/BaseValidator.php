<?php
/**
 * Created by PhpStorm.
 * User: thinhvoxuan
 * Date: 9/16/14
 * Time: 2:22 PM
 */

namespace Engine\Form\Validator;

use Phalcon\Validation;
use Phalcon\Validation\Message\Group;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;

class BaseValidator extends Validator implements ValidatorInterface {


    /**
     * Current validation object.
     *
     * @var Validation
     */
    public  $_currentValidator;

    /**
     * Current field name.
     *
     * @var string
     */
    public $_currentAttribute;

    /**
     * Executes the validation
     *
     * @param \Phalcon\Validator $validator
     * @param string $attribute
     * @return boolean
     */
    public function validate($validator, $attribute){
        $this->_currentValidator = $validator;
        $this->_currentAttribute = $attribute;
    }

    /**
     * Add error message.
     *
     * @param string     $msg  Message text.
     * @param array|null $args Message params.
     *
     * @return void
     */
    protected function _addMessage($msg, $args = null)
    {
        $this->_currentValidator->appendMessage(
            new Validation\Message(vsprintf($msg, $args), $this->_currentAttribute)
        );
    }
} 
<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace Engine\Form;

use Engine\Form\Behaviour\ContainerBehaviour;
use Engine\Form\Behaviour\ActionsBarBehaviour;
use Engine\Form;
use Phalcon\Validation\Message\Group;


/**
 * ActionsBar class.
 *
 * @category  PhalconEye
 * @package   Engine\Form
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
class ActionsBar implements ElementContainerInterface
{
    use ContainerBehaviour;

    /**
     * @var bool
     */
    protected $_isOnTop = false;
    /**
     * Fieldset attributes.
     *
     * @var array
     */
    protected $_attributes = [];

    /**
     * Combine elements in one container.
     *
     * @var bool
     */
    protected $_combineElements = false;

    /**
     * Check that all elements inside this fieldset must be named (attr 'id') according to it's fieldset.
     *
     * @var bool
     */
    protected $_namedElements = false;

    /**
     * Check that all elements inside this fieldset must be related to this fieldset (name="fieldSet[fieldName]").
     *
     * @var bool
     */
    protected $_dataElements = false;

    /**
     * Create fieldset.
     *
     * @param string $name ActionsBar name.
     * @param string $legend ActionsBar legend.
     * @param array $attributes ActionsBar attributes.
     * @param array $elements ActionsBar elements.
     */
    public function __construct($isTop = false, array $attributes = [], array $elements = [])
    {
        $this->__DIConstruct();

        $this->setIsOnTop($isTop);
        $this->_elements = $elements;
        $this->_validation = new Validation($this);

        $this->_errors = new Group();
        $this->_notices = new Group();

        if (method_exists($this, 'initialize')) {
            $this->initialize();
        }
    }

    /**
     * Returns the attributes for the element.
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->_attributes;
    }

    /**
     * Sets the element attribute.
     *
     * @param string $name Attribute name.
     * @param string $value Attribute value.
     *
     * @return $this
     */
    public function setAttribute($name, $value)
    {
        $this->_attributes[$name] = $value;
        return $this;
    }

    /**
     * Returns the form attribute.
     *
     * @param string $name Attribute name.
     *
     * @return string|null
     */
    public function getAttribute($name)
    {
        if (!isset($this->_attributes[$name])) {
            return null;
        }

        return $this->_attributes[$name];
    }

    /**
     * Combine elements in one container.
     *
     * @param bool $flag Combination flag.
     *
     * @return $this
     */
    public function combineElements($flag = true)
    {
        $this->_combineElements = $flag;
        return $this;
    }

    /**
     * Check if this fieldset must be combined in one container.
     *
     * @return bool
     */
    public function isCombined()
    {
        return $this->_combineElements;
    }

    /**
     * Enable named elements.
     *
     * @param bool $flag Flag.
     *
     * @return $this
     */
    public function enableNamedElements($flag = true)
    {
        $this->_namedElements = $flag;
        return $this;
    }

    /**
     * Enable data elements.
     *
     * @param bool $flag Flag.
     *
     * @return $this
     */
    public function enableDataElements($flag = true)
    {
        $this->_dataElements = $flag;
        return $this;
    }

    /**
     * Render fieldset attributes.
     *
     * @return string
     */
    public function renderAttributes()
    {
        $clz = $this->getAttribute('class');
        if ($clz == null) {
            $clz = '';
        }
        $clz .= 'form-actions';

        if ($this->getIsOnTop()) {
            $clz .= ' top';
        }
        $this->setAttribute('class', $clz);

        $html = '';
        foreach ($this->_attributes as $key => $attribute) {
            $html .= sprintf(' %s="%s"', $key, $attribute);
        }

        return $html;
    }

    /**
     * @param boolean $isOnTop
     */
    public function setIsOnTop($isOnTop)
    {
        $this->_isOnTop = $isOnTop;
    }

    /**
     * @return boolean
     */
    public function getIsOnTop()
    {
        return $this->_isOnTop;
    }
}
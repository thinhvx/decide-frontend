<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Zztest\Controller;

use Core\Controller\AbstractController;
use Zztest\Model\TestModel;

/**
 * Index controller.
 *
 * @category PhalconEye\Module
 * @package  Controller
 *
     * @RoutePrefix("/zztests", name="zztests-controller")
 */
class IndexController extends AbstractController
{
    /**
     * Module index action.
     *
     * @return void
     *
     * @Route("/", methods={"GET"}, name="zztests-index")
     * @Acl(roles="Guest")
     */
    public function indexAction()
    {
        $es = $this->di->getEs();
//        var_dump($es->ping());
//        die();
        $params['index'] = 'book';
        $params['type'] = 'tiki';
        $params['body'] = [
            'query' => [
                'match_all' => []
            ],
        ];
        $result = $es->search($params);
//        var_dump($result);
        foreach($result['hits']['hits'] as $row){
            var_dump($row);
        }
        die();
    }
}

<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]--> <head> <title>{{ helper('setting',
'core').get('system_title', '') }} | {% if pageTitle is defined %} {{ pageTitle
| i18n }} {% endif %}</title> <meta charset="utf-8"> <meta name="viewport"
content="width=device-width, initial-scale=1.0"> <link href="favicon.ico"
rel="shortcut icon" type="image/x-icon"/>

	{{ assets.outputCss() }}

	{{ assets.outputInline() }}

    <script type="text/javascript">
        {{ helper('i18n', 'core').render() }}
    </script>

    {%- block head -%}

    {%- endblock -%}

</head>
<body data-base-url="{{ url() }}" data-debug="{{ config.application.debug }}" class="page-header-fixed">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="{{ url() }}" class="site-logo">
                    <div class="logo">
                        <img  src="/assets/img/core/pe_logo.png" class="logo-img"/>
                    </div>
                <div class="site-name">
                    <p class="name"><span style="color: #014e7c">GEEK</span> <span style="color: #67db3c">Hub</span></p>
                </div>
            </a>

            <div class="menu-toggler sidebar-toggler hide">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN TOP NAVIGATION MENU -->
         <div class="top-menu">
             <ul class="nav navbar-nav pull-right">
                 {{ helper('renderer', 'base').renderWidgetByName('communication', 'headerNotification') }}
             </ul>
                 {#{{ topMenu.render() }}#}
         </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        {% if (sideBar is defined) %}
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->
                {{ sideBar.render() }}
                <!-- END SIDEBAR MENU -->
            </div>
        {% endif %}
    </div>
    <!-- END SIDEBAR -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12" id="alert-wrapper"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    {% if pageTitle is defined %}
                        <h3 class="page-title">
                            {{ pageTitle | i18n }}
                        </h3>
                    {% else %}

                    {% endif %}
                    {% if breadcrumbs is defined %}
                        {{ breadcrumbs.render() }}
                    {% endif %}
                    <!-- END PAGE TITLE & BREADCRUMB-->
                    {{ flashSession.output() }}
                </div>
            </div>
            <!-- BEGIN CONTENT -->
            {% block content %}{% endblock %}
            <!-- END CONTENT -->
        </div>
    </div>
</div>
<!-- END PAGE CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        2014 © GEEKHub.vn
    </div>
    <div class="page-footer-tools">
        <span class="go-top"><i class="fa fa-angle-up"></i></span>
    </div>
</div>
<!-- END FOOTER -->

{{ assets.outputJs() }}

<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
    });
</script>

{#{{ partial("../../Core/View/layouts/alert", ['messages': flashSession.getMessages()]) }}#}

</body>
</html>

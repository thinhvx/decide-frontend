<div class="note  note-{{ education.getStatusColor() }}">
    <h4 class="title">
        {{ education.school_display_name }}
        <div class="pull-right">
            <button
                    class="{{ education.getStatusColor() }} btn-link editEducation"
                    data-pid="{{ education.id }}"
                    data-url-edit="{{ url(['for': 'utopia-people-education-edit', 'id': people.id, 'educationId': education.id]) }}"
                    >
                <strong><i class="fa fa-edit"></i> Edit</strong>
            </button>
            <button class="btn-link danger"><i class="fa fa-minus-circle"></i> Delete</button> |
            <a href="#educationPanel-{{ education.id }}" class="btn-link sortHandler" title="{{ 'Drag to sort' | i18n }}"><i class="fa fa-slack"></i>{{ education.sort_index }}</a>
        </div>
    </h4>

    <p class="company">
        <strong>{{ education.degree }}</strong> {% if education.field_of_study is not null %}, {% endif %}{{ education.field_of_study }} <br/>
        <span class="text-muted">{{ education.start_year }} - {{ education.end_year }}</span>
    </p>

    <div class="description">
        {% if education.description_html is null %}
            {{ education.description  }}
        {% else %}
            {{ education.description_html}}
        {% endif %}
    </div>

    <p class="actions">

    </p>
</div>
<script type="application/javascript">
    {% if customeJs is defined %}
        {{ customeJs }}
    {% endif %}
</script>
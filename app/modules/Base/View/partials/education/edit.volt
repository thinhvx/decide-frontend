<div class="education-form">
    {{ form.render() }}
</div>
<script type="text/javascript">
    var panelId = '#educationPanel-{{ education.id }}';
    {% if education.school_id is null %}
        var select2 = $(panelId + ' :input[name=school_id]').data('select2');
        select2.externalSearch('{{ education.school_display_name | escape }}');
    {% endif %}
</script>
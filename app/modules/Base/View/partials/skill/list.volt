<ol class="list-inline" style="font-size: 12pt; line-height: 28px;">
    {% for item in skillItems %}
    <li id="skillItem-{{ item.id }}" style="padding: 5px;">
        <a class="btn btn-default green-stripe">
            {{ item.skill }}
        </a>
        {#{{ partial('partials/skill/view', ['item': item, 'people': people]) }}#}
    </li>
{% endfor %}
<div class="note  note-{{ position.getStatusColor() }}">
    <h4 class="title">
        {{ position.title }}
        <div class="pull-right">
            <button
                    class="{{ position.getStatusColor() }} btn-link editPosition"
                    data-pid="{{ position.id }}"
                    data-url-edit="{{ url(['for': 'utopia-people-position-edit', 'id': people.id, 'positionId': position.id]) }}"
                    >
                <strong><i class="fa fa-edit"></i> Edit</strong>
            </button>
            <button class="btn-link danger"><i class="fa fa-minus-circle"></i> Delete</button> |
            <a href="#positionPanel-{{ position.id }}" class="btn-link sortHandler" title="{{ 'Drag to sort' | i18n }}"><i class="fa fa-slack"></i>{{ position.sort_index }}</a>
        </div>
    </h4>

    <p class="company">
        <strong>{{ position.company_display_name }}</strong> <br/>
        <span class="text-muted">{{ position.getFormattedPeriodFrom() }} - {{ position.getFormattedPeriodTo() }} ({{ position.getWorkingPeriod() }})</span>
    </p>

    <div class="description">
        {% if position.description_html is null %}
            {{ position.description  }}
        {% else %}
            {{ position.description_html}}
        {% endif %}
    </div>

    <p class="actions">

    </p>
</div>
<script type="application/javascript">
    {% if customeJs is defined %}
        {{ customeJs }}
    {% endif %}
</script>
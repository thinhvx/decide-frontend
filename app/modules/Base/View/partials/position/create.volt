<div class="position-form">
    {{ form.render() }}
</div>
<script type="text/javascript">
    var panelId = '#{{ panelId }}';
    $(panelId + ' :checkbox[name=is_working_here]').change(function() {
        if (this.checked) {
            $(panelId).find(':input[name=to_month], :input[name=to_year]').attr('disabled', 'disabled');
        } else {
            $(panelId).find(':input[name=to_month], :input[name=to_year]').removeAttr('disabled');
        }
    }).trigger('change');
</script>
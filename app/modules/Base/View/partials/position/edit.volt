<div class="position-form">
    {{ form.render() }}
</div>
<script type="text/javascript">
    var panelId = '#positionPanel-{{ position.id }}';
    console.log(panelId);
    {% if position.company_id is null %}
        var select2 = $(panelId + ' :input[name=company_id]').data('select2');
        select2.externalSearch('{{ position.company_display_name | escape }}');
    {% endif %}
    $(panelId + ' :checkbox[name=is_working_here]').change(function() {
        if (this.checked) {
            $(panelId).find(':input[name=to_month], :input[name=to_year]').attr('disabled', 'disabled');
        } else {
            $(panelId).find(':input[name=to_month], :input[name=to_year]').removeAttr('disabled');
        }
    }).trigger('change');
</script>
{#
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
#}

{{ form.openTag() }}
<div class="portlet box blue {% if form.getAttribute('is_horizontal') %} form-horizontal {% endif %}">
    <div class="portlet-title">
        {% if form.getTitle()%}
            <div class="caption">
                {% if form.getAttribute('icon') %}<i class="{{ form.getAttribute('icon') }}"></i>{% endif %} {{ form.getTitle() }}
            </div>
        {% endif %}
    </div>
    <div class="portlet-body form">
        <div class="form-body">
            {% if form.getDescription() %}
                <h4 class="margin-bottom-20">{{ form.getDescription() }}</h4>
                <hr/>
            {% endif %}

            {{ partial(form.getErrorsView(), ['form': form]) }}
            {{ partial(form.getNoticesView(), ['form': form]) }}

            {% for element in form.getAll() %}
                {{ partial(form.getElementView(), ['element': element]) }}
            {% endfor %}
        </div>

        {% for element in form.getActionButtons().getAll() %}
            {{ partial(form.getActionsBarView(), ['bar': element]) }}
        {% endfor %}

        <div class="clear"></div>

        {% if form.useToken() %}
            <input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}">
        {% endif %}
    </div>
</div>
{{ form.closeTag() }}
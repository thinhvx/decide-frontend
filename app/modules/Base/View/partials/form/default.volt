<div class="box
    {% if form.getAttribute('withoutPortlet') is null %} portlet {% endif %}
    {% if form.getAttribute('color') is not null %} {{ form.getAttribute('color') }} {% else %} blue {% endif %}
    {% if form.getAttribute('is_horizontal') %} form-horizontal {% endif %}
">
    {#{% if form.getAttribute('hasTitle') and form.getAttribute('hasTitle') is false %}#}
    {% if form.getTitle()%}
    <div class="portlet-title">
            <div class="caption  {% if form.getAttribute('title_attribute') %} {{ form.getAttribute('title_attribute')  }} {% endif %}  "
                    {% if form.getAttribute('title_id') %} id="{{ form.getAttribute('title_id') }}" {% endif %}
                    >
                {% if form.getIcon() %}<i class="{{ form.getIcon() }}
                "></i>{% endif %} {{ form.getTitle() }}
            </div>
        {#{% endif %}#}
    </div>
    {% endif %}
    <div class="portlet-body form">
        {{ form.openTag() }}

        <div class="form-body">
            {% if form.getDescription() %}
                <h4 class="margin-bottom-20">{{ form.getDescription() }}</h4>
                <hr/>
            {% endif %}

            {% if form.getAttribute("show_total_errors") and form.getAttribute("show_total_errors") is true  %}
                {{ partial(form.getErrorsView(), ['form': form]) }}
            {% endif %}
            {{ partial(form.getErrorsView(), ['form': form]) }}
            {{ partial(form.getNoticesView(), ['form': form]) }}
            {% for element in form.getAll() %}
                {{ partial(form.getElementView(), ['element': element]) }}
            {% endfor %}
        </div>


        {% if form.getActionButtons().getAll() %}
            <div class="{% if form.getAttribute('cssFormAction') %}{{ form.getAttribute('cssFormAction') }}{% else %}form-actions{% endif %}">
                {% for button in form.getActionButtons().getAll() %}
                    {{ partial(form.getElementView(), ['element': button]) }}
                {% endfor %}
            </div>
        {% endif %}

        <div class="clear"></div>

        {% if form.useToken() %}
            <input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}">
        {% endif %}
        {{ form.closeTag() }}
    </div>
</div>
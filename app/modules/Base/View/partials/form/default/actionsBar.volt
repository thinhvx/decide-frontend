<div class="form-actions">
{% for element in bar.getElements() %}
    {{ partial(form.getElementView(), ['element': element]) }}
{% endfor %}
</div>
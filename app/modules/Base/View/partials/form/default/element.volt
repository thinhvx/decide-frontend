{% if instanceof(element, 'Engine\Form\FieldSet') %}
    {{ partial(form.getFieldSetView(), ['fieldSet': element]) }}
{% else %}
    {% if element.useDefaultLayout() %}

        {% if combined is not defined or not combined %}
            {#<div class="form_element_container{% if form.hasErrors(element.getName()) %} validation_failed{% endif %}">#}
        {% endif %}

        <div class="form-group {% if form.hasErrors(element.getName()) %} has-error {% endif %}">
                {% if !form.hideLabel() %}
                    {% if element.getOption('label') %}
                    <label for="{{ element.getName() }}" class="{% if form.isHorizontal() %}col-lg-3 col-md-3 {% endif %}control-label">
                        {{ element.getOption('label') |i18n }}
                        {% if element.getOption('required') %}
                            <span class="required" aria-required="true">*</span>
                        {% endif %}
                    </label>
                {% endif %}
            {% endif %}
            {% if form.isHorizontal() %}
                <div class="col-sm-10 col-lg-8">
            {% endif %}
            {% if instanceof(element, 'Engine\Form\Element\File') and element.getOption('isImage') and element.getValue() is not '/' %}
                <div class="form_element_file_image">
                    <img alt="" src="{{ element.getValue() }}"/>
                </div>
            {% endif %}
            {#{% if form.hasErrors(element.getName()) %}#}
                {#<span class="help-block"><i class="fa fa-warning"></i> {{ form.getErrors(element.getName())[0]|i18n }}</span>#}
            {#{% endif %}#}
            {#//==============#}
            {% if form.hasErrors(element.getName()) %}
                {% set errorValue = form.getErrors(element.getName())[0] %}
                <div class="input-icon right">
                    <i class="fa fa-warning tooltips" data-original-title="{{ errorValue.getMessage() | i18n }}"></i>
                    {{ element.render() }}
                </div>
            {% else %}
                {{ element.render() }}
            {% endif %}

            {#//==============#}
            {#{{ element.render() }}#}
            {% if element.getOption('description') %}
                <span class="help-block"><i class="fa fa-info-circle"></i> {{ element.getOption('description') |i18n }}</span>
            {% endif %}
            {% if form.isHorizontal() %}
                </div>
            {% endif %}
        </div>

        {% if combined is not defined or not combined %}
            {#</div>#}
        {% endif %}

    {% else %}
        {{ element.render() }}
    {% endif %}
{% endif %}

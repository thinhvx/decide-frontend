{#
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
#}

{% if instanceof(element, 'Engine\Form\FieldSet') %}
    {{ partial(form.getFieldSetView(), ['fieldSet': element]) }}
{% else %}
    {% if element.useDefaultLayout() %}

        {% if combined is not defined or not combined %}
            {#<div class="form_element_container{% if form.hasErrors(element.getName()) %} validation_failed{% endif %}">#}
        {% endif %}

        <div class="form-group {% if form.hasErrors(element.getName()) %} has-error {% endif %}">
            {% if element.getOption('label') %}
                <label for="{{ element.getName() }}" class="{% if form.isHorizontal() %}col-sm-3 {% endif %}control-label">
                    {{ element.getOption('label') |i18n }}
                    {% if element.getOption('required') %}
                        <span class="required" aria-required="true">*</span>
                    {% endif %}:
                </label>
            {% endif %}
            {% if form.isHorizontal() %}
                <div class="col-sm-9">
            {% endif %}
                {% if instanceof(element, 'Engine\Form\Element\File') and element.getOption('isImage') and element.getValue() is not '/' %}
                    <div class="form_element_file_image">
                        <img alt="" src="{{ element.getValue() }}"/>
                    </div>
                {% elseif instanceof(element, 'Engine\Form\Element\ListGroups') %}
                    {{ element.render() }}
                {% else %}
                    <p class="form-control-static">{{ element.getValue() }}</p>
                {% endif %}
    {% if form.isHorizontal() %}
                </div>
            {% endif %}
        </div>

        {% if combined is not defined or not combined %}
            {#</div>#}
        {% endif %}

    {% else %}
        {{ element.render() }}
    {% endif %}
{% endif %}

<!-- Begin Summary -->
<h3 class="form-section">{{ 'Summary' | i18n }}</h3>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="javascript;" data-widget="editable" data-trigger-for="#summary"><i
                            class="fa fa-pencil"></i></a>
                                    <span id="summary" data-widget="editable" data-type="textarea" data-pk="1"
                                          data-title="Enter summary" data-pk="1" data-url="/post"
                                          data-emptytext="No Summary" data-toggle="manual"
                                          data-placement="right">{{ people.getFullName() }}</span>
            </div>
        </div>
    </div>
</div>
<!-- End Summary -->
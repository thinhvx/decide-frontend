<div class="form-body">
    <h3 class="form-section">Person Info</h3>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-3">First Name:</label>

                <div class="col-md-9">
                    <p class="form-control-static" lingdex="10">
                        {{ people.first_name }}
                    </p>
                </div>
            </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-3">Last Name:</label>

                <div class="col-md-9">
                    <p class="form-control-static" lingdex="11">
                        {{ people.last_name }}
                    </p>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>
    <!--/row-->
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-3">{{ 'Email' | i18n }}:</label>

                <div class="col-md-9">
                    <p class="form-control-static" lingdex="12">
                        {{ people.email }}
                    </p>
                </div>
            </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-3">Email 2:</label>

                <div class="col-md-9">
                    <p class="form-control-static" lingdex="13">
                        {{ people.email2 }}
                    </p>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>
    <!--/row-->
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-3">Mobile phone:</label>

                <div class="col-md-9">
                    <p class="form-control-static" lingdex="14">
                        {{ people.mobile }}
                    </p>
                </div>
            </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-3">Home phone:</label>

                <div class="col-md-9">
                    <p class="form-control-static" lingdex="15">
                        {{ people.phone }}
                    </p>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>
    <!--/row-->
    <h3 class="form-section">Address</h3>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-3">Address:</label>

                <div class="col-md-9">
                    <p class="form-control-static" lingdex="16">
                        {{ people.address }}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-3">City:</label>

                <div class="col-md-9">
                    <p class="form-control-static" lingdex="17">
                        {{ people.city }}
                    </p>
                </div>
            </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-3">State:</label>

                <div class="col-md-9">
                    <p class="form-control-static" lingdex="18">
                        {{ people.state }}
                    </p>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>
    <!--/row-->
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-3">Post Code:</label>

                <div class="col-md-9">
                    <p class="form-control-static" lingdex="19">
                        {{ people.zip_code }}
                    </p>
                </div>
            </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-3">Country:</label>

                <div class="col-md-9">
                    <p class="form-control-static" lingdex="20">
                        {{ people.country_name }}
                    </p>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>
</div>
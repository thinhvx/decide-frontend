<?php
namespace Base\Plugin;
use Engine\Sidebar;
use Phalcon\Mvc\User\Plugin as PhalconPlugin;

class SidebarMenu extends PhalconPlugin {
    public function sidebar($event, Sidebar $sideBar) {
        $sideBar->addItem(200, [
            'name' => 'dashboard',
            'title' => 'Dashboard',
            'prepend' => '<i class="fa fa-dashboard"></i>',
            'href' => 'manager',
        ]);
    }
}
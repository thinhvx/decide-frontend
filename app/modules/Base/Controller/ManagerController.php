<?php
/**
 * Created by PhpStorm.
 * User: ngtrieuvi92
 * Date: 9/18/14
 * Time: 9:07 AM
 */
namespace Base\Controller;

/**
 *  Dashboard for manager page
 *
 * @RoutePrefix("/manager", name="manager")
 */
class ManagerController extends  AdminBaseController{

    /**
     * Index action
     *
     * @Route('/', method={"GET","POST"}, name="dashboard")
     * @Acl(roles="Content_Manager")
     *
     */
    public function indexAction(){
    }
}



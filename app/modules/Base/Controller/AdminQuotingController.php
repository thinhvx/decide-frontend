<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Base\Controller;

use Base\Controller\Grid\Admin\QuotingGrid;
use Base\Form\Admin\Quoting\Create;
use Base\Model\Quoting;
use Blog\Controller\Grid\Admin\PostGrid;
use Blog\Controller\Grid\Admin\NewPostsGrid;
use Blog\Form\Admin\Post\Edit as EditForm;
use Blog\Model\Comment;
use Blog\Model\Post;
use Core\Controller\AbstractAdminController;
use Engine\Navigation;

/**
 * Index controller.
 *
 * @category PhalconEye\Module
 * @package  Controller
 *
 * @RoutePrefix("/manager/module/base", name="admin-quoting")
 */
class AdminQuotingController extends AbstractAdminController
{

    /**
     * Init navigation.
     *
     * @return void
     */
    public function init()
    {
        $navigation = new Navigation();
        $navigation
            ->setItems(
                [
                    'index' => [
                        'href' => 'admin/module/base',
                        'title' => 'List Quoting',
                        'prepend' => '<i class="glyphicon glyphicon-list"></i>'
                    ],
                    1 => [
                        'href' => 'javascript:;',
                        'title' => '|'
                    ],
					'new-posts' => [
						'href' => 'admin/module/base/create',
						'title' => 'Add Quoting'
					]
                ]
            );

        $this->view->navigation = $navigation;
    }

    /**
     * Module index action.
     *
     * @return void
     *
     * @Route("/", methods={"GET"}, name="admin-quoting")
     */
    public function indexAction()
    {
        $grid = new QuotingGrid($this->view);
        if ($response = $grid->getResponse()) {
            return $response;
        }
    }

    /**
     *
     * @return void
     *
     * @Route("/create", methods={"GET", "POST"}, name="admin-quoting-create")
     */
    public function createAction()
    {

        $form = new Create();
		
        $this->view->form = $form;
		
        if (!$this->request->isPost() || !$form->isValid(null,true)) {
            return;
        }
		
        $page = $form->getEntity();

        if(!$page->save()){
            foreach($page->getMessages() as $m){
                $form->addError($form);
            }
            $this->view->form = $form;
            return;
        }

        $this->flashSession->success('New quoting created successfully!');

        return $this->response->redirect(['for' => "admin-quoting"]);
    }

    /**
     * Module index action.
     *
     * @return void
     *
    @Route("/{id:[0-9]+}/edit", methods={"GET", "POST"}, name="admin-quoting-edit")
     */
    public function editAction($id) {

        $page = Quoting::findFirstById($id);

        if (!$page || (!empty($page->type) && $page->type != Page::PAGE_TYPE_HOME)) {
            $this->flashSession->notice('Nothing to edit!');
            return $this->response->redirect(['for' => "admin-quoting"]);
        }

        $form = new \Base\Form\Admin\Quoting\Edit($page);
        $this->view->form = $form;

        if (!$this->request->isPost() || !$form->isValid(null, true)) {
            return;
        }

        $data = $form->getValues();

        if(!$page->save($data)){
            foreach($page->getMessages() as $m){
                $form->addError($form);
            }
            $this->view->form = $form;
            return;
        }
        $this->flashSession->success('Object saved!');

        return $this->response->redirect(['for' => "admin-quoting"]);
    }

    /**
     * Module index action.
     *
     * @return void
     *
     * @Route("/{id:[0-9]+}/delete", methods={"GET", "POST"}, name="admin-quoting-delete")
     */
    public function deleteAction($id) {
		$item = Quoting::findFirst($id);
        if ($item) {
            $item->status = Quoting::STATUS_DELETED;

            if ($item->save()) {

                $this->flashSession->notice('Quoting item has been deleted!');
            } else {
                $this->flashSession->error($item->getMessages());
            }
        }

        return $this->response->redirect(['for' => 'admin-quoting']);
    }
}

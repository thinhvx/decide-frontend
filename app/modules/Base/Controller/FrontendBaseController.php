<?php
/**
 * Created by PhpStorm.
 * User: ngtrieuvi92
 * Date: 9/10/14
 * Time: 1:57 PM
 */
namespace Base\Controller;

use Core\Controller\AbstractController;
use Engine\Asset\Manager as AssetManager;
use Core\Controller\Traits\JsTranslations;
use Engine\Breadcrumbs;

/**
 * Base controller.
 *
 * @category  GEEK Up
 * @package   Base\Controller
 *
 */
abstract class FrontendBaseController extends AbstractController
{
    use JsTranslations;

    /**
     * Initialize admin specific logic.
     *
     * @return void
     */
    public function initialize()
    {
        if ($this->request->isAjax()) {
            return;
        }

        $this->_setupNavigation();
        $this->_setupAssets();
    }

    /**
     * After route execution.
     *
     * @return void
     */
    public function afterExecuteRoute()
    {
        if ($this->di->has('profiler')) {
            $this->profiler->stop(get_called_class(), 'controller');
        }
    }


    /**
     * Disable header rendering.
     *
     * @return $this
     */
    public function disableHeader()
    {
        $this->view->disableHeader = true;

        return $this;
    }

    /**
     * Disable footer rendering.
     *
     * @return $this
     */
    public function disableFooter()
    {
        $this->view->disableFooter = true;

        return $this;
    }

    /**
     * Resolve modal window result.
     *
     * @param array $params Modal params.
     *
     * @return void
     */
    public function resolveModal(array $params = [])
    {
        if (empty($params)) {
            $params['hide'] = true;
        }

        $this->view->setVars($params);
        $this->view->hideSave = true;
        $this->view->pick('utils/modal', 'core');
    }

    /**
     * Setup assets.
     *
     * @return void
     */
    protected function _setupAssets()
    {
        $this->assets->set(
            AssetManager::DEFAULT_COLLECTION_CSS,
            $this->assets->getEmptyCssCollection()
                //Add Metronic Theme
//                ->addCss('metronic/font/font-open-san.css')

                ->addCss('metronic/global/plugins/font-awesome/css/font-awesome.min.css')
                ->addCss('metronic/global/plugins/bootstrap/css/bootstrap.min.css')
                ->addCss('metronic/global/css/components.css')
                ->addCss('metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')
                ->addCss('metronic/global/plugins/fancybox/source/jquery.fancybox.css')

                ->addCss('metronic/frontend/layout/css/style.css')
                ->addCss('metronic/frontend/pages/css/style-shop.css')
                ->addCss('metronic/frontend/pages/css/style-layer-slider.css')
                ->addCss('metronic/frontend/layout/css/style-responsive.css')
                ->addCss('metronic/frontend/layout/css/themes/green.css')

                //Add global custom css
                ->addCss('assets/css/core/layout.css')
                ->join(false)
        );

        $this->assets->set(
            AssetManager::DEFAULT_COLLECTION_JS,
            $this->assets->getEmptyJsCollection()

                /*
                 * Begin jquery
                 */
                ->addJs('external/jquery/jquery-2.1.0.js')
                ->addJs('metronic/global/plugins/jquery-1.11.0.min.js')
                ->addJs('metronic/global/plugins/jquery-migrate-1.2.1.min.js')
                ->addJs('external/jquery/jquery-ui-1.10.4.js')
                ->addJs('external/jquery/jquery.cookie.js')
                ->addJs('external/3dot/jquery.dotdotdot.min.js')

                /*
                 * end jquery - being metronic js
                 */
                ->addJs('metronic/global/plugins/bootstrap/js/bootstrap.min.js')
                ->addJs('metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'  )
                ->addJs('metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js'  )

                ->addJs('metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js')

                ->addJs('metronic/frontend/layout/scripts/back-to-top.js'  )
                ->addJs('metronic/frontend/layout/scripts/layout.js'  )


                //Defaut css of phalcon eye
                ->addJs('assets/js/core/core.js')
                ->addJs('assets/js/core/i18n.js')
                ->addJs('assets/js/core/form.js')
                ->addJs('assets/js/core/form/remote-file.js')

                //Init JS script
                ->addJs('js/app.js')

                ->addJs('assets/js/base/subscriber.js')
                ->join(false)
        );

        if ($this->di->has('profiler')) {
            $this->di->get('assets')
                ->collection(AssetManager::DEFAULT_COLLECTION_CSS)
                ->addCss('assets/css/core/profiler.css');

            $this->di->get('assets')
                ->collection(AssetManager::DEFAULT_COLLECTION_JS)
                ->addCss('assets/js/core/profiler.js');
        }

        $this->addDefaultJsTranslations();
    }

    /**
     * Setup navigation.
     *
     * @return void
     */
    protected function _setupNavigation()
    {
//        $this->_setupTopMenu();

        $url = $this->getDI()->getShared('url');
        $this->view->breadcrumbs = new Breadcrumbs();
        $this->view->breadcrumbs->links = array(
            'Home' => array('home', 'fa fa-home'),
        );
    }


    public function createLink($route, $params = array())
    {
        $url = $this->url;
        $query = $this->request->getQuery();
        if (isset($query['_url'])) {
            unset($query['_url']);
        }
        foreach ($params as $k => $p) {
            $query[$k] = $p;
        }

        return $url->get($route, $query) . (empty($query) ? '?' : '');
    }

    protected function _setupTopMenu()
    {
        $userMenuItems = [
            [
                'href' => '',
                'text' => 'My Tasks',
                'icon' => 'fa-tasks',
                'appendText' => '7',
                'appendCls' => 'badge badge-success'
            ],
            '-',
            [
                'href' => '/logout',
                'text' => 'Logout',
                'icon' => 'fa-key'
            ]
        ];
        $topMenu = new TopMenu();
        $topMenu->setUserItems($userMenuItems);

        $this->view->topMenu = $topMenu;
    }
}
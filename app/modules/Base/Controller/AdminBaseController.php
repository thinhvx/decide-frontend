<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace Base\Controller;

use Core\Controller\AbstractController;
use Core\Controller\Traits\JsTranslations;
use Engine\Asset\Manager as AssetManager;
use Engine\Behaviour\DIBehaviour;
use Engine\Breadcrumbs;
use Engine\Sidebar;
use Engine\TopMenu;
use Phalcon\DI;
use Phalcon\Mvc\View;
use User\Model\User;

/**
 * Base controller.
 *
 * @category  GEEK Up
 * @package   Base\Controller
 *
 */
abstract class AdminBaseController extends AbstractController
{
    use JsTranslations;

    /**
     * Initialize admin specific logic.
     *
     * @return void
     */
    public function initialize()
    {
        if ($this->request->isAjax()) {
            return;
        }

        $this->_setupNavigation();
        $this->_setupAssets();

        $this->view->pageTitle = "Manager Panel";
    }

    /**
     * After route execution.
     *
     * @return void
     */
    public function afterExecuteRoute()
    {
        if ($this->di->has('profiler')) {
            $this->profiler->stop(get_called_class(), 'controller');
        }
    }


    /**
     * Disable header rendering.
     *
     * @return $this
     */
    public function disableHeader()
    {
        $this->view->disableHeader = true;

        return $this;
    }

    /**
     * Disable footer rendering.
     *
     * @return $this
     */
    public function disableFooter()
    {
        $this->view->disableFooter = true;

        return $this;
    }

    /**
     * Resolve modal window result.
     *
     * @param array $params Modal params.
     *
     * @return void
     */
    public function resolveModal(array $params = [])
    {
        if (empty($params)) {
            $params['hide'] = true;
        }

        $this->view->setVars($params, false);
        $this->view->hideSave = true;
        $this->view->pick('utils/modal', 'core');
    }

    /**
     * Setup assets.
     *
     * @return void
     */
    protected function _setupAssets()
    {

        $this->assets->set(
            AssetManager::DEFAULT_COLLECTION_CSS,
            $this->assets->getEmptyCssCollection()
                // BEGIN GLOBAL MANDATORY STYLES
                ->addCss('css/font-open-san.css')
                ->addCss('external/chosen/public/vendor/chosen/chosen.min.css')
                ->addCss('metronic/global/plugins/font-awesome/css/font-awesome.min.css')
                ->addCss('metronic/global/plugins/bootstrap/css/bootstrap.min.css')
                ->addCss('metronic/global/plugins/uniform/css/uniform.default.css')
                // END GLOBAL MANDATORY STYLES
                ->addCss('metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')
                ->addCss('metronic/global/plugins/select2/select2.css')
                // BEGIN THEME STYLES
                ->addCss('metronic/global/css/components.css')
                ->addCss('metronic/global/css/plugins.css')
                ->addCss('metronic/admin/layout/css/layout.css')
                ->addCss('metronic/admin/layout/css/themes/darkblue.css')
                ->addCss('metronic/admin/layout/css/custom.css')
                ->addCss('metronic/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.css')
                ->addCss('assets/css/base/header.css')
        // END THEME STYLES
        );

        $this->assets->set(
            AssetManager::DEFAULT_COLLECTION_JS,
            $this->assets->getEmptyJsCollection()
                ->addJs('metronic/global/plugins/jquery-1.11.0.min.js')
                ->addJs('metronic/global/plugins/jquery-migrate-1.2.1.min.js')
//                ->addJs('metronic/global/plugins/can.min.js')
                ->addJs('metronic/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js')
                ->addJs('metronic/global/plugins/bootstrap/js/bootstrap.js')
                ->addJs('metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')
                ->addJs('metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.js')
                ->addJs('metronic/global/plugins/jquery.blockui.min.js')
                ->addJs('metronic/global/plugins/jquery.cokie.min.js')
//                ->addJs('metronic/global/plugins/bootstrap-dual-listbox/jquery.bootstrap-duallistbox.js')
//                ->addJs('metronic/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js')
                ->addJs('metronic/global/plugins/uniform/jquery.uniform.min.js')
                ->addJs('metronic/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js')
                ->addJs('metronic/global/plugins/bootstrap-markdown/lib/markdown.js')

                ->addJs('metronic/global/scripts/metronic.js')
                ->addJs('metronic/admin/layout/scripts/layout.js')

                // PhalconEye javascripts
                ->addJs('assets/js/core/core.js')
                ->addJs('assets/js/core/i18n.js')
                ->addJs('assets/js/core/form.js')
                ->addJs('external/ckeditor/ckeditor.js')
                ->addJs('assets/js/core/widgets/ckeditor.js')
                ->addJs('assets/js/core/widgets/grid.js')
                ->addJs('assets/js/core/widgets/datepicker.js')
//                ->addJs('assets/js/core/widgets/markdown.js')
//                ->addJs('assets/js/core/widgets/modal.js')
//                ->addJs('assets/js/core/widgets/dual-listbox.js')
//                ->addJs('assets/js/core/widgets/editable.js')
//                ->addJs('assets/js/core/widgets/easy-pie-chart.js')
                //Add some other jquery plugin
                ->addJs('external/chosen/public/vendor/chosen/chosen.jquery.min.js')
                ->addJs('external/chosen/public/vendor/chosen/chosen.proto.min.js')
        // Postal
//				->addJs('assets/js/lodash.js')
//				->addJs('assets/js/conduit.js')
//				->addJs('assets/js/postal.js')
//				->addJs('assets/js/subscriptions/alert.js')
        );


        if ($this->di->has('profiler')) {
            $this->di->get('assets')
                ->collection('css')
                ->addCss('assets/css/core/profiler.css');

            $this->di->get('assets')
                ->collection('js')
                ->addCss('assets/js/core/profiler.js');
        }

        $this->addDefaultJsTranslations();
    }

    /**
     * Setup navigation.
     *
     * @return void
     */
    protected function _setupNavigation()
    {
        $this->_setupTopMenu();
        $this->_setupSidebar();

        $url = $this->getDI()->getShared('url');
        $this->view->breadcrumbs = new Breadcrumbs();
        $this->view->breadcrumbs->links = array(
            'Home' => array('home', 'fa fa-home'),
        );
    }

    protected function _setupSidebar() {

        $path = explode('/', $this->request->get('_url'));

        $activeItem = '';
        $limit = (count($path) > 3 ? 1 : 0);
        for ($i = 1, $count = count($path); $i < $count - $limit && $i < 3; $i++) {
            $activeItem .= $path[$i] . '/';
        }
        $activeItem = substr($activeItem, 0, -1);

        $sideBar = new Sidebar();
        $this->getDI()->get('eventsManager')->fire('navigation:sidebar', $sideBar);

        $role = User::getViewer()->getRole();

        $this->view->sideBar = $sideBar;
    }

    public function createLink($route, $params=array()){
        $url = $this->url;
        $query = $this->request->getQuery();
        if (isset($query['_url'])){
            unset($query['_url']);
        }
        foreach($params as $k=>$p){
            $query[$k] = $p;
        }

        return $url->get($route, $query) . (empty($query) ? '?' : '');
    }

    protected function _setupTopMenu()
    {
        $userMenuItems = [
            [
                'href' => '',
                'text' => 'My Tasks',
                'icon' => 'fa-tasks',
                'appendText' => '7',
                'appendCls' => 'badge badge-success'
            ],
            '-',
            [
                'href' => '/logout',
                'text' => 'Logout',
                'icon' => 'fa-key'
            ]
        ];

        $topMenu = new TopMenu();
        $topMenu->setUserItems($userMenuItems);

        $this->view->topMenu = $topMenu;
    }
}
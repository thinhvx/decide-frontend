/**
 * Created by thinhvoxuan on 10/29/14.
 */
function isValidEmail(email) {
    return /^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/.test(email)
        && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(email);
}

function onSubmitNewsletter() {
    var email = $("input[name=subscribe-newsletter]").val();
    if (!isValidEmail(email)) {
        $("#input-email").addClass('form-email-error');
        return;
    }
    if (email != "") {
        $.ajax({
            url: "{{ url(['for': 'subscribe']) }}",
            method: 'POST',
            data: {
                email: email
            },
            success: function (res) {

                $("input[name=subscribe-newsletter]").val("");
                //@TODO: show message success or failed here
                console.log(res);
                var response = $.parseJSON(res);
                if (parseInt(response['success']) == 1) {
                    $('#span-subcribe').click();
                    $("#input-email").removeClass('form-email-error');
                } else if (parseInt(response['success']) == 2) {
                    $('#span-error').click();
                    $("#input-email").removeClass('form-email-error');
                } else {
                    $("#input-email").addClass('form-email-error');
                }

            }
        })
    }
}
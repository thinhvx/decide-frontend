(function (window, $, root) {
    root.ns(
        'PhalconEye.widget.inputTags',
        {
            /**
             * Init input tags.
             *
             * @param item input for init
             */
            init: function (item) {

                var data = item.data('data');
                var extend = item.data('extend');

                var categories = [];
                if (data) {
                    data = data.split(",");

                    for (var i = 0; i < data.length; i += 2) {
                        categories.push({ text: data[i + 1], value: data[i]});
                    }

                }

                item.selectize({
                    plugins: ['remove_button'],
                    persist: false,
                    create: extend,
                    options: categories
                });
            }
        }
    );
}(window, jQuery, PhalconEye));
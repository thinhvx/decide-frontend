/**
 * MediumEditor widget.
 *
 */
(function (window, $, root, undefined) {
    root.ns(
        'PhalconEye.widget.medium-editor',
        {
            /**
             * Init MediumEditor.
             *
             * @param element MediumEditor lement.
             */
            init: function (element) {

                console.log(element);

                var options = element.data('options');
                if (typeof options == 'string') {
                    options = $.parseJSON(options);
                } else if (typeof options != 'object') {
                    options = {};
                }

                var addon = element.data('addons');
                if (typeof addon == 'string') {
                    addon = $.parseJSON(addon);
                } else if (typeof addon != 'object') {
                    addon = {};
                }

                var mEditor = element.data('name') + "-medium-editor";
                var jMeditor = "#" + mEditor;
                $("#" + element.data('name')).hide();
                $("#" + element.data('name')).after("<div id='" + mEditor + "'></div>")

                var editor = new MediumEditor(jMeditor, options);

                var content = element.val();
                if (content != "") {
                    $(jMeditor).prepend(content);
                    $(jMeditor).removeClass('medium-editor-placeholder');
                }

                $(jMeditor).on('input', function () {
                    var jMeditorContent = $(jMeditor).html();
                    var insertImageDivLastPos = jMeditorContent.lastIndexOf('<div class="mediumInsert"');
                    if (insertImageDivLastPos > -1) {
                        jMeditorContent = jMeditorContent.substr(0, insertImageDivLastPos);
                    }
                    $("#" + element.data('name')).val(jMeditorContent);
                });

                if (JSON.stringify(addon) != "{}") {
                    $(jMeditor).mediumInsert({
                        editor: editor,
                        addons: addon
                    })
                }
            }
        }
    );
}(window, jQuery, PhalconEye));

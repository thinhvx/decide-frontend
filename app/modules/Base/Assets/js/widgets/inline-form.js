/*
 +------------------------------------------------------------------------+
 | PhalconEye CMS                                                         |
 +------------------------------------------------------------------------+
 | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
 +------------------------------------------------------------------------+
 | This source file is subject to the New BSD License that is bundled     |
 | with this package in the file LICENSE.txt.                             |
 |                                                                        |
 | If you did not receive a copy of the license and are unable to         |
 | obtain it through the world-wide-web, please send an email             |
 | to license@phalconeye.com so we can send you a copy immediately.       |
 +------------------------------------------------------------------------+
 | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
 +------------------------------------------------------------------------+
 */

/**
 * Modal forms.
 *
 * @category  PhalconEye
 * @package   PhalconEye Core Module
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright Copyright (c) 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
(function (window, $, root, undefined) {
    root.ns(
        'PhalconEye.widget.inlineForm',
        {
            arrayContainer: {},
            /**
             * Init ckeditor.
             *
             * @param elements Element(s) to init.
             */
            init: function (elements) {
                console.log(elements);
                var container = $(elements);
                console.log(container);
                var html = container.html();
                console.log(html);
                container.data('current-html', html);
                container.html('<div class="inlineForm" tabindex="-1"><div class="inlineForm-content">' + html + '</div></div>');
                var modalObject = $('.inlineForm', container);
                console.log(modalObject);
//                modalObject.data('container', container);
                console.log(modalObject.data('container', container));

                var formId = $('form', container).attr('id');
                PhalconEye.widget.inlineForm.arrayContainer[formId] = container;

                PhalconEye.widget.inlineForm.bindSubmit(modalObject);
                PhalconEye.widget.inlineForm.bindCancel(modalObject);
            },

            /**
             * Open modal for url with data.
             *
             * @param container jQuery object to manage this form.
             * @param url Url to open.
             * @param data With data.
             */
            open: function (container, url, data) {
                PhalconEye.core.showLoadingStage(container.attr('id'));
                $.get(url, data)
                    .done(function (html) {
                        console.log(html);
                        container.data('current-html', container.html());
                        container.html('<div class="inlineForm" tabindex="-1"><div class="inlineForm-content">' + html + '</div></div>');
                        var modalObject = $('.inlineForm', container);
                        modalObject.data('container', container);

                        PhalconEye.widget.inlineForm.bindSubmit(modalObject);
                        PhalconEye.widget.inlineForm.bindCancel(modalObject);

                        PhalconEye.core.hideLoadingStage(container.attr('id'));
                        PhalconEye.helper.scrollTo(container);
                    });
            },

            bindCancel: function (modalObject) {
                var cancelButton = $('.btn-cancel, .btn[name=cancel]', modalObject);
                cancelButton.click(function(e) {
                    e.preventDefault();
                    container = modalObject.data('container');
                    container.html(container.data('current-html'));

                    PhalconEye.core.hideLoadingStage(container.attr('id'));
                    PhalconEye.helper.scrollTo(container);
                    return false;
                });
            },

            /**
             * Bind some submit events for modal form.
             */
            bindSubmit: function (modalObject) {
                var form = $('form', modalObject)

                form.submit(function (e) {
                    var container = modalObject.data('container');
                    e.preventDefault();
                    if (form.length == 1) {
                        PhalconEye.core.showLoadingStage(container.attr('id'));

                        var ckEditor = $("textarea[data-widget=ckeditor]", modalObject);
                        if (ckEditor.length == 0) {
                            ckEditor = $("textarea[data-widget='(ckeditor):invoked']");
                            if (ckEditor != 0) {
                                var ckEditorId = ckEditor.attr('id');
                                var ckContent = CKEDITOR.instances[ckEditorId].getData();
                                ckEditor.val(ckContent);
                            }
                        } else {
                            var ckEditorId = ckEditor.attr('id');
                            var ckContent = CKEDITOR.instances[ckEditorId].getData();
                            ckEditor.val(ckContent);
                        }

                        $.post(form.attr('action'), form.serialize())
                            .done(function (postHTML) {
                                $('.inlineForm-content', modalObject).html(postHTML);
                                PhalconEye.widget.inlineForm.bindSubmit(modalObject);
                                PhalconEye.widget.inlineForm.bindCancel(modalObject);
                                PhalconEye.core.hideLoadingStage(container.attr('id'));

                                // Evaluate js
                                $(postHTML).filter("script").each(function () {
                                    var scriptContent = $(this).html(); //Grab the content of this tag
                                    eval(scriptContent); //Execute the content
                                });

                                PhalconEye.helper.scrollTo(container);
                            });
                    }
                    else {
                        container.html(container.data('current-html'));
                    }

                    return false;
                });
            },
            /**
             * Bind data modal form.
             */
            bindData: function (params) {
                var container = PhalconEye.widget.inlineForm.arrayContainer[params.form];

                PhalconEye.widget.inlineForm.open(container, params.url, '');
            }
        }
    );
}(window, jQuery, PhalconEye));


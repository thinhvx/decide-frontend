<?php
/**
 * Created by PhpStorm.
 * User: kahn
 * Date: 9/10/14
 * Time: 3:33 PM
 */

namespace Base\Helper;

use Engine\Helper;

class Renderer extends \Core\Helper\Renderer {
    public function renderWidgetByName($widgetModule, $widgetName, $action = "index", $params = []) {
        if (!$this->widgetIsAllowed($params)) {
            return '';
        }

        $widgetModule = ucfirst($widgetModule);
        $widgetName = ucfirst($widgetName);

        $controllerClass = "\\{$widgetModule}\\Widget\\{$widgetName}\\Controller";

        $controller = new $controllerClass();
        $controller->setDefaults($widgetName, $widgetModule, $params);

        // Check cache.
        $output = null;
        $cacheKey = $controller->getCacheKey();
        $cacheLifeTime = $controller->getCacheLifeTime();
        /** @var \Phalcon\Cache\BackendInterface $cache */
        $cache = $this->getDI()->get('cacheOutput');

        if ($controller->isCached()) {
            $output = $cache->get($cacheKey, $cacheLifeTime);
        }

        if ($output === null) {

            $controller->prepare($action);
            $controller->{"{$action}Action"}();

            if ($controller->getNoRender()) {
                return '';
            }

            $controller->view->getRender(null, $action);
            $output = $controller->view->getContent();

            if ($controller->isCached()) {
                $cache->save($cacheKey, trim($output), $cacheLifeTime);
            }
        }
        return $output;

    }
}
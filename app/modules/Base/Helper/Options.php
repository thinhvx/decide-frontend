<?php
/**
 * Created by PhpStorm.
 * User: Khanh
 * Date: 7/28/14
 * Time: 8:44 PM
 */

namespace Base\Helper;


use Phalcon\DI;

class Options {

    public static function months() {
        $i18n = DI::getDefault()->getI18n();
        $arr = [
            0 => '--  ' . $i18n->_('Select a month') . '  --',
            1 => $i18n->_('January'),
            2 => $i18n->_('February'),
            3 => $i18n->_('March'),
            4 => $i18n->_('April'),
            5 => $i18n->_('May'),
            6 => $i18n->_('June'),
            7 => $i18n->_('July'),
            8 => $i18n->_('August'),
            9 => $i18n->_('September'),
            10 => $i18n->_('October'),
            11 => $i18n->_('November'),
            12 => $i18n->_('December')
        ];

        return $arr;
    }

    public static function degrees() {
        $i18n = DI::getDefault()->getI18n();
        $arr = [
            'none' => '-',
            'High School' => $i18n->_('High School'),
            'Associate\'s Degree' => $i18n->_('Associate\'s Degree'),
            'Bachelor\'s Degree' => $i18n->_('Bachelor\'s Degree'),
            'Master\'s Degree' => $i18n->_('Master\'s Degree'),
            'Master of Business Administration (M.B.A.)' => $i18n->_('Master of Business Administration (M.B.A.)'),
            'Juris Doctor (J.D.)' => $i18n->_('Juris Doctor (J.D.)'),
            'Doctor of Medicine (M.D.)' => $i18n->_('Doctor of Medicine (M.D.)'),
            'Doctor of Philosophy (Ph.D.)' => $i18n->_('Doctor of Philosophy (Ph.D.)'),
            'Engineer\'s Degree' => $i18n->_('Engineer\'s Degree'),
            'other' => $i18n->_('Other'),
        ];

        return $arr;
    }

    public static function formatWorkingPeriod($years, $months) {
        $i18n = DI::getDefault()->getI18n();

        $arr = [];
        if ($years > 0) {
            $arr[] = $years > 1 ? ($years . ' ' . $i18n->_('years')) : $i18n->_('1 year');
        }
        if ($months > 0) {
            $arr[] = ($months > 1 ? ($months . ' ' . $i18n->_('months')) : $i18n->_('1 month'));
        }

        return implode(' ', $arr);
    }
} 
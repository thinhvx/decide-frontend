<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Base\Widget\Pageviews;

use Core\Form\CoreForm;
use Engine\Widget\Controller as WidgetController;

/**
 * Widget Pageviews.
 *
 * @category PhalconEye\Widget
 * @package  Widget
 */
class Controller extends WidgetController
{
    /**
     * Widget name.
     *
     * @var string
     */
    private $_widgetName = 'page_views';

    /**
     * Index action.
     *
     * @return void
     */
    public function indexAction()
    {
        $ga_email = $this->getDI()->getConfig()->application->params->ga->ga_email;
        $ga_password = $this->getDI()->getConfig()->application->params->ga->ga_password;
        $ga_profile_id = $this->getDI()->getConfig()->application->params->ga->ga_profile_id;
        $ga_start_date = $this->getDI()->getConfig()->application->params->ga->ga_start_date;
        $ga_record_property = $this->getDI()->getConfig()->application->params->ga->ga_record_property;

        $ga = new \Gapi($ga_email, $ga_password);

        $ga->requestReportData($ga_profile_id, array('browser'), array($ga_record_property), null, null, $ga_start_date, date('Y-m-d', time()));

        $gaResult = $ga->getResults();
        if (empty($gaResult)) {
            $this->view->pageviews = 0;
        } else {
            $counter = 0;
            foreach ($gaResult as $result) {
                $counter += $result->getPageviews();
            }

            $this->view->pageviews = $counter;
        }
    }

    /**
     * Action for management from admin panel.
     *
     * @return CoreForm
     */
    public function adminAction()
    {
        $form = new CoreForm();

        return $form;
    }

    /**
     * Check if this widget must be cached.
     *
     * @return bool
     */
    public function isCached()
    {
        return false;
    }

    /**
     * What cache lifetime will be for this widget.
     *
     * @return int
     */
    public function cacheLifeTime()
    {
        return 300;
    }
}
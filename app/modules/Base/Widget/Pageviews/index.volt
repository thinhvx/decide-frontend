{#
   PhalconEye

   LICENSE

   This source file is subject to the new BSD license that is bundled
   with this package in the file LICENSE.txt.

   If you did not receive a copy of the license and are unable to
   obtain it through the world-wide-web, please send an email
   to phalconeye@gmail.com so we can send you a copy immediately.
#}
{% extends "../../Core/View/layouts/widget.volt" %}

{% block content %}
    <div class="col-md-4 info-items">
        <div class="info-items-content">
            <div class="info-items-data">
                {{ pageviews }}
            </div>

            <div class="info-items-unit">
                Views
            </div>
        </div>
        <div class="info-items-title">
            <a href="#">
                <span class="info-item-viewmore">VIEW MORE</span>
                <span class="info-item-viewmoreicon"><i class="m-icon-swapright m-icon-white"></i> </span>

                {#<span class="glyphicon glyphicon-circle-arrow-right"></span>#}
            </a>
        </div>
    </div>
{% endblock %}
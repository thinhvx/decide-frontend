<div class="pre-footer">
    <div class="container geekup-prefooter">
        <div class="row">
            <!-- BEGIN BOTTOM ABOUT BLOCK -->
            <div class="col-md-4 col-sm-4 pre-footer-col footer-aboutus-col">
                <h2>About us</h2>

                <p class="margin-bottom-13">
                    Decide - What to buy - When to buy Product<br/>
                    <a href="#">More details...</a>
                </p>
            </div>
            <!-- END BOTTOM ABOUT BLOCK -->

            <!-- BEGIN BOTTOM CONTACTS -->
            <div class="col-md-4 col-sm-4 pre-footer-subscribe-box pre-footer-subscribe-box-vertical geekup-subcribe-box">
                <h2>Newsletter</h2>

                <p class="margin-bottom-10"></p>

                <form action="#">
                    <div class="input-group" id="input-email">
                        <input name="subscribe-newsletter" type="text" placeholder="youremail@mail.com"
                               class="form-control"/>
                            <span class="input-group-btn">
                            <button class="btn btn-primary geekup-subcribe-btn"
                                    onclick="onSubmitNewsletter(); return false;">
                                Subscribe
                            </button>
                            </span>
                    </div>
                </form>
            </div>
        </div>
        <!-- END BOTTOM CONTACTS -->
        <!-- BEGIN TWITTER BLOCK -->
        <div class="col-md-4 col-sm-4 pre-footer-col">
        </div>
        <!-- END TWITTER BLOCK -->

    </div>
</div>
<!-- END PRE-FOOTER -->

<!-- BEGIN FOOTER -->
<div class="footer ">
    <div class="container geekup-footer">
        <div class="row">
            <!-- BEGIN COPYRIGHT -->
            <div class="col-md-6 col-sm-6 col-xs-6 padding-top-10 geekup-copyright">
                2014 © <a href="http://thinhvoxuan.me" style="text-decoration: none">Thinh VoXuan</a>
            </div>
            <!-- END COPYRIGHT -->
            <!-- BEGIN PAYMENTS -->
            <div class="col-md-6 col-sm-6 col-xs-6 geekup-social-icon ">
                <ul class="social-footer list-unstyled list-inline pull-right  ">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                </ul>
            </div>
            <!-- END PAYMENTS -->
        </div>
    </div>
</div>
{#<!-- Alpha Modal -->#}
<div class="modal fade" id="alpha-model" tabindex="-1" role="dialog" aria-labelledby="alpha-model"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Alpha version</h4>
            </div>
            <div class="modal-body">
                This application is alpha version, we appreciate and welcome all your feedback!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{#<!-- End Alpha Modal -->#}

<!-- Thank subscribe email -->
<span id="span-subcribe" style="display: hidden" data-toggle="modal" data-target="#subcribe-email"></span>
<div class="modal fade" id="subcribe-email" tabindex="-1" role="dialog" aria-labelledby="alpha-model"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 style="text-align: center">Thanks for signing up</h4>
                <h4 style="text-align: center">Check your email to confirm your subscription</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End subscribe email  -->


<!-- Thank subscribe email -->
<span id="span-error" style="display: hidden" data-toggle="modal" data-target="#subcribe-error"></span>
<div class="modal fade" id="subcribe-error" tabindex="-1" role="dialog" aria-labelledby="alpha-model"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4> Your Email have already subscribed </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End subscribe email  -->

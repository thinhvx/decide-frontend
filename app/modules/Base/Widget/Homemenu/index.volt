{#
   PhalconEye

   LICENSE

   This source file is subject to the new BSD license that is bundled
   with this package in the file LICENSE.txt.

   If you did not receive a copy of the license and are unable to
   obtain it through the world-wide-web, please send an email
   to phalconeye@gmail.com so we can send you a copy immediately.
#}
{% extends "../../Core/View/layouts/widget.volt" %}

{% block content %}
    <div class="header">
        <div class="container geekup-menu ">
            <a href="{{ url() }}" class="site-logo">

                {% if not(logo is empty) %}
                    <div class="logo">
                        <img alt='{{ site_title }}' src="{{ url(logo) }}" class="logo-img"/>
                    </div>
                {% endif %}
                <div class="site-name">
                    <p class="name"><span style="color: #014e7c">GEEK</span> <span style="color: #67db3c">Hub</span></p>
                    <p class="slogan">INSIGHTFUL SOFTWARE DEVELOPMENT</p>
                </div>
            </a>
            <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

            <!-- BEGIN NAVIGATION -->
            <div class="header-navigation pull-right font-transform-inherit geekup-menu-list">
                <ul class="nav">

                    {% for item in menu_items %}
                        {% if item['active'] %}
                            <li class="active">
                        {% else %}
                            <li>
                        {% endif %}
                        <a class="system-tooltip" href="{{ item['href'] }}" data-original-title="" title="">{{ item['name'] }}</a>
                        </li>
                    {% endfor %}
                </ul>
            </div>
            <!-- END NAVIGATION -->
            <div class="clear"></div>
        </div>
    </div>

{% endblock %}
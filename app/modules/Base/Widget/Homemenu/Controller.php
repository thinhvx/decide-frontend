<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Base\Widget\Homemenu;

use Core\Form\CoreForm;
use Engine\Widget\Controller as WidgetController;
use Engine\Sidebar;
use Core\Model\Settings;

/**
 * Widget Homemenu.
 *
 * @category PhalconEye\Widget
 * @package  Widget
 */
class Controller extends WidgetController
{
    /**
     * Widget name.
     *
     * @var string
     */
    private $_widgetName = 'home_menu';

    const MODULE_NAME = 'base';
    /**
     * Index action.
     *
     * @return void
     */
    public function indexAction()
    {
        $menuBar = new Sidebar();
        $isActive = (self::MODULE_NAME === $this->dispatcher->getModuleName());
        $menuBar->addItem(0, [
            'name' => 'Home',
            'href' => '/',
            'active' => $isActive
        ]);
        $this->getDI()->getEventsManager()->fire('nagivation:homemenu',  $menuBar);

        $menuItems = $menuBar->getItems();
        ksort($menuItems);

        $this->view->site_title = $this->getDI()->getConfig()->application->params->generals->siteTitle;
        $this->view->logo = $this->getDI()->getConfig()->application->params->generals->logoPath;
        $this->view->menu_items = $menuItems;
    }

    /**
     * Action for management from admin panel.
     *
     * @return CoreForm
     */
    public function adminAction()
    {
        $form = new CoreForm();

        return $form;
    }

    /**
     * Check if this widget must be cached.
     *
     * @return bool
     */
    public function isCached()
    {
        return false;
    }

    /**
     * What cache lifetime will be for this widget.
     *
     * @return int
     */
    public function cacheLifeTime()
    {
        return 300;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Khanh
 * Date: 8/2/14
 * Time: 9:38 PM
 */

namespace Base\Form;

use Core\Form\CoreForm;
use Core\Form\TextForm;
use Engine\Form\Element\Button;
use Engine\Form\Element\ButtonLink;
use Engine\Form\FieldSet;

class BaseTextForm extends TextForm {

    protected $_actionButtons;
    /**
     * Resolve view.
     *
     * @param string $view   View path.
     * @param string $module Module name (capitalized).
     *
     * @return string
     */
    protected function _resolveView($view, $module = 'Base')
    {
        return '../../' . $module . '/View/' . $view;
    }

    /**
     * Button element.
     *
     * @param string     $name       Element name.
     * @param null       $label      Element label.
     * @param bool       $isSubmit   Button type 'submit'.
     * @param mixed|null $value      Element value.
     * @param array      $options    Element options.
     * @param array      $attributes Element attributes.
     *
     * @return $this
     */
    public function addActionButton(
        $name,
        $label = null,
        $isSubmit = true,
        $value = null,
        array $options = [],
        array $attributes = []
    )
    {
        $element = new Button($name, $options, $attributes);

        if (!$label) {
            $label = ucfirst($name);
        }

        $element
            ->setOption('label', $label)
            ->setOption('isSubmit', $isSubmit)
            ->setValue($value);

        $this->getActionButtons()->add($element);

        return $this;
    }

    public function clearActionButtons() {
        $this->getActionButtons()->clearElements();
        return $this;
    }

    /**
     * ButtonLink element.
     *
     * @param string     $name       Element name.
     * @param null       $label      Element label.
     * @param mixed|null $value      Element link.
     * @param array      $options    Element options.
     * @param array      $attributes Element attributes.
     *
     * @return $this
     */
    public function addActionButtonLink($name, $label = null, $value = null, array $options = [], array $attributes = [])
    {
        $element = new ButtonLink($name, $options, $attributes);

        if (!$label) {
            $label = ucfirst($name);
        }

        $element
            ->setOption('label', $label)
            ->setValue($value);

        $this->getActionButtons()->add($element);

        return $this;
    }

    public function getActionButtons() {
        if ($this->_actionButtons == null) {
            $this->_actionButtons = new FieldSet('actionButtons');
        }
        return $this->_actionButtons;
    }

    public function setIsHorizontal($bool = true) {
        $this->setAttribute('is_horizontal', $bool);
        return $this;
    }

    public function isHorizontal() {
        return $this->getAttribute('is_horizontal');
    }

    public function getIcon() {
        return $this->getAttribute('icon');
    }

    public function setIcon($icon) {
        return $this->setAttribute('icon', $icon);
    }

}
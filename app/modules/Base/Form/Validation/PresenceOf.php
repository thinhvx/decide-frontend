<?php
/**
 * Created by PhpStorm.
 * User: thinhvoxuan
 * Date: 9/17/14
 * Time: 9:01 AM
 */

namespace Base\Form\Validation;
use Phalcon\DI;
use Phalcon\Validation\Validator\PresenceOf as PresenceOfPhalcon;
use Phalcon\Validation\ValidatorInterface;

class PresenceOf extends PresenceOfPhalcon implements ValidatorInterface {

    public function __construct($params = [])
    {
        if (isset($params['message'])) {
            $params['message'] = DI::getDefault()->get('i18n')->_($params['message']);
        }
        parent::__construct($params);
    }
} 
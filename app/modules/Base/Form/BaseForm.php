<?php
/**
 * Created by PhpStorm.
 * User: Khanh
 * Date: 8/2/14
 * Time: 9:38 PM
 */

namespace Base\Form;

use Base\Form\Element\InputTagsElement;
use Base\Form\Element\MediumEditor;
use Base\Form\Element\RawElement;
use Core\Form\CoreForm;
use Engine\Form\Element\Button;
use Engine\Form\Element\ButtonLink;
use Engine\Form\FieldSet;

class BaseForm extends CoreForm {

    protected $_actionButtons;
    /**
     * Resolve view.
     *
     * @param string $view   View path.
     * @param string $module Module name (capitalized).
     *
     * @return string
     */
    protected function _resolveView($view, $module = 'Base')
    {
        return '../../' . $module . '/View/' . $view;
    }

    /**
     * Button element.
     *
     * @param string     $name       Element name.
     * @param null       $label      Element label.
     * @param bool       $isSubmit   Button type 'submit'.
     * @param mixed|null $value      Element value.
     * @param array      $options    Element options.
     * @param array      $attributes Element attributes.
     *
     * @return $this
     */
    public function addActionButton(
        $name,
        $label = null,
        $isSubmit = true,
        $value = null,
        array $options = [],
        array $attributes = []
    )
    {
        $element = new Button($name, $options, $attributes);

        if (!$label) {
            $label = ucfirst($name);
        }

        $element
            ->setOption('label', $label)
            ->setOption('isSubmit', $isSubmit)
            ->setValue($value);

        $this->getActionButtons()->add($element);

        return $this;
    }

    public function clearActionButtons() {
        $this->getActionButtons()->clearElements();
        return $this;
    }

    /**
     * ButtonLink element.
     *
     * @param string     $name       Element name.
     * @param null       $label      Element label.
     * @param mixed|null $value      Element link.
     * @param array      $options    Element options.
     * @param array      $attributes Element attributes.
     *
     * @return $this
     */
    public function addActionButtonLink($name, $label = null, $value = null, array $options = [], array $attributes = [])
    {
        $element = new ButtonLink($name, $options, $attributes);

        if (!$label) {
            $label = ucfirst($name);
        }

        $element
            ->setOption('label', $label)
            ->setValue($value);

        $this->getActionButtons()->add($element);

        return $this;
    }

    /**
    * Medium Editor element.
    *
    * @param string      $name           Element name.
    * @param string|null $label          Element label.
    * @param string|null $description    Element description.
    * @param array|null  $elementOptions Medium Editor options.
    * @param mixed|null  $value          Element value.
    * @param array       $options        Element options.
    * @param array       $attributes     Element attributes.
    *
    * @return $this
    */
    public function addMediumEditor(
        $name,
        $label = null,
        $description = null,
        $elementOptions = [],
        $elementAddons = [],
        $value = null,
        array $options = [],
        array $attributes = []
    )
    {
        $options['elementOptions'] = $elementOptions;
        $options['elementAddons'] = $elementAddons;
        $element = new  MediumEditor($name, $options, $attributes);

        if (!$label) {
            $label = ucfirst($name);
        }

        $element
            ->setOption('label', $label)
            ->setOption('description', $description)
            ->setOption('elementOptions', $elementOptions)
            ->setOption('elementAddons', $elementAddons)
            ->setValue($value);

        $this->add($element);

        return $this;
    }

    public function addInputTagsElement(
        $name,
        $label = null,
        array $options = [],
        array $attributes = [],
        $elementOption = []
    )
    {

        $element = new InputTagsElement($name, $options, $attributes, $elementOption);

        if (!$label) {
            $label = ucfirst($name);
        }

        $element
            ->setOption('label', $label);

        $this->add($element);

        return $this;
    }

    public function getActionButtons() {
        if ($this->_actionButtons == null) {
            $this->_actionButtons = new FieldSet('actionButtons');
        }
        return $this->_actionButtons;
    }

    public function setIsHorizontal($bool = true) {
        $this->setAttribute('is_horizontal', $bool);
        return $this;
    }
    public function hideLabel() {
        return $this->getAttribute('hideLabel');
    }

    public function isHorizontal() {
        return $this->getAttribute('is_horizontal');
    }

    public function getIcon() {
        return $this->getAttribute('icon');
    }

    public function setIcon($icon) {
        return $this->setAttribute('icon', $icon);
    }

}
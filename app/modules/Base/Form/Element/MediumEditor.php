<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace Base\Form\Element;

use Engine\Form\Element\TextArea;
use Engine\Form\ElementInterface;

/**
 * Form element - Medium Editor.
 *
 */
class MediumEditor extends TextArea implements ElementInterface
{
    /**
     * Get allowed options for this element.
     *
     * @return array
     */
    public function getAllowedOptions()
    {

        return  array_merge(parent::getAllowedOptions(), ['elementOptions','elementAddons']);
    }

    /**
     * Get element default attribute.
     *
     * @return array
     */
    public function getDefaultAttributes()
    {
        return array_merge(
            parent::getDefaultAttributes(),
            [
                'data-widget' => 'medium-editor',
                'data-name' => $this->getName(),
                'data-options' => htmlspecialchars(json_encode($this->getOption('elementOptions', []))),
                'data-addons' =>htmlspecialchars(json_encode($this->getOption('elementAddons', [])))
            ]
        );
    }




    /**
     * Sets the element option.
     *
     * @param string $value  Element value.
     * @param bool   $escape Try to escape html in value.
     *
     * @return $this
     */
    public function setValue($value, $escape = false)
    {
        return parent::setValue($value, $escape);
    }

    /**
     * Render element.
     *
     * @return string
     */
    public function render()
    {
        $this->getDI()->get('assets')->addJs('assets/js/base/widgets/medium-editor.js');
        return parent::render();
    }
}

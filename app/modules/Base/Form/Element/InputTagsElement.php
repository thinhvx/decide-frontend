<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace Base\Form\Element;

use Engine\Form\Element\Text;

/**
 * Form element - Medium Editor.
 *
 */
class InputTagsElement extends Text
{
    private $elementOption;

    public function __construct($name, array $options = [], array $attributes = [], $elementOption = [])
    {
        parent::__construct($name, $options, $attributes);
        $this->elementOption = $elementOption;
    }

    /**
     * If element is need to be rendered in default layout.
     *
     * @return bool
     */
    public function useDefaultLayout()
    {
        return false;
    }

    public function render()
    {
        return sprintf(
            $this->getHtmlTemplate(),
            $this->getValue()
        );
    }

    public function getDefaultAttributes()
    {
        $default = ['id' => $this->getName(), 'name' => $this->getName()];
        if ($this->getOption('required')) {
            $default['required'] = 'required';
        }
        return $default;
    }

    /**
     * Get attributes as html.
     *
     * @return string
     */
    protected function _renderAttributes()
    {
        $html = '';
        foreach ($this->_attributes as $key => $attribute) {
            $html .= sprintf(' %s="%s"', $key, $attribute);
        }

        if (count($this->elementOption) > 0) {
            $key = $this->getOption("using")[0];
            $value = $this->getOption("using")[1];

            foreach ($this->elementOption as $option) {
                $data[] = $option->$key;
                $data[] = $option->$value;
            }

            $data = implode(",", $data);
            $html .= " data-data='" .  $data . "'";
        }

        return $html;
    }

    public function getHtmlTemplate()
    {
        return $this->getOption('htmlTemplate', '<input' . $this->_renderAttributes() . ' value="%s">');
    }
}

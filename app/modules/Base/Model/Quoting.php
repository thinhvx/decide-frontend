<?php
/**
 * Created by PhpStorm.
 * User: ngtrieuvi92
 * Date: 8/20/14
 * Time: 11:44 AM
 */
namespace Base\Model;
use Engine\Db\AbstractModel;

/**
 * Class Quoting
 * @package Base\Model
 *
 * @Source('quoting')
 */

class Quoting extends AbstractModel {
    const
        /**
         * Post statuses.
         */
        STATUS_DRAFT        = 1,
        STATUS_PUBLISHED    = 2,
        STATUS_DELETED      = 3;

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    public $id;

    /**
     * @Column(type="string", nullable=false, size="255")
     */
    public $quoting;

    /**
     * @Column(type="string", nullable=false, size="255")
     */
    public $author;
    /**
     * @Column(type="integer", nullable=false, size="4")
     */
    public $status = 0;
}


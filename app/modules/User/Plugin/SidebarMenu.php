<?php

namespace User\Plugin;
use Engine\Sidebar;
use Phalcon\Mvc\User\Plugin as PhalconPlugin;

class SidebarMenu extends PhalconPlugin {
    public function sidebar($event, Sidebar $sideBar) {
        $sideBar->addItem(100, [
            'name' => 'userrole',
            'title' => 'User And Role',
            'prepend' => '<i class="fa fa-user"></i>',
            'items' => [ // type - dropdown
                'manager/users' => [
                    'title' => 'List Users',
                    'href' => 'manager/users',
                    'prepend' => '<i class="fa fa-users"></i>'
                ],
                'manager/users/upgrade-requests' => [
                    'title' => 'List Upgrade Requests',
                    'href' => 'manager/users/upgrade-requests',
                    'prepend' => '<i class="fa fa-level-up"></i>'
                ],
                'manager/users/create' => [
                    'title' => 'Create User',
                    'href' => 'manager/users/create',
                    'prepend' => '<i class=" fa fa-plus"></i>'
                ],
                'manager/users/roles' => [
                    'title' => 'Roles',
                    'href' => 'manager/users/roles',
                    'prepend' => '<i class="fa fa-check"></i>'
                ],
                'manager/users/roles-create' => [
                    'title' => 'Create role',
                    'href' => 'manager/users/roles-create',
                    'prepend' => '<i class="fa fa-plus"></i>'
                ]

            ]
        ]);
    }
}
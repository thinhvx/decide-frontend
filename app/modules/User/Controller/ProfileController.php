<?php
/**
 * Created by PhpStorm.
 * User: Ngan
 * Date: 8/7/14
 * Time: 7:19 AM
 */

namespace User\Controller;
use Base\Controller\FrontendBaseController;
use Engine\Helper;
use Core\Controller\AbstractController;
use User\Form\Profile\ChangePassword;
use User\Form\Profile\Profile as ProfileForm;
use User\Form\Profile\Avatar as AvatarForm;
use User\Form\Profile\ChangePassword as ChangePasswordForm;
use User\Model\Profile;
use User\Model\Role;
use Phalcon\Db\Column;
use Phalcon\Mvc\Url;
use User\Model\User;
use Phalcon\Flash\Session as FlashSesion;

/**
 * Profile controller.
 *
 * @category PhalconEye\Module
 * @package  Controller
 *
 * @RoutePrefix("/profile", name="profile")
 */
class ProfileController extends FrontendBaseController
{



    /**
     * Module index action.
     *
     * @return void
     *
     * @Route("/", methods={"POST", "GET"}, name="view-profile")
     * @Acl(roles="Guest")
     */
    public function indexAction(){
        if(empty(User::getViewer()->id)){
            return $this->response->redirect(['for'=>'login']);
        }
        $this->getDI()->set('flash', function(){
            return new FlashSesion(array(
                'error' => 'alert alert-error',
                'success' => 'alert alert-success',
                'notice' => 'alert alert-info',
            ));
        });

        $form = new ProfileForm();
        $form->setAction('profile#tab_1_3');
        $changePasswordForm = new ChangePasswordForm();
        $changePasswordForm->setAction('profile#tab_3-3');

        $user = User::getViewer();

        $profile = Profile::getProfileUser($user);

        $this->view->userModel = $user;
        $this->view->profile = $profile;

        $form->setValues(array(
            'firstname'=>$profile->firstname,
            'lastname' =>$profile->lastname,
            'phonenumber'=>$profile->phonenumber,
            'interest'=>$profile->interest,
            'occupation'=>$profile->occupation,
            'about_me' => $profile->about_me,
            'website' => $profile->website
        ));

        if(!$this->request->isPost()){
            $this->view->form  = $form;
            $this->view->form  = $form;
            $this->view->changePasswordForm = $changePasswordForm;
            return;
        }else{
            if( isset($_POST['context']) && $_POST['context'] == "Password" && !$changePasswordForm->isValid()){

                $this->view->changePasswordForm = $changePasswordForm;
                $this->view->form = $form;
                return;
            }
            if( isset($_POST['context']) && $_POST['context'] == "Profile" && !$form->isValid()){
                $this->view->changePasswordForm = $changePasswordForm;
                $this->view->form = $form;
                return;
            }

            if (isset($_FILES['Profile']['name'])) {
                $profile->avatar = $_FILES['Profile']['name']['avatar'];
                $profile->save();
                $updateDestination = UPLOAD_PATH . "avatar/";
                $file = $updateDestination . $_FILES['Profile']['name']['avatar'];
                if (move_uploaded_file($_FILES['Profile']['tmp_name']['avatar'], $file)) {
                    return $this->response->redirect(['for' => 'view-profile']);
                }
            }



            if($this->request->isPost()){
                if($_POST['context'] == "Password"){
                    $changePasswordData = $changePasswordForm->getValues();
                    if (!$this->security->checkHash($changePasswordData['password'], $user->password)) {
                        $changePasswordForm->addError('Password is incorrect');
                    }
                    if($changePasswordData['newPassword'] != $changePasswordData['repeatNewPassword']){
                        $changePasswordForm->addError('New password and renew password is not matched');
                    }
                    if( sizeof($changePasswordForm->getErrors()) == 0 ) {
                        $user->setPassword($changePasswordData['newPassword']);

                        $user->save();
                        //print_r($changePasswordData['newPassword']);die;
                        $this->flashSession->notice("Change password successful!");
                        return $this->response->redirect(['for' => 'view-profile']);
                    }else{
                        $this->view->form = $form;
                        $this->view->changePasswordForm = $changePasswordForm;
                        return;
                    }
                }

                if($_POST['context'] == "Profile"){
                    $profileData = $form->getValues();
                    //print_r($profileData);die;
                    if(!$profile->save($profileData)){
                        foreach ($profile->getMessages() as $message) {
                            $form->addError($message);
                        }
                        $this->view->form = $form;
                        $this->view->changePasswordForm = $changePasswordForm;
                        return;
                    }else{
                        $this->flashSession->notice("Edit profile successful!");
                        return $this->response->redirect(['for' => 'view-profile']);
                    }
                }

            }else{

            }
        }



    }
}
<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace User\Controller;

use Base\Controller\FrontendBaseController;
use Core\Controller\AbstractController;
use Phalcon\Db\Column;
use Phalcon\Flash\Session as FlashSesion;
use Phalcon\Http\ResponseInterface;
use User\Form\Auth\Login as LoginForm;
use User\Form\Auth\Register as RegisterForm;
use User\Form\Auth\ForgotPasswordRequest as ForgotPasswordRequestForm;
use User\Form\Auth\ResetPassword as ResetPasswordForm;
use User\Helper\MandrillEmail;
use User\Model\Profile;
use User\Model\Role;
use User\Model\Subscribe;
use User\Model\User;
use Core\Model\Access;
use OAuth\Common\Http\ClientInterface;
use OAuth\OAuth2\Service\Facebook;
use OAuth\Common\Storage\Session;
use OAuth\Common\Consumer\Credentials;
use OAuth\ServiceFactory;

use User\Model\UserResetPasswordRequest;
use Mandrill;
//use User\Helper\MandrillEmail;

/**
 * Auth handler.
 *
 * @category  PhalconEye
 * @package   User\Controller
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
class AuthController extends FrontendBaseController
{
    /**
     * Login action.
     *
     * @return mixed
     *
     * @Route("/login", methods={"GET", "POST"}, name="login")
     * @Acl(roles="Guest")
     */
    public function loginAction()
    {
//        $this->assets->addCss('assets/css/user/auth.css');
        $this->view->not_render_breadcrums = true;
        if (User::getViewer()->id) {
            return $this->response->redirect();
        }

        $form = new LoginForm();
        if (!$this->request->isPost() || !$form->isValid(null,true)) {
            $this->view->form = $form;
            return $this->view->pick('Auth/loginOnly2');;
        }

        $login = $this->request->getPost('login', 'string');
        $password = $this->request->getPost('password', 'string');

        $user = User::findFirst(
            [
                "email = ?0 OR username = ?0",
                "bind" => [$login],
                "bindTypes" => [Column::BIND_PARAM_STR]
            ]
        );

        if ($user) {
            if ($this->security->checkHash($password, $user->password)) {
                $this->core->auth()->authenticate($user->id);
                $request = $this->getDI()->getRequest();
                $returnUrl = $request->get('r');
                if($returnUrl != null  && $returnUrl != ""){
                    $returnUrl = str_replace('//', '/', $returnUrl);
                    return $this->response->redirect($returnUrl);
                }
                return $this->response->redirect();
            }
        }
        $this->view->pick('Auth/loginOnly2');
        $form->addError('Incorrect email or password!');
        $this->view->form = $form;

    }



    /**
     * Logout action.
     *
     * @return ResponseInterface
     *
     * @Route("/logout", methods={"GET", "POST"}, name="logout")
     * @Acl(roles="User")
     */
    public function logoutAction()
    {
       if (User::getViewer()->id) {
            $this->core->auth()->clearAuth();
        }

        return $this->response->redirect();
    }

    /**
     * Register action.
     *
     * @return mixed
     *
     * @Route("/register", methods={"GET", "POST"}, name="register")
     * @Acl(roles="Guest")
     */
    public function registerAction()
    {
	$this->view->not_render_breadcrums = true;
        $this->getDI()->set('flash', function(){
            return new FlashSesion(array(
                'error' => 'alert alert-error',
                'success' => 'alert alert-success',
                'notice' => 'alert alert-info',
            ));
        });


        if (User::getViewer()->id) {
            return $this->response->redirect();
        }
        $form = new RegisterForm();

        if (!$this->request->isPost() || !$form->isValid()) {
            $this->view->form = $form;

            return;
        }
        $password = $form->getValues()['password'];
        $repeatPassword = $form->getValues()['repeatPassword'];
        if ($password != $repeatPassword) {
            $form->addError("Passwords doesn't match!", 'password');
            $this->view->form = $form;

            return;
        }

//        $role = new Role();
//        $role->name = $form->getValue('username');
//        $role->description = $role->name." role";
//        $role->save();

        $user = new User();
        $data = $form->getValues();
        $user->role_id = Role::getDefaultRole()->id;
//        $user->role_id = $role->id;


        $isSubscribe = (isset($data['subscribe_newsletter']) && $data['subscribe_newsletter'] == 1) ? 1: 0;
        $activateKey = md5($this->_generateToken() . time());
        $user->activate_key = $activateKey;
        $user->status = User::STATUS_INACTIVE;


        if (!$user->save($data)) {
            foreach ($user->getMessages() as $message) {
                $form->addError($message);
            }
            // rollback
//            $role->delete();
            $this->view->form = $form;

            return;
        }


//        //@TODO: save user profile
//        $profile = new Profile();
//        $profile->firstname = $data['firstname'];
//        $profile->lastname = $data['lastname'];
//        $profile->user_id = $user->id;
//        $profile->save();

        //@TODO: save user email for subscribe
        if($isSubscribe){
            $subscribe = \User\Model\User::find('email="'.$data['email'] . '"')->getFirst();
            if(!$subscribe){
                $subscribe = new Subscribe();
                $subscribe->email = $data['email'];
                $subscribe->save();
            }
        }


        //@TODO: send email with activate key

        if($_SERVER['SERVER_PORT'] != 80){
            $activateLink =  'http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . '/activate/' . $activateKey;
        }else{
            $activateLink =  'http://' . $_SERVER['SERVER_NAME'] . '/activate/' . $activateKey;
        }

        $mailer = new MandrillEmail();
        $from = $this->getDI()->getConfig()->application->params->mail->no_reply_from;
        $fromName = $this->getDI()->getConfig()->application->params->mail->no_reply_from_name;
        $subject = $this->getDI()->getConfig()->application->params->mail->activate_account_subject;

        $emailContent = 'This verifies your email address is correct. You can activate your account instantly by clicking on the button below.
            If this was you, you can set a new password <a href="'.$activateLink.'" target="_blank">here</a>:
            <br>
            <br>
            <center><a style="border-radius:3px;color:white;font-size:15px;padding:14px 7px 14px 7px;max-width:210px;font-family:proxima_nova,\'Open Sans\',\'lucida grande\',\'Segoe UI\',arial,verdana,\'lucida sans unicode\',tahoma,sans-serif;border:1px #1373b5 solid;text-align:center;text-decoration:none;width:210px;margin:6px auto;display:block;background-color:#007ee6" href="'.$activateLink.'" target="_blank">Activate your account</a>
            </center>';

        $content = $mailer->emailContent($activateLink, $emailContent);

        $mailer->sendEmail($from, $fromName, $user->email, $subject, $content);

        $this->flashSession->notice($this->getDI()->getConfig()->application->params->flash_message->after_register_success);
        $this->view->form = $form;

        return $this->response->redirect(['for'=>'home-page']);
//        $this->core->auth()->authenticate($user->id);
//        return $this->response->redirect();
    }

    /**
     * Social Login action.
     *
     * @return mixed
     *
     * @Route("/socialLogin", methods={"GET", "POST"}, name="socialLogin")
     * @Acl(roles="Guest")
     */
    public function socialLoginAction(){

        $uriFactory = new \OAuth\Common\Http\Uri\UriFactory();
        $currentUri = $uriFactory->createFromSuperGlobalArray($_SERVER);
        $storage = new Session();

        // Setup the credentials for the requests
        $credentials = new Credentials(
            $this->getDI()->getConfig()->application->params->facebook_social->app_id,
            $this->getDI()->getConfig()->application->params->facebook_social->app_secret,
            $currentUri->getAbsoluteUri()
        );

        /** @var $facebookService Facebook */
        $serviceFactory = new ServiceFactory();
        $facebookService = $serviceFactory->createService('facebook', $credentials, $storage, array('email'));
        $url = $facebookService->getAuthorizationUri();


        if (!empty($_GET['code'])) {
            // This was a callback request from facebook, get the token
            $token = $facebookService->requestAccessToken($_GET['code']);

            $result = json_decode($facebookService->request('/me'));


            //print_r($result);die;
            //@TODO: save user into database

            $userModel = User::findFirst(
                [
                    "email = ?0 OR username = ?0",
                    "bind" => [$result->email],
                    "bindTypes" => [Column::BIND_PARAM_STR]
                ]
            );
            
            if($userModel){
                $profile = \User\Model\Profile::find('user_id='.$userModel->id)->getFirst();
                if($profile){
                    $profile->firstname = $result->first_name;
                    $profile->lastname = $result->last_name;
                    $profile->fb_avatar = "https://graph.facebook.com/" . $result->id . "/picture?type=large";
                    $profile->save();
                }
                $this->core->auth()->authenticate($userModel->id);
                return $this->response->redirect();
            }else{
                $userModel = new User();
                $data = [];
                $data['username'] = $result->email;
                $data['email'] = $result->email;
                $data['social_id'] = $result->id;
                $data['provider'] = isset($_REQUEST['provider']) ? $_REQUEST['provider'] : '';
                $data['role_id'] = Role::getDefaultRole()->id;

                if($userModel->save($data)){

                    //@TODO: save profile
                    $profile = \User\Model\Profile::find("user_id=".$userModel->id)->getFirst();
                    if(!$profile) $profile = new Profile();
                    $profile->firstname = $result->first_name;
                    $profile->lastname = $result->last_name;
                    $profile->user_id = $userModel->id;
                    $profile->fb_avatar = "https://graph.facebook.com/" . $result->id . "/picture?type=large";
                    $profile->save();

                    //@TODO: save user email for subscribe
                    $subscribe = \User\Model\User::find('email="'.$data['email'].'"')->getFirst();
                    if(!$subscribe){
                        $subscribe = new Subscribe();
                        $subscribe->email = $data['email'];
                        $subscribe->save();
                    }

                    //@TODO: login with user
                    $this->core->auth()->authenticate($userModel->id);
                    return $this->response->redirect();
                }else{
                    foreach ($userModel->getMessages() as $message) {
                       echo $message;
                        echo '<br/>';
                    }
                    die("debug");
                }
            }
        }else{
            header('Location: ' . $url);
        }
    }

    /**
     * Forgot password Request
     *
     * @Return mixed
     *
     * @Route("/forgotPassword", method={"GET","POST"}, name="forgotPasswordRequest")
     * @Acl(roles="Guest")
     */
    public function forgotPasswordRequestAction(){
        $this->getDI()->set('flash', function(){
            return new FlashSesion(array(
                'error' => 'alert alert-error',
                'success' => 'alert alert-success',
                'notice' => 'alert alert-info',
            ));
        });
        //if user has been login
        if(User::getViewer()->id){
            return $this->response->redirect();
        }

        $form = new ForgotPasswordRequestForm();
        if(! $this->request->isPost() || !$form->isValid(null,true)){
           $this->view->form = $form;
            return;
        }
        $email = $form->getValues()['email'];
        $user = User::findFirst(array(
            "email ='".$email."'"
        ));
        if(empty($user)){
            $form->addError('No account with that e-mail address exists.');
            $this->view->form = $form;
            return;
        } else {
            $userResetPasswdRequest = new UserResetPasswordRequest();
            $token = md5($this->_generateToken());
            $userResetPasswdRequest->email = $email;
            $userResetPasswdRequest->token = $token;
            $userResetPasswdRequest->user_id = $user->id;
            $userResetPasswdRequest->reset = 0;

            if($userResetPasswdRequest->save()){
                if($_SERVER['SERVER_PORT'] != 80){
                    $links =  'http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].'/resetPassword/'.$token;
                } else
                {
                    $links =  'http://'.$_SERVER['SERVER_NAME'].'/resetPassword/'.$token;
                }
                    $this->_sendResetPasswordEmail($links,$email);
                    $this->flashSession->notice("An e-mail has been sent to ".$email." with further instructions. ");
                    $this->view->form = $form;
                    return $this->response->redirect(['for'=>'forgotPasswordRequest']);
            }
            else {
                $form->addError("Opps,we have some error, please! try again");
                foreach ($userResetPasswdRequest->getMessages() as $message) {
                    $form->addError( $message);
                }
                $this->view->form = $form;
            }
        }
        $this->view->form = $form;
    }

    /**
     * Reset Password
     *
     * @Return mixed
     *
     * @Route("/resetPassword/{token:[0-9a-zA-Z_]+}", method = {"GET","POST"}, name="resetPassword")
     * @Acl(roles="Guest")
     */
    public function resetPasswordAction($token){
        $this->view->breadcrumb = "Enter your new password";
        if(User::getViewer()->id){
            return $this->response->redirect();
        }
        $UserResetPasswdRequest = UserResetPasswordRequest::findFirst(array(
            "token='".$token."'"
        ));
        if(empty($UserResetPasswdRequest)){
            $this->flashSession->error("That reset key has expired or is not valid.");
            return $this->response->redirect(['for'=>'forgotPasswordRequest']);

        }
        if($UserResetPasswdRequest->reset){
            $this->flashSession->error("That reset key has expired or is not valid.");
            return $this->response->redirect(['for'=>'forgotPasswordRequest']);
        }

        $form = new ResetPasswordForm();
        if(! $this->request->isPost() || !$form->isValid(null,true)){
            $this->view->form = $form;
            return;
        }
        $password = $form->getValues()['newPassword'];
        $repeatPassword = $form->getValues()['repeatPassword'];
        if ($password != $repeatPassword) {
            $form->addError("The passwords don't match, please try again.", 'password');
            $this->view->form = $form;
            return;
        }

        $user  = User::findFirstById($UserResetPasswdRequest->user_id);
        $user->setPassword($password);
        if (!$user->save()) {
            foreach ($user->getMessages() as $message) {
                $form->addError($message);
            }
            $this->view->form = $form;
            return;
        }
        $UserResetPasswdRequest->reset = 1;
        $UserResetPasswdRequest->save();

        $this->core->auth()->authenticate($user->id);
        return $this->response->redirect();
    }


    /**
     * Activate Account
     *
     * @Return mixed
     *
     * @Route("/activate/{token:[0-9a-zA-Z_]+}", method = {"GET","POST"}, name="activate-account")
     * @Acl(roles="Guest")
     */
    public function activateAction($activateKey){

        $this->view->not_render_breadcrums = true;

        $this->getDI()->set('flash', function(){
            return new FlashSesion(array(
                'error' => 'alert alert-danger',
                'success' => 'alert alert-success',
                'notice' => 'alert alert-success',
            ));
        });

        if(User::getViewer()->id){
            return $this->response->redirect();
        }
        $userModel = User::findFirst(array(
            "activate_key='" . $activateKey . "'"
        ));
        if(empty($userModel)){
            $this->flashSession->error("The activate key has expired or is not valid.");
            return;


        }
        if($userModel->status == User::STATUS_ACTIVATED){
            $this->flashSession->error("Your account has been activated already");
            return;
        }else{
            $userModel->status = User::STATUS_ACTIVATED;
            if(!$userModel->save()){
                foreach($userModel->getMessages() as $message){
                    print_r($message);
                }
            }
            $this->flashSession->notice("Your account has been activated.");

        }

    }


    /**
     *  Send email reset password
     *
     * @return void
     */

    protected function _sendResetPasswordEmail($link,$email){

        $mailer = new MandrillEmail();
        $from = $this->getDI()->getConfig()->application->params->mail->no_reply_from;
        $fromName = $this->getDI()->getConfig()->application->params->mail->no_reply_from_name;
        $subject = $this->getDI()->getConfig()->application->params->mail->reset_password_subject;

        $emailBody = 'Someone recently requested a password change for your DECIDE VN account. If this was you, you can set a new password <a href="'.$link.'" target="_blank">here</a>:
                                                                <br>
                                                                <br>
                                                                <center><a style="border-radius:3px;color:white;font-size:15px;padding:14px 7px 14px 7px;max-width:210px;font-family:proxima_nova,\'Open Sans\',\'lucida grande\',\'Segoe UI\',arial,verdana,\'lucida sans unicode\',tahoma,sans-serif;border:1px #1373b5 solid;text-align:center;text-decoration:none;width:210px;margin:6px auto;display:block;background-color:#007ee6" href="'.$link.'" target="_blank">Reset password</a>
                                                                </center>
                                                                <br>If you don\'t want to change your password or didn\'t request this, just ignore and delete this message.
                                                                <br>
                                                                <br>To keep your account secure, please don\'t forward this email to anyone.';

        $content = $mailer->emailContent($link, $emailBody);
        $mailer->sendEmail($from, $fromName, $email, $subject, $content);
    }

    /**
     * Generate a random password
     *
     * @param  integer $length
     * @return string
     */
    protected function _generateToken($length = 30)
    {
        $chars = "abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789";

        return substr(str_shuffle($chars),0,$length);
    }


    /**
     * @param $linky
     * @return image
     */
    protected  function _getFbAvatar($facebookId) {
        $facebookImageLink = 'http://graph.facebook.com/'.$facebookId.'/picture?width=200&height=200';
        $randomName = time();
        //Get facebook avatar and save it to db
        $img = file_get_contents($facebookImageLink);
        $file = UPLOAD_PATH."avatar/" .$randomName . ".jpg";
        file_put_contents($file, $img);
        $imageName = $randomName . ".jpg";
        return $imageName;
    }

    /**
     * Subscribe action.
     *
     * @return void
     *
     * @Route("/subscribe", methods={"GET","POST"}, name="subscribe")
     * @Acl(roles="Guest")
     */
    public function ajaxsubscribeAction(){

        $email = $this->request->getPost('email');

        if($email &&  filter_var($email, FILTER_VALIDATE_EMAIL)){
            $subscribe = Subscribe::find(array(
                'conditions'=>'email=?1',
                'bind'=> array(1=>$email)
            ))->getFirst();

            if(!$subscribe){
                $subscribe = new Subscribe();
                $subscribe->email = $email;
                $subscribe->save();
                $this->_sendSubscriberEmail($email);
                $this->view->message = json_encode(array('success'=>1, 'message'=>'Success'));
            }else{
                $this->view->message = json_encode(array('success'=>2, 'message'=>'Your Email have subscribe'));
            }
        }else{
            $this->view->message = json_encode(array('success'=> 0, 'message'=>'Something wrong'));
        }

    }

    /**
     *  Send email reset password
     *
     * @return void
     */

    protected function _sendSubscriberEmail($email){

        $mailer = new MandrillEmail($this->getDI());
        $from = $this->getDI()->getConfig()->application->params->mail->no_reply_from;
        $fromName = $this->getDI()->getConfig()->application->params->mail->no_reply_from_name;
        $subject = $this->getDI()->getConfig()->application->params->mail->subcribe_email_success;

        $emailBody = 'Thank you for your subscription on our website. We all wish you have an interesting journey with us.
                      <br>If you have any question or feedback, please let us know by sending email to <a href="mailto:support@thinhvoxuan.me" target="_blank">support@thinhvoxuan.me</a>
                      <br>To keep your account secure, please don\'t forward this email to anyone.
                      ';

        $content = $mailer->emailContent("", $emailBody);
        $api_key = $this->getDI()->getConfig()->application->mandrill->key;
        $mailer->sendEmailWithDi($from, $fromName, $email, $subject, $content, $api_key);
    }

}

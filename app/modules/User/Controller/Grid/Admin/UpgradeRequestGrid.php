<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace User\Controller\Grid\Admin;

use Core\Controller\Grid\CoreGrid;
use Engine\Form;
use Engine\Grid\GridItem;
use Phalcon\Db\Column;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Mvc\View;
use User\Model\UserRequest;

/**
 * Role grid.
 *
 * @category  PhalconEye
 * @package   Core\Controller\Grid\Admin
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
class UpgradeRequestGrid extends CoreGrid
{
    /**
     * Get main select builder.
     *
     * @return Builder
     */
    public function getSource()
    {
        $builder = new Builder();
        $builder
            ->from('User\Model\UserRequest')
            ->addFrom('User\Model\User', 'u')
            ->where('User\Model\UserRequest.type = :type: '.
                    'AND User\Model\UserRequest.user_id = u.id '.
                    'AND User\Model\UserRequest.status = :status:',
                array(
                    'type' => UserRequest::UPGRADE_MEMBER_TYPE,
                    'status' => UserRequest::WAITING_STATUS
                )
            );

        return $builder;
    }

    /**
     * Get item action (Edit, Delete, etc).
     *
     * @param GridItem $item One item object.
     *
     * @return array
     */
    public function getItemActions(GridItem $item)
    {
        $id = $item->getObject()['user\Model\UserRequest']->id;

        $actions['Approve'] = [
            'href' => [
                'for' => 'user-request-approve',
                'id' => $id
            ]
        ];

        $actions['Delete'] = [
            'href' => [
//                'for' => 'user-request-reject',
                'for' => 'admin-users-roles',
                'id' => $id
            ],
            'attr' => ['class' => 'grid-action-delete']
        ];

        return $actions;
    }

    /**
     * Initialize grid columns.
     *
     * @return array
     */
    protected function _initColumns()
    {
        $this
            ->addTextColumn('u.username', 'user');
    }
}
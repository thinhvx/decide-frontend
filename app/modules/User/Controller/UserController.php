<?php
/**
 * Created by PhpStorm.
 * User: Ngan
 * Date: 8/7/14
 * Time: 7:19 AM
 */

namespace User\Controller;
use Base\Controller\FrontendBaseController;
use Communication\Model\Notification;
use Blog\Model\Post;
use Engine\Helper;
use Core\Controller\AbstractController;
use User\Form\Profile\ChangePassword;
use User\Form\Profile\Profile as ProfileForm;
use User\Form\Profile\Avatar as AvatarForm;
use User\Form\Profile\ChangePassword as ChangePasswordForm;
use User\Model\Profile;
use User\Model\Role;
use Phalcon\Db\Column;
use Phalcon\Mvc\Url;
use User\Model\User;
use Phalcon\Flash\Session as FlashSesion;
use User\Model\UserRequest;

/**
 * Profile controller.
 *
 * @category PhalconEye\Module
 * @package  Controller
 *
 * @RoutePrefix("/user", name="user-controller")
 */
class UserController extends FrontendBaseController
{
    /**
     * View action.
     *
     * @param $username
     * @return mixed
     *
     * @Route("/{username:[0-9a-zA-Z_.\-]+}", methods={"GET", "POST"}, name="user-profile")
     * @Acl(roles="Guest")
     */
    public function viewAction($username)
    {
//        $this->view->username = $username;
        $user = User::findFirst(array("username = '$username' "));

        if($user){
            $this->view->currentUser = $user;
            $this->view->profileUser = Profile::getProfileUser($user);
            $this->view->numberOfPost = count(Post::getPublicPostFromUser($user));
            $this->view->pick('User/view');
        }else{
            echo 'not found user';
            die;
        }
    }

    /**
     * Request Upgrade
     * @return mixed
     *
     * @Route("/request-upgrade", methods={"POST"}, name="request-upgrade")
     * @Acl(roles="User")
     */
    public function requestUpgradeAction() {
        $this->view->disable();
        if (User::getViewer()->getRole()->name != User::ROLE_USER_NAME) {
            $result = [
                'success' => 0,
                'message' => 'Only user can request upgrade'
            ];
        } else {
            $request = UserRequest::findFirst(array(
                'conditions' => 'user_id = ?1 AND status = ?2',
                'bind' => array(
                    1 => User::getViewer()->getId(),
                    2 => UserRequest::WAITING_STATUS
                )
            ));

            if (!empty($request)) {
                $result = [
                    'success' => 0,
                    'message' => 'You already made a upgrade request'
                ];
            } else {
                $request = new UserRequest();
                $request->user_id = User::getViewer()->getId();
                $request->type = UserRequest::UPGRADE_MEMBER_TYPE;

                if ($request->save()) {
                    $notification = new Notification();
                    $notification->type = Notification::USER_TYPE;
                    $notification->object_id = User::getViewer()->getId();

                    if ($notification->save()) {
                        $result = [
                            'success' => 1,
                            'message' => 'Thanks for your request. We will check it soon'
                        ];
                    } else {
                        $result = [
                            'success' => 0,
                            'message' => 'Sorry. There is something wrong. Contact admin to solve this problem'
                        ];
                    }
                } else {
                    $result = [
                        'success' => 0,
                        'message' => 'Sorry. There is something wrong. Contact admin to solve this problem'
                    ];
                }
            }
        }

        echo json_encode($result);
    }

}
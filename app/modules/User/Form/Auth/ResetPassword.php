<?php
/**
 * Created by PhpStorm.
 * User: ngtrieuvi92
 * Date: 7/30/14
 * Time: 4:28 PM
 */

namespace User\Form\Auth;
use Base\Form\BaseForm;

/**
 *
 * Forgot password request Form
 *
 * @category  Geekup Teamsite
 * @package   User\Form\Auth
 *
 */

class ResetPassword extends BaseForm
{

    /**
     * Add element to Form
     *
     * @return void
     */
    public function initialize(){
        $this
            ->setAttribute('class', 'form-horizontal form-without-legend')
            ->setAttribute('color','none')
            ->setAttribute('is_horizontal','true')
            ->setAttribute('role', 'form');
        $content = $this->addContentFieldSet()
            ->addPassword('newPassword', 'New Password', '', array(), array('parent-class'=>'col-lg-8', 'class' =>'form-control'))
            ->addPassword('repeatPassword','Confirm Password', '', array(), array('parent-class'=>'col-lg-8', 'class' =>'form-control'))
            ->setRequired('newPassword')
            ->setRequired('repeatPassword');

        $this->addHtml("", '<div class="row" ><div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-10 padding-right-30">')
            ->addButton('Submit', 'Submit', true, null, ['icon' => 'fa fa-check'], [])
            ->addHtml('', '</div></div>');
        $this->_setValidation($content);
    }

    /**
     *  Set form Validation
     *
     * @params Fieldset $content Fieldset object.
     *
     * @return void
     */
    protected function _setValidation($content){

    }
}

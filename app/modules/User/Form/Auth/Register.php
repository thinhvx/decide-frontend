<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace User\Form\Auth;

use Base\Form\BaseForm;
use Engine\Form\FieldSet;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;

/**
 * Register form.
 *
 * @category  PhalconEye
 * @package   User\Form\Auth
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
class Register extends BaseForm
{
    /**
     * Add elements to form.
     *
     * @return void
     */
    public function initialize()
    {
        $this
            // ->setTitle('Create a new account')
            ->setAttribute('class', 'form-horizontal form-without-legend')
            ->setAttribute('color','none')
            ->setAttribute('is_horizontal',true)
            ->setAttribute('autocomplete', 'off');

        $personalDetailsFieldset = new FieldSet('personalDetailsFieldset', 'Your personal details', [], []);
        $personalDetailsFieldset->addText('username', 'Username', '', '', array(), array('parent-class' => 'col-lg-8'))
            ->addText(
                'email',
                null,
                null,
                null,
                [],
                ['autocomplete' => 'off', 'parent-class' => 'col-lg-8']
            )
            ->setRequired('username')
            ->setRequired('email');

        $passwordFieldset = new FieldSet('passwordFieldset', 'Your password', [], []);
        $passwordFieldset->addPassword(
            'password',
            null,
            null,
            [],
            ['autocomplete' => 'off', 'parent-class' => 'col-lg-8']
        )
            ->addPassword(
                'repeatPassword',
                'Repeat Password',
                null,
                [],
                ['autocomplete' => 'off', 'parent-class' => 'col-lg-8']
            )
            ->setRequired('password')
            ->setRequired('repeatPassword');

        $newsletterFieldset = new FieldSet('newsletterFieldset','Newsletter', [], []);
        $newsletterFieldset->addCheckbox(
                'subscribe_newsletter',
                'Sign up for newsletter',
                '',
                1,
                true,
                '',
                [],
                ['parent-class' => 'col-lg-8 col-sm-8']);


        $this->addFieldSet($personalDetailsFieldset);
        $this->addFieldSet($passwordFieldset);
        $this->addFieldSet($newsletterFieldset);

        $this->_setValidationPersonalDetails($personalDetailsFieldset);
        $this->_setValidationPassword($passwordFieldset);


        $this->addFooterFieldSet()
        ->addHtml('', '
        <div class="row">
                      <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
                        <button type="submit" class="btn btn-primary">Create an account</button>
                        <button type="button" class="btn btn-default">Cancel</button>
                      </div>
                    </div>
        ');

    }

    /**
     * Set form validation.
     *
     * @param FieldSet $content Fieldset object.
     *
     * @return void
     */
    protected function _setValidationPersonalDetails($content)
    {
        $content->getValidation()
            ->add('username', new StringLength(['min' => 2]))
            ->add('email', new Email());

        $content
            ->setRequired('username')
            ->setRequired('email');
    }
    /**
     * Set form validation.
     *
     * @param FieldSet $content Fieldset object.
     *
     * @return void
     */
    protected function _setValidationPassword($content)
    {
        $content->getValidation()
            ->add('password', new StringLength(['min' => 6]))
            ->add('repeatPassword', new StringLength(['min' => 6]));

        $content
            ->setRequired('password')
            ->setRequired('repeatPassword');

        $this
            ->addFilter('password', self::FILTER_STRING)
            ->addFilter('repeatPassword', self::FILTER_STRING);
    }
}
<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace User\Form\Auth;

use Base\Form\BaseForm;
use Core\Form\CustomCoreForm;

/**
 * Login form.
 *
 * @category  PhalconEye
 * @package   User\Form\Auth
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
class Login extends BaseForm
{
    /**
     * Add elements to form.
     *
     * @return void
     */
    public function initialize()
    {

        $this
//            ->setAttribute('class', 'form-horizontal form-without-legend')
            ->setAttribute('color','none')
//            ->setAttribute('is_horizontal','true')
            ->setAttribute('show_total_errors', true)
            ->setAttribute('role', 'form');

        $this->addContentFieldSet()
            ->addText('login', 'Username', '', '', array(), array('parent-class'=>'col-lg-8 col-md-8', 'class' =>'form-control','autofocus'=>'true', 'placeholder'=>'username'))
            ->addPassword('password', 'Password', '', array(), array('parent-class'=>'col-lg-8 col-md-8', 'placeholder'=>'password'))
            ->setRequired('login')
            ->setRequired('password');

        //@TODO: add Facebook Login button
//        $this
//        ->addHtml("", '<div class="row"><div class="col-lg-8 col-md-offset-3 ">')
//        ->addButtonLink('', 'Forgot password?', ['for'=>"forgotPasswordRequest"], array(), ['class'=>''])
//        ->addHtml('', '</div></div>');
//
//
        $this
//        ->addHtml("", '<div class="row" ><div class="col-lg-12 padding-top-10">')
        ->addButton('save', '<span class="fa fa-lock fa-6"></span>', true, null, ['icon' => 'fa fa-check'], ['class'=>'col-lg-12 col-md-12 col-xs-12 btn btn-primary pull-right'])
//        ->addHtml('', '</div></div>')
    ;

//        $this->addHtml("", '<div class="row">
//                                <div class="col-lg-8 col-md-offset-3 padding-top-5 padding-right-30">
//                                    <hr>
//                                    <div class="login-socio"  style="position: relative">
//                                        <p class="text-muted">or login using:
//                                        <ul class="social-icons" style="position: absolute;top: 0;left: 100px"><li>' );
//        $this->addButtonLink("", "", ['for' => "socialLogin"],array(), ['class'=>'facebook', 'data-original-title'=>'facebook']) ;
//        $this->addHtml("",'</li></ul></p></div></div></div>');

    }

    /**
     * Get layout view path.
     *
     * @return string
     */
    public function getLayoutView()
    {
        return $this->_resolveView(self::LAYOUT_DEFAULT_PATH, 'User');
    }
}
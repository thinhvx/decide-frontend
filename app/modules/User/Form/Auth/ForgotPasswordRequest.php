<?php
/**
 * Created by PhpStorm.
 * User: ngtrieuvi92
 * Date: 7/30/14
 * Time: 4:28 PM
 */

namespace User\Form\Auth;
use Base\Form\BaseForm;
use Phalcon\Validation\Validator\Email;

/**
 *
 * Forgot password request form
 *
 * @category  Geekup Teamsite
 * @package   User\Form\Auth
 *
 */

class ForgotPasswordRequest extends BaseForm
{

    /**
     * Add element to Form
     *
     * @return void
     */
    public function initialize(){
        $this
           // ->setTitle('Forgot your password?')
            ->setAttribute('class', 'form-horizontal form-without-legend forgotPasswordForm')
            ->setAttribute('color','white')
            ->setAttribute('is_horizontal',true);

        $content = $this->addContentFieldSet()
            ->addText('email','Email address','','',array(), array('parent-class'=>'col-lg-8', 'class' =>'form-control'))
            ->setRequired('email');
        $this->addHtml("", '<div class="row" ><div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-10 padding-right-30">')
            ->addButton('Send', 'Send', true, null, ['icon' => 'fa fa-check'], [])
            ->addHtml('', '</div></div>');

        $this->_setValidation($content);
    }

    /**
     *  Set form Validation
     *
     * @params Fieldset $content Fieldset object.
     *
     * @return void
     */
    protected function _setValidation($content){
        $content->getValidation()
            ->add('email',new Email());
        $content->setRequired('email');
    }
}

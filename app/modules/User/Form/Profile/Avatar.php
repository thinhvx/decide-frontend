<?php
/**
 * Created by PhpStorm.
 * User: Ngan
 * Date: 8/7/14
 * Time: 7:34 AM
 */

namespace User\Form\Profile;


use Base\Form\FileBaseForm;


/**
 * Profle form.
 *
 * @category  PhalconEye
 * @package   User\Form\Auth
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */


class Avatar extends FileBaseForm {


    /**
     * Add elements to form.
     *
     * @return void
     */
    public function initialize()
    {
        $this

            ->setAction(['for'=>'edit-profile'])
            ->setAttribute('class', 'form-horizontal form-without-legend')
            ->setAttribute('color','none')
            ->setAttribute('is_horizontal',false)
            ->setAttribute('autocomplete', 'off');

        $this->addFooterFieldSet()
            ->addHtml('', '
        <div class="row">
                      <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
                        <button type="submit" class="btn btn-primary">Update your profile</button>
                        <button type="button" class="btn btn-default">Cancel</button>
                      </div>
                    </div>
        ');

    }



} 
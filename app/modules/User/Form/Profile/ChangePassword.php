<?php
/**
 * Created by PhpStorm.
 * User: Ngan
 * Date: 8/7/14
 * Time: 7:34 AM
 */

namespace User\Form\Profile;


use Base\Form\FileBaseForm;
use Phalcon\Validation\Validator\StringLength;


/**
 * Profle form.
 *
 * @category  PhalconEye
 * @package   User\Form\Auth
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */


class ChangePassword extends FileBaseForm {


    /**
     * Add elements to form.
     *
     * @return void
     */
    public function initialize()
    {
        $this

            ->setAction(['for'=>'view-profile'])
            //->setAttribute('class', 'form-horizontal form-without-legend')
            ->setAttribute('color','none')
            ->setAttribute('is_horizontal',false)
            ->setAttribute('hasTitle', false)
            ->setAttribute('autocomplete', 'off');

        $this->addContentFieldSet()
            ->addHidden('context', 'Password', array(), array('parent-class' => 'col-lg-8'))
            ->addPassword('password', 'Current Password', '', array(), array('parent-class' => 'col-lg-8'))
            ->addPassword('newPassword', 'New Password', '', array(), array('parent-class' => 'col-lg-8'))
            ->addPassword('repeatNewPassword', 'Repeat New Password', '', array(), array('parent-class' => 'col-lg-8'))
            ->setRequired('password')
            ->setRequired('newPassword')
            ->setRequired('repeatNewPassword');


        $this->addActionButton('save', 'Change Password', true, '', [], ["class"=>"btn btn-primary"]);
        $this->addActionButton('save', 'Cancel', false, '', [], ["class"=>"btn btn-default"]);
        $this->setAttribute('cssFormAction', 'form-action-transparent form-group');

    }

    protected function _setValidationPassword($content)
    {
        $content->getValidation()
            ->add('password', new StringLength(['min' => 6]))
            ->add('newPassword', new StringLength(['min' => 6]))
            ->add('repeatNewPassword', new StringLength(['min' => 6]));

        $content
            ->setRequired('password')
            ->setRequired('newPassword')
            ->setRequired('repeatNewPassword');

        $this
            ->addFilter('password', self::FILTER_STRING)
            ->addFilter('repeatNewPassword', self::FILTER_STRING);
    }

} 
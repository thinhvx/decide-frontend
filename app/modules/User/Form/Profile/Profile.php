<?php
/**
 * Created by PhpStorm.
 * User: Ngan
 * Date: 8/7/14
 * Time: 7:34 AM
 */

namespace User\Form\Profile;


use Base\Form\FileBaseForm;


/**
 * Profle form.
 *
 * @category  PhalconEye
 * @package   User\Form\Auth
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */


class Profile extends FileBaseForm {


    /**
     * Add elements to form.
     *
     * @return void
     */
    public function initialize()
    {
        $this
             ->setTitle('Edit your profile')
            ->setAction(['for'=>'view-profile'])
            //->setAttribute('class', 'form-horizontal form-without-legend')
            ->setAttribute('color','none')
            ->setAttribute('hasTitle', false)
            ->setAttribute('is_horizontal',false)
            ->setAttribute('autocomplete', 'off');


        $this->addContentFieldSet()
            ->addHidden('context', 'Profile', array(), array('parent-class' => 'col-lg-8'))
            ->addText('firstname', 'First name', '', '', array(), array('parent-class' => 'col-lg-8'))
            ->addText('lastname', 'Last name', '', '', array(), array('parent-class' => 'col-lg-8'))
            ->addText('phonenumber', 'Mobile Number', null, null, array(), array('parent-class'=> 'col-lg-8') )
            ->addText('interest', 'Interests', null, null, [], array('parent-class'=>'col-lg-8'))
            ->addText('occupation', 'Occupation', null, null, [], array('parent-class'=>'col-lg-8'))
            ->addTextArea('about_me', 'About me', null, null, [], array('parent-class'=>'col-lg-8'))
            ->addText('website', 'Web Url', null, null, [], array('parent-class'=>'col-lg-8'));



        $this->addActionButton('save', 'Save', true, '', [], ["class"=>"btn btn-primary"]);
        $this->addActionButton('save', 'Cancel', false, '', [], ["class"=>"btn btn-default"]);
        $this->setAttribute('cssFormAction', 'form-action-transparent form-group');


        $this->addFooterFieldSet();


    }



} 
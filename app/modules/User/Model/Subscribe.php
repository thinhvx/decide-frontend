<?php
/**
 * Created by PhpStorm.
 * User: Ngan
 * Date: 8/7/14
 * Time: 6:54 AM
 */

namespace User\Model;
use Core\Api\Acl;
use Engine\Db\AbstractModel;
use Engine\Db\Model\Behavior\Timestampable;
use Phalcon\DI;
use Phalcon\Mvc\Model\Validator\Email;
use Phalcon\Mvc\Model\Validator\StringLength;
use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 * User.
 *
 * @category  PhalconEye
 * @package   User\Model
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 *
 * @Source("subscribe")
 */
class Subscribe extends  AbstractModel {

    // use trait Timestampable for creation_date and modified_date fields.
    use Timestampable;

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    public $id;

    /**
     * @Column(type="string", nullable=false, column="email", size="255")
     */
    public $email;


    /**
     * Validations and business logic.
     *
     * @return bool
     */
    public function validation()
    {

        if ($this->_errorMessages === null) {
            $this->_errorMessages = [];
        }


        return $this->validationHasFailed() !== true;
    }

} 
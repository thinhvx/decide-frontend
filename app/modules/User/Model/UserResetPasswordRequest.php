<?php
/**
 * Created by PhpStorm.
 * User: ngtrieuvi92
 * Date: 7/30/14
 * Time: 4:54 PM
 */

namespace User\Model;

use Engine\Db\AbstractModel;
use User\Model\User;
use Engine\Db\Model\Behavior\Timestampable;

/**
 * UserResetPasswordRequest.
 *
 * @category  GEEKUp
 * @package   User\Model
 * @Source("user_reset_password_request")
 * @BelongsTo("user_id", '\User\Model\User', "id", {
 *  "alias": "User"
 * })
 */

class UserResetPasswordRequest extends AbstractModel
{
    // use trait Timestampable for creation_date and modified_date fields.
    use Timestampable;

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    public $id;

    /**
     * @Column(type="integer", nullable=false, size="11")
     */
    public $user_id;

    /**
     * @Column(type="string", nullable=false, size="255")
     */
    public $token;
    /**
     * @Column(type="string", nullable=false, size="255")
     */
    public $email;
    /**
     * @Column(type="boolean", nullable=false)
     */
    public $reset;

}

<?php
/**
 * Created by PhpStorm.
 * User: Ngan
 * Date: 8/7/14
 * Time: 6:54 AM
 */

namespace User\Model;
use Core\Api\Acl;
use Engine\Db\AbstractModel;
use Engine\Db\Model\Behavior\Timestampable;
use Phalcon\DI;
use Phalcon\Mvc\Model\Validator\Email;
use Phalcon\Mvc\Model\Validator\StringLength;
use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 * User.
 *
 * @category  PhalconEye
 * @package   User\Model
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 *
 * @Source("profile")
 * @BelongsTo("user_id", '\User\Model\User', "id", {
 *  "alias": "User"
 * })
 */
class Profile extends  AbstractModel {

    const
        /**
         * Cache prefix.
         */
        CACHE_PREFIX = 'role_id_';

    // use trait Timestampable for creation_date and modified_date fields.
    use Timestampable;

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false, column="id", size="11")
     */
    public $id;

    /**
     * @Column(type="integer", nullable=false, column="user_id", size="11")
     */
    public $user_id;

    /**
     * @Index("ix_firstname")
     * @Column(type="string", nullable=false, column="firstname", size="255")
     */
    public $firstname;

    /**
     * @Column(type="string", nullable=false, column="lastname", size="255")
     */
    public $lastname;

    /**
     * @Column(type="string", nullable=true, column="phonenumber", size="255")
     */
    public $phonenumber;

    /**
     * @Column(type="string", nullable=true, column="occupation", size="255")
     */
    public $occupation;

    /**
     * @Column(type="string", nullable=true, column="website", size="255")
     */
    public $website;

    /**
 * @Column(type="string", nullable=true, column="interest", size="255")
 */
    public $interest;

    /**
     * @Column(type="string", nullable=true, column="about_me", size="255")
     */
    public $about_me;

    /**
     * @Column(type="string", nullable=true, column="avatar", size="255")
     */
    public $avatar;

    /**
     * @Column(type="string", nullable=true, column="fb_avatar", size="255", after="social_id")
     */
    public $fb_avatar;

    /**
     * Validations and business logic.
     *
     * @return bool
     */
    public function validation()
    {

        if ($this->_errorMessages === null) {
            $this->_errorMessages = [];
        }


        return $this->validationHasFailed() !== true;
    }

    public static function getProfileUser($user){
        $profile = Profile::findFirst("user_id = $user->id");
        if (!$profile){
            $profile = new Profile();
            $profile->firstname = $user->username;
            $profile->lastname = ' ';
            $profile->user_id = $user->id;
            $profile->save();
        }
        return $profile;
    }

} 
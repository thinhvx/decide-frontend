{{ form.openTag() }}
<div>
    {% if form.getTitle() or form.getDescription() %}
        <div class="form_header">
            <h3>{{ form.getTitle() }}</h3>

            <p>{{ form.getDescription() }}</p>
        </div>
    {% endif %}
    {{ partial(form.getErrorsView(), ['form': form]) }}
    {{ partial(form.getNoticesView(), ['form': form]) }}

    <div class="form_elements">
        {% for element in form.getAll() %}
            {{ partial(form.getElementView(), ['element': element]) }}
        {% endfor %}
    </div>
    <div class="clear"></div>

    {% if form.useToken() %}
        <input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}">
    {% endif %}
</div>
{{ form.closeTag() }}
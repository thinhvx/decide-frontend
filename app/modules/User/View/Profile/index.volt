{% extends "../../Base/View/layouts/frontend.volt" %}


{% block content %}
    <div style="padding: 10px"></div>
    <div class="col-md-12 geekup-tab">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#profile-overview" data-toggle="tab">Overview</a></li>
            <li class=""><a href="#edit-profile" data-toggle="tab">My Account</a></li>

        </ul>
        <div class="tab-content">
            <div class="tab-pane row fade active in" id="profile-overview">

                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled profile-nav">
                            <li>
                                <img src="{{ helper('user','user').getAvatarPath(profile.user_id) }}" class="img-responsive" alt="">
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-8 profile-info">
                                <h2 class="profileFullName">{{ profile.firstname  }} {{ profile.lastname }}</h2>
                                <h4 class="thin profileOccupation">{{ profile.occupation }}</h4>
                                <p>
                                    {{ profile.about_me }}
                                </p>
                                <p>
                                    <a class="linkHasColor" href="#">{{ profile.website }} </a>
                                </p>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <dl>
                                            <dt>Email</dt>
                                            <dd><i class="fa fa-envelope"></i>&nbsp;{{ userModel.email }}</dd>
                                        </dl>
                                        <dl class="margin-sm-bottom">
                                            <dt>Mobile Number</dt>
                                            <dd><i class="fa fa-phone"></i>{{ profile.phonenumber }}</dd>
                                        </dl>
                                        <dl>
                                            <dt>Interest</dt>
                                            <dd><i class="fa fa-heart"></i> {{ profile.interest }}</dd>
                                        </dl>

                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="tab-pane row fade " id="edit-profile">

                    <div class="row profile-account">
                        <div class="col-md-3">
                            <ul class="ver-inline-menu tabbable margin-bottom-10">
                                <li class="active">
                                    <a data-toggle="tab" href="{{ url(['for':'view-profile']) }}#tab_1-1">
                                        <i class="fa fa-cog"></i> Personal info </a>
                                                    <span class="after">
                                                    </span>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#tab_2-2">
                                        <i class="fa fa-picture-o"></i> Change Avatar </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#tab_3-3">
                                        <i class="fa fa-lock"></i> Change Password </a>
                                </li>

                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="tab-content">
                                <div id="tab_1-1" class="tab-pane active">
                                    <div>{{ flash.output() }}</div>
                                    {{ form.render() }}
                                </div>
                                <div id="tab_2-2" class="tab-pane">

                                    <form action="{{ url(['for': 'view-profile']) }}" class="" color="none" is_horizontal="" autocomplete="off" method="POST" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="{{ helper('user','user').getAvatarPath(profile.user_id) }}" alt="" class="img-responsive">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                </div>
                                                <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new">
                                                        Select image
                                                    </span>
                                                    <span class="fileinput-exists">
                                                        Change
                                                    </span>
                                                    <input type="file" name="Profile[avatar]">
                                                </span>
                                                    <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove</a>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-group margin-top-10">

                                            <button class="btn btn-primary">Submit</button>
                                            <a href="{{ url(['for' : 'view-profile']) }}"><button type="button" class="btn btn-default">Cancel</button></a>

                                        </div>
                                    </form>
                                </div>
                                <div id="tab_3-3" class="tab-pane">
                                    <div>{{ flash.output() }}</div>
                                    {{ changePasswordForm.render() }}
                                </div>
                                <div id="tab_4-4" class="tab-pane">
                                    <form action="#">
                                        <table class="table table-bordered table-striped">
                                            <tbody><tr>
                                                <td>
                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus..
                                                </td>
                                                <td>
                                                    <label class="uniform-inline">
                                                        <div class="radio"><span><input type="radio" name="optionsRadios1" value="option1"></span></div>
                                                        Yes </label>
                                                    <label class="uniform-inline">
                                                        <div class="radio"><span class="checked"><input type="radio" name="optionsRadios1" value="option2" checked=""></span></div>
                                                        No </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Enim eiusmod high life accusamus terry richardson ad squid wolf moon
                                                </td>
                                                <td>
                                                    <label class="uniform-inline">
                                                        <div class="checker"><span><input type="checkbox" value=""></span></div> Yes </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Enim eiusmod high life accusamus terry richardson ad squid wolf moon
                                                </td>
                                                <td>
                                                    <label class="uniform-inline">
                                                        <div class="checker"><span><input type="checkbox" value=""></span></div> Yes </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Enim eiusmod high life accusamus terry richardson ad squid wolf moon
                                                </td>
                                                <td>
                                                    <label class="uniform-inline">
                                                        <div class="checker"><span><input type="checkbox" value=""></span></div> Yes </label>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                        <!--end profile-settings-->
                                        <div class="margin-top-10">
                                            <a href="#" class="btn green">
                                                Save Changes </a>
                                            <a href="#" class="btn default">
                                                Cancel </a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--end col-md-9-->
                    </div>

            </div>

        </div>
    </div>
{% endblock %}
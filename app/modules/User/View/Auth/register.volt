{#
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
#}

{% extends "../../Base/View/layouts/frontend.volt" %}

{% block content %}
    <div class="sidebar col-md-3 col-sm-3">

    </div>
    <div class="col-lg-9 col-md-9 col-sm-9 register-container">
        <h1>Create an account</h1>
        <div class="content-form-page">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    {{ flash.output() }}
                    {{ form.render() }}
                    {#<div class="form-group has-error">#}
                        {#<label class="control-label col-md-3">Input with error</label>#}
                        {#<div class="col-md-4">#}
                            {#<div class="input-icon right">#}
                                {#<i class="fa fa-warning tooltips" data-original-title="please write a valid email"></i>#}
                                {#<input type="text" class="form-control">#}
                            {#</div>#}
                        {#</div>#}
                    {#</div>#}
                </div>
                <div class="col-md-4 col-sm-4 pull-right">

                </div>
            </div>
        </div>
    </div>
{% endblock %}

{#
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
#}
{% extends "../../Base/View/layouts/frontend.volt" %}

{% block title %}{{ 'Login'|i18n }}{% endblock %}
{% block content %}
    <div class="sidebar col-md-3 col-sm-3">

    </div>
    <div class="col-md-9 col-sm-9">
        <div class="content-form-page">
            <div class="row">

                <div class="col-md-8 col-sm-8">
                    <h1 class="form-title">Reset Your Password</h1>
                    <p>Please enter a new password for your account.</p>
                    {{ form.render() }}
                </div>
                <div class="col-md-4 col-sm-4 pull-right">

                </div>
            </div>
        </div>
    </div>
{% endblock %}

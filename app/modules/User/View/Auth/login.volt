{#
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
#}

{% extends "../../Base/View/layouts/frontend.volt" %}

{% block content %}
	<div class="col-md-9 col-sm-9  login-container">
		<div class="content-form-page">
			<div class="row">
				<div class="col-md-7 col-sm-7 login">
					<h1 class="form-title">Login</h1>
					{{ form.render() }}
				</div>
			</div>
		</div>
   </div>
   <script type="text/javascript">
        function onSocialButtonLoginClick(provider){
            window.location.href = '{{ url(['for':'socialLogin'])}}?provider=' + provider;
        }
   </script>


{% endblock %}


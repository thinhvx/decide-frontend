
{% extends "../../Base/View/layouts/empty.volt" %}
{% block head %}
    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0 0;
            background-color: lightgray;
        }

        .container-fluid {
            height: 100%;
            display: table;
            width: 100%;
            padding-right: 0;
            padding-left: 0;
        }

        .row-fluid {
            height: 100%;
            display: table-cell;
            vertical-align: middle;
            width: 250px;
        }

        .centering {
            float: none;
            margin: 0 auto;
            background-color: white;
            padding: 10px;
            padding-bottom: 1px;
            border-radius: 2px !important;
        }
        .centering h2{
            text-align: center;
            margin-top: 15px;
        }

        label.control-label{
            padding-left: 4px;
        }
        input.form-control:focus{
            border: 1px solid #e45000;
            /*color: red;*/
        }
    </style>
{% endblock %}
{% block content %}
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="offset3 span6 centering">
                <h2>Login</h2>
                <div>
                    {{ form.render() }}
                </div>
            </div>
        </div>
    </div>
{% endblock %}




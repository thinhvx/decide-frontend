
{% extends "../../Base/View/layouts/empty.volt" %}
{% block head %}
    <style>
        html, body, .container{
            height: 100%;
            background-color: #F0EEEE;
        }

        .container{
            display: table;
            /*margin-top: 100px;*/
        }

        .redborder {
            border:2px solid #f96145;
            border-radius:2px;
        }

        .hidden {
            display: none;
        }

        .visible {
            display: normal;
        }

        .colored {
            background-color: #F0EEEE;
        }

        .row {
            display: table-row;
            padding: 20px 0px;
        }

        .bigicon {
            font-size: 97px;
            color: #f96145;
        }

        .contcustom {
            text-align: center;
            width: 300px;
            border-radius: 0.5rem;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: 10px auto;
            background-color: white;
            padding: 20px;
        }

        input {
            width: 100%;
            margin-bottom: 17px;
            padding: 15px;
            background-color: #ECF4F4;
            border-radius: 2px;
            border: none;
        }

        h2 {
            margin-top: 0px;
            margin-bottom: 20px;
            font-weight: bold;
            color: #ABABAB;
        }

        .btn {
            border-radius: 2px;
            padding: 10px;
        }

        .med {
            font-size: 27px;
            color: white;
        }

        .medhidden {
            font-size: 27px;
            color: #f96145;
            padding: 10px;
            width:100%;
        }

        .wide {
            background-color: #8EB7E4;
            width: 100%;
            -webkit-border-top-right-radius: 0;
            -webkit-border-bottom-right-radius: 0;
            -moz-border-radius-topright: 0;
            -moz-border-radius-bottomright: 0;
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }
        .fa-6{
            font-size: 27px;
            /*line-height: 1 !important;*/
        }

        .fa {
            line-height: 1;
        }
        img.logo-img{
            width: 192px;
            height: 48px;
            margin-bottom: 0px;
            margin-top: 0px;
        }
    </style>
{% endblock %}
{% block content %}

    <div class="container text-center">
        <div class="row colored">
            <div id="contentdiv" class="contcustom">
                {#<span class="fa fa-geekup bigicon"></span>#}
                <img alt="GEEKHub" src="/metronic/frontend/layout/img/logos/logo-shop-green.png" class="logo-img"/>
                <div>
                    {{ form.render() }}
                </div>
            </div>
        </div>
    </div>
{% endblock %}




<?php
/**
 * Created by PhpStorm.
 * User: ngtrieuvi92
 * Date: 8/21/14
 * Time: 4:15 PM
 */
namespace User\Helper;


use Engine\Helper;

class Firebase extends Helper{

    /**
     * Generator firebase token
     *
     * @params:  username
     *
     * @Return: token
     */
    public function generateFireBaseToken($usernameID){
        $secret = $this->getDI()->getConfig()->application->params->firebase->key;
        $tokenGen = new \Services_FirebaseTokenGenerator($secret);
        $token = $tokenGen->createToken(array("id" => $usernameID));

    }
}
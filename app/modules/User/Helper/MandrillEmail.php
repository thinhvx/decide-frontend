<?php
/**
 * Created by PhpStorm.
 * User: ngtrieuvi92
 * Date: 7/31/14
 * Time: 3:16 PM
 */

namespace User\Helper;


use Engine\Helper;
use Mandrill;

class MandrillEmail extends Helper
{
    /**
     * Send reset password request
     *
     * @Return void
     */
    public function __construct(){

    }

    public  function sendEmail($from, $formName, $to, $subject, $content){
        $api_key =   $this->getDI()->getConfig()->application->mandrill->key;
        $this->sendEmailWithDi($from, $formName, $to, $subject, $content, $api_key);
    }

    public  function sendEmailWithDi($from, $formName, $to, $subject, $content, $api_key){
        try {
            $mandrill = new Mandrill($api_key);
            $message = array(
                'html' => $content,
                'subject' => $subject,
                'from_email' => $from,
                'from_name' => $formName,
                'to' => array(
                    array(
                        'email' => $to,
                        'type' => 'to'
                    )
                ),
                'headers' => array('Reply-To' => 'no-reply@geekhub.vn')
            );
            $result = $mandrill->messages->send($message);
            return $result;

        } catch(Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
            throw $e;
        }
    }

    /**
     *  Forgot passwors mail temp
     */

    public static function emailContent($link, $content){
        $html =
            '
            <div id=":1ha" class="ii gt m1478af14b88aaca4 adP adO">
    <div id=":1h9" class="a3s" style="overflow: hidden;"><u></u>
        <div style="padding:0;width:100%!important;margin:0" marginheight="0" marginwidth="0">
            <center>
                <table cellpadding="8" cellspacing="0" style="padding:0;width:100%!important;background:#ffffff;margin:0;background-color:#ffffff" border="0">
                    <tbody>
                        <tr>
                            <td valign="top">
                                <table cellpadding="0" cellspacing="0" style="border-radius:4px;border:1px #dceaf5 solid" border="0" align="center">
                                    <tbody>
                                        <tr>
                                            <td colspan="3" height="6"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" style="line-height:25px" border="0" align="center">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="3" height="30"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="36"></td>
                                                            <td width="454" align="left" style="color:#444444;border-collapse:collapse;font-size:11pt;font-family:proxima_nova,\'Open Sans\',\'Lucida Grande\',\'Segoe UI\',Arial,Verdana,\'Lucida Sans Unicode\',Tahoma,\'Sans Serif\';max-width:454px" valign="top">Hi there,
                                                                <br>
                                                                <br>
                                                                '. $content . '
                                                                <br>
                                                                <br>Thanks!
                                                                <br>- GEEKHub Team</td>
                                                            <td width="36"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" height="36"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table cellpadding="0" cellspacing="0" align="center" border="0">
                                    <tbody>
                                        <tr>
                                            <td height="10"></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:0;border-collapse:collapse">
                                                <table cellpadding="0" cellspacing="0" align="center" border="0">
                                                    <tbody>
                                                        <tr style=\'color:#a8b9c6;font-size:11px;font-family:proxima_nova,"Open Sans","Lucida Grande","Segoe UI",Arial,Verdana,"Lucida Sans Unicode",Tahoma,"Sans Serif"\'>
                                                            <td width="400" align="left"></td>
                                                            <td width="128" align="right">© 2014 GEEKHub</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </center>
        </div>

    </div>
    <div class="yj6qo"></div>
</div>
           ';
        return $html;
    }


}



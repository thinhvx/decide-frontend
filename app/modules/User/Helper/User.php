<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace User\Helper;

use Engine\Helper;
use Phalcon\DI;
use Phalcon\Tag;
use User\Form\Profile\Profile;
use User\Model\User as UserModel;

/**
 * Viewer helper.
 *
 * @category  PhalconEye
 * @package   User\Helper
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
class User extends Helper
{
    /**
     * Get current user (viewer).
     *
     * @return UserModel
     */
    public function current()
    {
        return UserModel::getViewer();
    }

    /**
     * Get some user.
     *
     * @param int $id User identity.
     *
     * @return UserModel
     */
    public function get($id)
    {
        return UserModel::findFirstById($id);
    }

    /**
     * Is current viewer is user.
     *
     * @return bool
     */
    public function isUser()
    {
        return (bool)UserModel::getViewer()->id;
    }

    /**
     * Uppercase first character
     *
     * @param $lusername
     * @return string
     */
    public function upperUsername($lusername){
        return ucfirst($lusername);
    }

    public function getCurrentUserAvatar(){
        $user = UserModel::getViewer();
        return $this->getAvatarPath($user->id);
    }
    /**
     *  Get Avatar Path of current user
     *
     * @return string
     */
    public function getAvatarPath($user_id){

        $profile = \User\Model\Profile::find('user_id='.$user_id)->getFirst();
        $avatar_name = "default.png";
        if($profile){
            if($profile->avatar != null){

                $avatar_name = $profile->avatar;
            }else{

                if($profile->fb_avatar != null){
                    return $profile->fb_avatar;
                }
            }
        }
        if($_SERVER['SERVER_PORT'] != 80){
            $baserUrl =  'http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$this->getDI()->getConfig()->application->baseUrl;
        } else
        {
            $baserUrl =  'http://'.$_SERVER['SERVER_NAME'].$this->getDI()->getConfig()->application->baseUrl;
        }
        return $baserUrl . "upload/avatar/".$avatar_name;
    }




    public function generateAvatarPath($avatar_name){
        if($_SERVER['SERVER_PORT'] != 80){
            $baserUrl =  'http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$this->getDI()->getConfig()->application->baseUrl;
        } else
        {
            $baserUrl =  'http://'.$_SERVER['SERVER_NAME'].$this->getDI()->getConfig()->application->baseUrl;
        }
        return $baserUrl . "upload/avatar/".$avatar_name;
    }

    public function formatDateTime($datetime){
        $that_date = date_parse_from_format('Y-m-d H:i:s', $datetime);
        $year = $that_date['year'];
        $month = $that_date['month'];
        $date = $that_date['day'];
        return "$date/$month/$year";
    }

    public function isLogin(){
        $user = UserModel::getViewer();
        return ($user->id);
    }
};
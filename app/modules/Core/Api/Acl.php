<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace Core\Api;

use Core\Model\Access;
use Engine\Api\AbstractApi;
use Engine\Application;
use Engine\Behaviour\DIBehaviour;
use Engine\Exception;
use Phalcon\Acl\Adapter\Memory as AclMemory;
use Phalcon\Acl\Resource as AclResource;
use Phalcon\Acl as PhalconAcl;
use Phalcon\DI;
use Phalcon\Events\Event as PhalconEvent;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Text;
use User\Model\Role;
use User\Model\User;
use Engine\Acl\Adapter\Database as DBAdapter;

/**
 * Core API Acl.
 *
 * @category  PhalconEye
 * @package   Core\Api
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
class Acl extends AbstractApi
{
    const
        /**
         * Acl cache key.
         */
        CACHE_KEY_ACL = "acl_data";

    const
        /**
         * Role - ADMIN.
         */
        DEFAULT_ROLE_ADMIN = 'admin',

        /**
         * Role - USER.
         */
        DEFAULT_ROLE_USER = 'user',

        /**
         * Role - GUEST.
         */
        DEFAULT_ROLE_GUEST = 'guest',
        /**
        * Role - GUEST.
        */
        DEFAULT_ROLE_MEMBER = 'member',
        /**
        * Role - GUEST.
        */
        DEFAULT_ROLE_CONTENT_MANAGER = 'content_manager';

    const
        /**
         * Admin area name in ACL.
         */
        ACL_ADMIN_AREA = 'AdminArea';

    const
        ACL_AUTH_MODULE = 'User\Controller\AuthController';

    const
        ACL_DATA_FILE_PATH = '/app/var/data/security/acl.data';

    /**
     * Acl adapter.
     *
     * @var AclMemory
     */
    protected $_acl;

    /**
     * Get acl system.
     *
     * @return AclMemory
     */
    public function getAcl() {
        if (!$this->_acl) {
            $connection = $this->getDI()->getDb();

            $acl = new DBAdapter(array(
                'db' => $connection,
                'roles' => 'roles',
                'rolesInherits' => 'roles_inherits',
                'resources' => 'resources',
                'resourcesAccesses' => 'resources_accesses',
                'accessList' => 'access_list',
            ));

            $acl->setDefaultAction(PhalconAcl::DENY);

            $this->_acl = $acl;
        }

        return $this->_acl;
    }

    public function isAdmin()
    {
        $current_user = User::getViewer();
        return $current_user->role_id == User::ROLE_ADMIN;
    }

    public  function isContentManager(){
        $current_user = User::getViewer();
        return $current_user->role_id == User::ROLE_CONTENT_MANAGER;
    }


    public function couldPost(){
        $current_user = User::getViewer();
        $resourcePath = '\Blog\Controller\IndexController';
        $action = 'createAction';
        return $this->_acl->isAllowed($current_user->getRole()->name, $resourcePath, $action) == PhalconAcl::ALLOW;
    }

    public function isMember()
    {
        $current_user = User::getViewer();
        return $current_user->role_id == User::ROLE_MEMBER;
    }

    /**
     * Wrapper to real isAllowed method.
     *
     * @param string $role     Role name.
     * @param string $resource Resource name.
     * @param string $access   Access name.
     *
     * @return boolean
     */
    public function isAllowed($role, $resource, $access)
    {
        return $this->getAcl()->isAllowed($role, $resource, $access);
    }

    /**
     * Get allowed value.
     *
     * @param string $objectName Object name.
     * @param Role   $role       Role object.
     * @param string $action     Action name.
     *
     * @return null|mixed
     */
    public function getAllowedValue($objectName, Role $role, $action)
    {
        $result = Access::findFirst(
            [
                "conditions" => "object = ?1 AND action = ?2 AND role_id = ?3",
                "bind" => [1 => $objectName, 2 => $action, 3 => $role->id]
            ]
        );

        if ($result) {
            return $result->value;
        }

        return null;
    }

    /**
     * Get acl object.
     *
     * @param string $objectName Object name.
     *
     * @return null|\stdClass
     */
    public function getObject($objectName)
    {
        $object = new \stdClass();
        $object->name = $objectName;
        $object->module = ucfirst(Application::SYSTEM_DEFAULT_MODULE);
        $object->actions = [];
        $object->options = [];

        if ($objectName == self::ACL_ADMIN_AREA) {
            $object->actions = ['access'];

            return $object;
        }

        $objectNameParts = explode('\\', $objectName);
        if (count($objectNameParts) > 1) {
            $object->module = $objectNameParts[1];
        }

        $reader = new \Phalcon\Annotations\Adapter\Memory();
        $reflector = $reader->get($objectName);
        $annotations = $reflector->getClassAnnotations();
        if ($annotations && $annotations->has('Acl')) {
            $annotation = $annotations->get('Acl');

            if ($annotation->hasNamedArgument('actions')) {
                $object->actions = $annotation->getNamedArgument('actions');
            }
            if ($annotation->hasNamedArgument('options')) {
                $object->options = $annotation->getNamedArgument('options');
            }
        } else {
            return null;
        }

        return $object;
    }

    /**
     * Clear acl cache.
     *
     * @return void
     */
    public function clearAcl()
    {
        $this->getDI()->get('cacheData')->delete(self::CACHE_KEY_ACL);
    }

    /**
     * Return an array with every resource registered in the list.
     *
     * @return \Phalcon\Acl\Resource[]
     */
    public function getResources()
    {
        return $this->getAcl()->getResources();
    }

    /**
     * This action is executed before execute any action in the application.
     *
     * @param PhalconEvent $event      Event object.
     * @param Dispatcher   $dispatcher Dispatcher object.
     *
     * @return mixed
     */
    public function beforeDispatch(PhalconEvent $event, Dispatcher $dispatcher)
    {

        $viewer = User::getViewer();
        $acl = $this->getAcl();

        $controller = $dispatcher->getControllerName();
        $namespace = $dispatcher->getNamespaceName();
        $action = $dispatcher->getActionName().'Action';

        $resourcePath = '\\'.$namespace.'\\'.$controller."Controller";

        if ($acl->isAllowed($viewer->getRole()->name, $resourcePath, $action) == PhalconAcl::DENY) {
            $di = DI::getDefault();
            $identity = $di->get('core')->auth()->getIdentity();

            if ($identity) {
                header('Location: /error/404');
            } else {
                header('Location: /login');
            }
        }

        return !$event->isStopped();
    }
}
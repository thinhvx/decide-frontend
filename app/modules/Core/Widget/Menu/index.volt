<div class="header reduce-header">
    <div class="container">
        <a class="site-logo" href="/book"><img src="/metronic/frontend/layout/img/logos/logo-shop-green.png"
                                                         alt="Metronic Shop UI"></a>
        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

        <!-- BEGIN CART -->
        <div class="top-cart-block">
            {#<div class="top-cart-info">#}
                {#<a href="javascript:void(0);" class="top-cart-info-count">0 items</a>#}
                {#<a href="javascript:void(0);" class="top-cart-info-value">$0</a>#}
            {#</div>#}
            {#<i class="fa fa-shopping-cart"></i>#}
            {#<div class="top-cart-content-wrapper">#}
                {#<div class="top-cart-content">#}
                    {#<div class="text-right">#}
                        {#<a href="/book/#" class="btn btn-default">View Cart</a>#}
                        {#<a href="/book/#" class="btn btn-primary">Checkout</a>#}
                    {#</div>#}
                {#</div>#}
            {#</div>#}
        </div>
        <!--END CART -->

        <!-- BEGIN NAVIGATION -->
        <div class="header-navigation">
            <ul>
                <li><a href="/book">Book</a></li>
                <!-- BEGIN TOP SEARCH -->
                <li class="menu-search">
                    <span class="sep"></span>
                    <i class="fa fa-search search-btn"></i>
                    <div class="search-box">
                        <form action="/book/search.html" method="GET">
                            <div class="input-group">
                                <input type="text" name="q" placeholder="Search" class="form-control"/>
                                <span class="input-group-btn">
                                  <button class="btn btn-primary" type="submit">Search</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </li>
                <!-- END TOP SEARCH -->
            </ul>
        </div>
        <!-- END NAVIGATION -->
    </div>
</div>

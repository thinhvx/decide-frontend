{% extends "../../Core/View/layouts/widget.volt" %}
{%- block content -%}
    <div class="pre-header">
        <div class="container geekup-pre-header" >
            <div class="row" >
                <!-- BEGIN TOP BAR LEFT PART -->
                <div class="col-md-5 col-sm-5 additional-shop-info">
                    <ul class="list-unstyled list-inline">
                        <li><i class="fa fa-phone"></i><span> (+84) 905 037 468 </span></li>
                        <li><i class="fa fa-envelope-o"></i><span>thinhvoxuan@gmail.com</span></li>
                    </ul>
                </div>
                <!-- END TOP BAR LEFT PART -->
                <!-- BEGIN TOP BAR MENU -->
                <div class=" col-lg-md-7 col-md-7 col-sm-7 additional-nav geekup-login-nav">
                    <ul class="list-unstyled list-inline pull-right">
						<li class="alpha-label">
							<span data-toggle="modal" data-target="#alpha-model">Alpha</span>
						</li>
                        {% if not helper('user', 'user').isUser() %}
                            <li>
                                <a href="{{ url('login') }}">{{ 'Login' |i18n }}</a>&nbsp;
                            </li>
                            <li>
                                <a href="{{ url('register') }}">{{ 'Register' |i18n }}</a>
                            </li>
                        {% else %}
                            <li>
                                {#<a href="{{ url(['for':'view-profile']) }}">{{ helper('user','user').upperUsername(helper('user', 'user').current().username)  }} </a>#}
                                <a href="{{ url(['for':'view-profile']) }}">{{ helper('user', 'user').current().username  }} </a>
                            </li>
                            {% if helper('acl', 'core').isAllowed('AdminArea', 'access') %}
                                <li>
                                    <a href="{{ url('admin') }}">{{ 'Admin panel' |i18n }}</a>
                                </li>
                            {% endif %}
                            {% if helper('acl', 'core').isAdmin() or helper('acl', 'core').isContentManager() %}
                                <li>
                                    <a href="{{ url('manager') }}">{{ 'Manager panel' |i18n }}</a>
                                </li>
                            {% endif %}
                            <li>
                                <a href="{{ url('logout') }}">{{ 'Logout' |i18n }}</a>
                            </li>

                        {% endif %}
                    </ul>
                </div>
                <!-- END TOP BAR MENU -->
            </div>
        </div>
    </div>
{%- endblock -%}

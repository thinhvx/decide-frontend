<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace Core\Helper;

use Engine\Helper;
use Phalcon\DI;
use Phalcon\Tag;
use Engine\Helper\Url;

/**
 * ACL helper.
 *
 * @category  PhalconEye
 * @package   Core\Helper
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
class Title extends Helper
{
    /**
     * Using this function to set title of page
     *
     * @param mixed $breadcrumbs_params
     * @return string
     */

    private $_defaultName = "Decide - What to Buy, When to Buy";

    public function setPageTitle($params= "")
    {
        if ($params != ""){
            $params = array_merge([], (array)$params);
        }
        $arrayData = $this->getCurrentUrl();
        $arrayData = $this->customTitle($arrayData, $params);
        return $this->generateTitle($arrayData);
    }


    public function mappingUrl($arrayUrl)
    {
        $resultArray = [];
        foreach ($arrayUrl as $eachElement) {
            $resultArray[] = ucfirst($eachElement);
        };
        return $resultArray;
    }

    public function getCurrentUrl()
    {
        $url = Url::getInstance($this->getDI());
        $splitArray = explode("/", $url->currentUrl());
        array_shift($splitArray);
        $splitArray = $this->mappingUrl($splitArray);
        return $splitArray;;
    }

    public function generateTitle($arrayData){
        if ( !is_array($arrayData) || sizeof($arrayData) == 0){
            return $this->_defaultName;
        }
        if(sizeof($arrayData) == 1 ){
            return $this->_defaultName.' | '.$arrayData[0];
        }
        return  implode(' | ',$arrayData);

    }

    public function customTitle($arrayUrl, $arrayName){
        if ( !is_array($arrayName) || sizeof($arrayName) == 0){
            return $arrayUrl;
        }
        $arrayUrl = array_reverse($arrayUrl);
        $arrayName = array_reverse($arrayName);
        foreach($arrayName as $key => $value){
            $arrayUrl[$key] = $value;
        }
        return array_reverse($arrayUrl);
    }



}
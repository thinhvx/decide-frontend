<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
*/

namespace Core\Helper;

use Engine\Helper;
use Phalcon\DI;
use Phalcon\Tag;
use Engine\Helper\Url;

/**
 * ACL helper.
 *
 * @category  PhalconEye
 * @package   Core\Helper
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
class Breadcrumb extends Helper
{

    public static $LINK = "LINK";
    public static $NAME = "NAME";
    private $HOME = ["LINK" => '/', "NAME" => 'Home'];

    /**
     * Using this function to generating breadcrumb in view
     *
     * If do not set 'breadcrumb' in view, it would generation from current url
     * currentURl: domain/blog
     * -----------
     * Home > Blog
     * -----------
     *
     *
     * If breadcrumb is an string, last element of breadcrumb be replaced
     * currentURL: domain/page/2
     * (in controller) $this->view->breadcrumb = "Blog"
     * ----------
     * Home>Page>Blog
     * ----------
     *
     *
     * If breadcrumb is an array, last n element of breadcrumb should be replaced
     * currentURL: domain/page/2
     * (in controller) $this->view->breadcrumb = ["Custom Page","Blog"]
     * -----------
     * Home>Custom Page>Blog
     * -----------
     * @param mixed $breadcrumbs_params , boolean renderBreadcrums
     * @param $not_render_breadcrums
     * @return string
     */
    public function showBreadcrumb($breadcrumbs_params= "",$not_render_breadcrums)
    {
        if($not_render_breadcrums){
            return;
        }
        if ($breadcrumbs_params != ""){
            $breadcrumbs_params = array_merge([], (array)$breadcrumbs_params);
        }
        $arrayData = $this->getArrayBreadcrumb();
        $arrayData = $this->customBreadcrumb($arrayData, $breadcrumbs_params);
        $lastElement = array_pop($arrayData);
        $breadcrumbs = array_reduce($arrayData, function($x, $y){ return $x.$this->generateElement($y);});
        $breadcrumbs = $breadcrumbs . $this->generateLastElement($lastElement);
        return $this->generateWrapper($breadcrumbs);
    }


    public function mappingUrl($arrayUrl)
    {
        $baseLink = "";
        $resultArray = [];
        foreach ($arrayUrl as $eachElement) {
            $baseLink = $baseLink . "/" . $eachElement;
            $resultArray[] = [self::$LINK => $baseLink, self::$NAME => ucfirst($eachElement)];
        };
        return $resultArray;
    }

    public function getArrayBreadcrumb()
    {
        $url = Url::getInstance($this->getDI());
        $splitArray = explode("/", $url->currentUrl());
        array_shift($splitArray);
        $splitArray = $this->mappingUrl($splitArray);
        array_unshift($splitArray, $this->HOME);
        return $splitArray;
    }

    public function generateElement($element){
        return "<li> <a href=\"{$element[self::$LINK]}\"> {$element[self::$NAME]} </a></li>";
    }

    public function generateLastElement($lastElement)
    {
        return "<li class='active'> {$lastElement[self::$NAME]} </li>";
    }

    public function generateWrapper($string){
        return "<ul class=\"breadcrumb\"> " . $string . "</ul>";
    }

    public function customBreadcrumb($arrayUrl, $arrayName){

        if ( !is_array($arrayName) || sizeof($arrayName) == 0){
            return $arrayUrl;
        }
        $arrayUrl = array_reverse($arrayUrl);
        $arrayName = array_reverse($arrayName);
        foreach($arrayName as $key => $value){
            $arrayUrl[$key][self::$NAME] = $value;
        }
        return array_reverse($arrayUrl);
    }
}
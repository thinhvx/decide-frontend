/**
 * Dual ListBox initializer.
 *
 * @category  PhalconEye
 * @package   PhalconEye Core Module
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright Copyright (c) 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
(function (window, $, root, undefined) {
    root.ns(
        'PhalconEye.widget.dualListBox',
        {
            /**
             * Init autocomplete.
             *
             * @param element Element object.
             */
            init: function (element) {
                element.bootstrapDualListbox({
                });
            }
        }
    );
}(window, jQuery, PhalconEye));


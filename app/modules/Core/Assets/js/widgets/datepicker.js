/*
 +------------------------------------------------------------------------+
 | PhalconEye CMS                                                         |
 +------------------------------------------------------------------------+
 | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
 +------------------------------------------------------------------------+
 | This source file is subject to the New BSD License that is bundled     |
 | with this package in the file LICENSE.txt.                             |
 |                                                                        |
 | If you did not receive a copy of the license and are unable to         |
 | obtain it through the world-wide-web, please send an email             |
 | to license@phalconeye.com so we can send you a copy immediately.       |
 +------------------------------------------------------------------------+
 | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
 +------------------------------------------------------------------------+
 */

/**
 * CkEditor widget.
 *
 * @category  PhalconEye
 * @package   PhalconEye Core Module
 * @author    Ivan Vorontsov <ivan.vorontsov@phalconeye.com>
 * @copyright Copyright (c) 2013-2014 PhalconEye Team
 * @license   New BSD License
 * @link      http://phalconeye.com/
 */
(function (window, $, root, undefined) {
    root.ns(
        'PhalconEye.widget.datePicker',
        {
            /**
             * Init datePicker.
             *
             * @param element datepicker element.
             */
            init: function (element) {
                var $elts = $(element) || $('input.date-picker');
//                var startDate = $elt.data('date-start-date');
//                if(typeof startDate != 'undefined'){
//                    startDate = "+0d";
//                }
//
//                var dateFormat = $elt.data('date-format');
//                if(typeof startDate != 'undefined'){
//                    startDate = "+0d";
//                }

                $elts.each(function(idx, $elt){
                    var $elt = $($elt);
                    if(!$elt.val()){
                        var date = new Date();
                        var strDate = date.getDate() + "/" +  (parseInt(date.getMonth())+1) + "/" + date.getFullYear();
                        $elt.val(strDate);
                    }

                    $element_replace  = '<div id="'+ $elt.attr('id')+'" class="input-group input-medium date date-picker" data-date-format="d/m/yyyy" data-date="'+strDate+'">';
                    $element_replace += '<input type="text" readonly="" class="form-control" name="' + $elt.attr('name') + '" value="'+$elt.val()+'">';
                    $element_replace += '<span class="input-group-btn">';
                    $element_replace += '<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-calendar"></i></button>';
                    $element_replace += '</span>';
                    $element_replace += '</div';

                    $elt.replaceWith($element_replace);

                    $('.date-picker').datepicker({
                        todayHighlight: true
                    });
                })

            }
        }
    );
}(window, jQuery, PhalconEye));

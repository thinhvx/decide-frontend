{#
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
#}

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <title>{{ helper('title','core').setPageTitle(pageTitle| default('')) }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <meta Http-Equiv="Cache-Control" Content="no-cache">
    <meta Http-Equiv="Pragma" Content="no-cache">
    <meta Http-Equiv="Expires" Content="0">
    <meta Http-Equiv="Pragma-directive: no-cache">
    <meta Http-Equiv="Cache-directive: no-cache">

    {{ assets.outputCss() }}

    {{ assets.outputInline() }}

    <script type="text/javascript">
        {{ helper('i18n', 'core').render() }}
    </script>

    {% block head %}

    {% endblock %}

</head>

<body data-base-url="{{ url() }}" data-debug="{{ config.application.debug }}" class="corporate">
{# FB Comments #}
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1487666311475340&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
 {#end FB Comments#}

    {% if disableHeader is not defined %}
    {{ helper('renderer', 'core').renderContent('header')}}
    {% endif %}

    {%- block header -%}
    {%- endblock -%}

    <div class="system-container">
        {{ content() }}
    </div>


    <div class="main">
        <div class="container geekup-main-content" >
            {%  if not_render_breadcrums is empty %}
                     {% set is_render =  false %}
            {% else  %}
                    {% set is_render =  not_render_breadcrums %}
            {% endif %}

            {{ helper('Breadcrumb', 'core').showBreadcrumb(breadcrumb|default(''), is_render )}}

            <div class="row  geekup-main-content-row" >
                <div class="col-md-12 col-sm-12 geekup-main-content-col" >
                    <div class="content-page">
                        <div class="row">
                            {%- block content -%}
                            {%- endblock -%}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	{% if disableFooter is not defined %}
    {{ helper('renderer', 'core').renderContent('footer')}}
    {% endif %}
	{%- block footer -%}
    {%- endblock -%}


{{ helper('profiler', 'core').render() }}

{{ assets.outputJs() }}
</body>
</html>
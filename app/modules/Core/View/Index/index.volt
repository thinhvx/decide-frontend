{% extends "../../Core/View/layouts/home_layout.volt" %}

{% block content %}
    <div class="header-slider">

        <div class="container">
            <div class="fullwidthbanner-container revolution-slider">
                <div class="fullwidthabnner">
                    <ul id="revolutionul">
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-delay="9400">
                            <img src="../../upload/home/AWS_ac_ra_web_01-1.png?{{ date('YmdHis') }}" alt="">
                        </li>
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-delay="9400">
                            <img src="../../upload/home/diagram_agile_scrum-1.png?{{ date('YmdHis') }}" alt="">
                        </li>
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-delay="9400">
                            <img src="upload/home/phalcon-php-1.png?{{ date('YmdHis') }}" alt="">
                        </li>
                    </ul>
                    <div class="tp-bannertimer tp-bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SLIDER -->
    <div class="container geekup-main-content">
    <div class="row  geekup-main-content-row">
    <div class="col-md-12 col-sm-12 geekup-main-content-col">
    <div class="content-page">
    <div class="row">
    <div class="row">
        <div class="col-md-12 geekup-info">
            <div class="col-md-6 geekup-quoting">

                <div id="cbp-qtrotator" class="cbp-qtrotator">
                    {% for quoting in quotings %}
                        <div class="cbp-qtcontent">
                            <p><i class="fa fa-quote-left"></i>{{ quoting.quoting }}<i class="fa fa-quote-right"></i></p>
                            <footer>{{ quoting.author }}</footer>
                        </div>
                    {% endfor %}
                </div>
            </div>
            <div class="col-md-6 geekup-general-info">
                <div class="col-md-4 info-items">
                    <div class="info-items-content">
                        <div class="info-items-data">
                            {{ pageviews }}
                        </div>

                        <div class="info-items-unit">
                            Views
                        </div>
                    </div>
                    <div class="info-items-title">
                        <a href="#">
                            <span class="info-item-viewmore">VIEW MORE</span>
                            <span class="info-item-viewmoreicon"><i class="m-icon-swapright m-icon-white"></i> </span>

                            {#<span class="glyphicon glyphicon-circle-arrow-right"></span>#}
                        </a>
                    </div>
                </div>

                {{ helper('renderer', 'base').renderWidgetByName('blog', 'totalcomment') }}

                <div class="col-md-4 info-items">
                    <div class="info-items-content">
                        <div class="info-items-data">
                            {{ total_members }}
                        </div>
                        <div class="info-items-unit">
                            Members
                        </div>
                    </div>
                    <div class="info-items-title">
                        <a href="#">
                            {#<p>VIEW MORE </p>#}
                            {#<span class="glyphicon glyphicon-circle-arrow-right"></span>#}
                            <span class="info-item-viewmore">VIEW MORE</span>
                            <span class="info-item-viewmoreicon"><i class="m-icon-swapright m-icon-white"></i> </span>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="geekup-content col-md-12">
            <!-- TABS -->
            <div class="col-md-6 geekup-tab" style="padding-left: 0px;">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#posts" data-toggle="tab">Posts</a></li>
                    <li><a href="#comments" data-toggle="tab">Comments</a></li>
                    {#<li><a href="#activites" data-toggle="tab">Activities</a></li>#}
                </ul>
                <div class="tab-content">
                    <div class="tab-pane row fade in active " id="posts">
                        {% for post in recentNews %}
                            <div class="col-md-12 col-sm-12 geekup-tab-item">
                                <div class="col-md-3 col-sm-3 recent-post-img-col">
                                    <a href="metronic/temp/photos/img7.jpg" class="fancybox-button" title="Image Title"
                                       data-rel="fancybox-button">
                                        {#<img class="img-responsive" src="{{ url() }}" alt="">#}
                                        <img class="img-responsive" alt=""
                                             src="{{ helper('post','blog').getThumbnailPath(post.header_image) }}">
                                    </a>
                                </div>
                                <div class="col-md-9 col-sm-9 recent-post">
                                    <h5><a href="{{ url(['for':'blog-view', 'slug': post.slug]) }}">{{ post.title }}</a>
                                    </h5>


                                    <div class="threedots recent-post-description">{{ post.short_description }}</div>

                                    <p><a class="more" href="{{ url(['for':'blog-view', 'slug': post.slug]) }}">Read
                                            more <i class="icon-angle-right"></i></a></p>
                                </div>
                            </div>
                            <hr class="col-md-11 col-sm-12 geekup-hr">
                        {% endfor %}
                    </div>
                    <div class="tab-pane row fade" id="comments">
                        {% for comment in recentComments %}
                            <div class="col-md-12 col-sm-12 geekup-tab-comment-item gu-hp-cm-item">
                                <div class="col-md-2 col-sm-2 comment-avatar-wrapper">
                                    <a href="#" class="pull-left ">
                                        <img src="{{ helper('user','user').getAvatarPath(comment.user_id) }}" alt=""
                                             class="img-circle media-object comment-avatar">
                                    </a>
                                </div>
                                <div class="col-md-10 col-sm-10">
                                    <a href="#"
                                       class="recent-comment-author">{{ helper('comment','blog').upperUsername(comment.user.username) }} </a>
                                        <span class="recent-comment-time pull-right">
                                            {{ helper('comment', 'blog').commentTime(comment.creation_date) }}
                                        </span>

                                    <p class="comment-content">
                                        {{ comment.content }}
                                    </p>
                                    <a href="{{ url(['for': 'blog-view', 'slug': comment.post.slug]) }}"
                                       class="recent-comment-link"><em> {{ helper('url').getFullUrl(url(['for': 'blog-view', 'slug': comment.post.slug])) }}</em></a>

                                </div>
                            </div>
                            <hr class="col-md-11 col-sm-12 geekup-hr">
                        {% endfor %}
                    </div>
                    <div class="tab-pane fade" id="activities">
                        <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic
                            lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork
                            tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica.
                            DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh
                            mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog.
                            Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown.
                            Pitchfork sustainable tofu synth chambray yr.</p>
                    </div>
                </div>
            </div>
            <!-- END TABS -->

            <!-- TESTIMONIALS -->
            <div class="col-md-6 testimonials-v1 geekup-chat">
                <h2 class="chat-title ">Chats</h2>

                <div class="portlet">
                    <div class="portlet-body" id="chats">
                        <div class="scroller">
                            <ul class="chats" id="listChats">
                            </ul>
                        </div>
                        <div class="chat-form">
                            <label id="chat-error"  for="messageInput"></label>
                            <div class="input-cont">
                                <input  id="messageInput" class="form-control" type="text" placeholder="Type a message here..."
                                        >
                            </div>
                            <div class="btn-cont">
									<span class="arrow">
									</span>
                                <a href="" class="btn blue icn-only" id="sentBtn">
                                    <i class="fa fa-send icon-white"></i>
                                </a>
                                <input type="hidden" value="{{ helper('user','user').current().username }}" id="username">
                                <input type="hidden" value="{{ helper('user','user').getAvatarPath(helper('user','user').current().id) }}" id="avatar_path">
                                <input type="hidden" value="{{ token }}" id="token">
                                <input type="hidden" value="{{ fireBaseUrl }}" id="fireBaseUrl">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- END TESTIMONIALS -->
        </div>
    </div>

    <div class="row our-clients geekup-partner">
        <div class="col-md-3">
            <h2 class="padding-top-0"><a href="#">Partners</a></h2>

            <p>Our partners provide essential support, expertise & outreach.</p>
        </div>
        <div class="col-md-9">
            <div class="owl-carousel owl-carousel6-brands">
                <div class="client-item">
                    <a href="http://www.htgsoft.com/">
                        <img src="../../upload/partner/htgsoft_gray.png" class="img-responsive"
                             alt="">
                        <img src="../../upload/partner/htgsoft.png"
                             class="color-img img-responsive" alt="">
                    </a>
                </div>
                <div class="client-item">
                    <a href="http://liprof.com">
                        <img src="../../upload/partner/liprof_gray.png" class="img-responsive"
                             alt="">
                        <img src="../../upload/partner/liprof.png"
                             class="color-img img-responsive" alt="">
                    </a>
                </div>
                <div class="client-item">
                    <a href="http://vht.com.vn">
                        <img src="../../upload/partner/vht_gray.png" class="img-responsive"
                             alt="">
                        <img src="../../upload/partner/vht.png"
                             class="color-img img-responsive" alt="">
                    </a>
                </div>
                <div class="client-item">
                    <a href="#">
                        <img src="../../upload/partner/n_a_gray.png" class="img-responsive"
                             alt="">
                        <img src="../../upload/partner/n_a.png"
                             class="color-img img-responsive" alt="">
                    </a>
                </div>
                <div class="client-item">
                    <a href="#">
                        <img src="../../upload/partner/n_a_gray.png" class="img-responsive"
                             alt="">
                        <img src="../../upload/partner/n_a.png"
                             class="color-img img-responsive" alt="">
                    </a>
                </div>
                <div class="client-item">
                    <a href="#">
                        <img src="../../upload/partner/n_a_gray.png" class="img-responsive"
                             alt="">
                        <img src="../../upload/partner/n_a.png"
                             class="color-img img-responsive" alt="">
                    </a>
                </div>
                <div class="client-item">
                    <a href="#">
                        <img src="../../upload/partner/n_a_gray.png" class="img-responsive"
                             alt="">
                        <img src="../../upload/partner/n_a.png"
                             class="color-img img-responsive" alt="">
                    </a>
                </div>
                <div class="client-item">
                    <a href="#">
                        <img src="../../upload/partner/n_a_gray.png" class="img-responsive"
                             alt="">
                        <img src="../../upload/partner/n_a.png"
                             class="color-img img-responsive" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
{% endblock %}
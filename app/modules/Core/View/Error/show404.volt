{#
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Author: Ivan Vorontsov <ivan.vorontsov@phalconeye.com>                 |
  +------------------------------------------------------------------------+
#}

{% extends "../../Base/View/layouts/frontend.volt" %}

{% block title %}
    {{ 'Not Found'|i18n }}
{% endblock %}

{% block content %}
    <div class="row margin-bottom-40" style="min-height: 500px">
        <!-- BEGIN CONTENT -->
        <div class="col-md-12 col-sm-12">
            <div class="content-page page-404">
                <div class="number">
                    404
                </div>
                <div class="details">
                    <h3>Oops!  You're lost.</h3>
                    <p>
                        We can not find the page you're looking for.<br>
                        <a href="/" class="link">Return home</a> or try the search bar below.
                    </p>
                </div>
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
{% endblock %}


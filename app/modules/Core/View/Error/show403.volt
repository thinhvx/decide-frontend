{% extends "../../Base/View/layouts/frontend.volt" %}

{% block pageTitle %}
    {{ 'Forbidden'|i18n }}
{% endblock %}

{% block content %}
    <div class="row">
        <div class="col-md-12 page-404">
            <div class="number">403</div>
            <div class="details">
                <h3>Oops! Permission denied.</h3>
                <p>
                    You don't have permission to access this resource.
                </p>
            </div>
        </div>
    </div>
{% endblock %}
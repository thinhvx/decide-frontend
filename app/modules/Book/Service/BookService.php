<?php
/**
 * Created by PhpStorm.
 * User: thinhvoxuan
 * Date: 11/17/14
 * Time: 3:00 PM
 */

namespace Book\Service;

use Phalcon\DI;

class BookService {

    var $di;

    public function __construct(){
        $this->di = DI::getDefault();
    }

    public function query_all($from){

        $es = $this->di->getEs();
        $params['index'] = 'book';
        $params['type'] = 'tiki';
        $params['body'] = [
            'query' => [
                'match_all' => []
            ],
        ];
        $params['from'] = $from;
        $params['size'] = 9;
        $result = $es->search($params);

//      var_dump($result);
//      die();
        return $result;
    }

} 
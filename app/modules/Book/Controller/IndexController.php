<?php
/*
  +------------------------------------------------------------------------+
  | PhalconEye CMS                                                         |
  +------------------------------------------------------------------------+
  | Copyright (c) 2013-2014 PhalconEye Team (http://phalconeye.com/)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file LICENSE.txt.                             |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconeye.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
*/

namespace Book\Controller;

use Base\Controller\FrontendBaseController;
use Book\Service\BookService;
use Core\Api\Acl;
use Core\Controller\AbstractController;
/**
 * Index controller.
 *
 * @category PhalconEye\Module
 * @package  Controller
 *
 * @RoutePrefix("/book", name="blog-controller")
 */
class IndexController extends FrontendBaseController
{
    /**
     * Module index action.
     *
     * @return void
     *
     * @Route("/", methods={"GET"}, name="book-get")
     * @Acl(roles="Guest")
     */
    public function indexAction()
    {
        $from = $this->request->get('from', null, 0);
        $books = new BookService();

        $data = $books->query_all($from);
        $this->view->books_result = $data;
        $this->view->books = $data['hits']['hits'];
        $this->view->from = $from;
        $this->view->current_page = ceil($from / 9) + 1;
//        var_dump($data);
//        die();
    }


    /**
     * Module index action.
     *
     * @param $id
     * @return void
     *
     * @Route("/{id:[_0-9a-zA-Z\-]+}", methods={"GET"}, name="book-get-item")
     * @Acl(roles="Guest")
     */
    public function getAction($id)
    {
        $this->add_assets();
        $another_result = [];

        $params['index'] = 'book';
        $params['type'] = 'tiki';
        $params['id'] = $id;

        $result = $this->di->getEs()->get($params);
        if(!empty($result['_source']['other_site']))
            $another_result[] = $this->find_by_link($result['_source']['other_site'][0], "nobita");
        $another_result[] = $this->find_by_link($result['_source']['link'], "tiki");

        $this->view->item_all = $result;
        $this->view->item_data = $result['_source'];
        $this->view->another_result = $another_result;

    }

    public function add_assets(){
        $this->assets->addCss('external/rickshaw/rickshaw.css')
            ->addCss('external/rickshaw/src/css/graph.css')
            ->addCss('external/rickshaw/src/css/lines.css')
        ;
//        $this->assets
//            ->addJs('external/rickshaw/vendor/d3.v3.js')
//            ->addJs('external/rickshaw/rickshaw.js')
//        ;

    }

    function find_by_link($link, $index){
        $params_2['index'] = 'book';
        $params_2['type'] = $index;
        $params_2['body']['query'] = [
            'match_phrase' => [
                'link' => $link,
            ]
        ];
        $params_2["sort"] = "update_date:asc";

        $result = $this->di->getEs()->search($params_2);
        return $result['hits']['hits'];
    }

    /**
     * Module index action.
     *
     * @param $id
     * @return void
     *
     * @Route("/search.html", methods={"GET"}, name="book-search-item")
     * @Acl(roles="Guest")
     */
    public function searchAction()
    {
        $from = $this->request->get('from', null, 0);

        $title_book = $this->request->get('q');

        if($title_book==null){
            return $this->response->redirect(['for'=> 'book-get']);
        }

//        GET /book/tiki/_search
//        {
//            "query": {
//            "multi_match": {
//                "type":     "phrase",
//              "query": "viet nam",
//              "fields": [ "title", "title.folded" ]
//            }
//          }
//        }

        $params['index'] = 'book';
        $params['type'] = 'tiki';

//        $params['body']['query']['bool']['must'] = array(
//            array('match' => array('testField' => 'abc')),
//            array('match' => array('anotherTestField' => 'xyz')),
//        );

        $params['body']['query']['bool'] = [
             'must' => [
                ['multi_match' => [
                    "type" => "most_fields",
                    "query" => $title_book,
                    "fields" =>[
                        "title^4", "title.folded^3",
                        "author^2", "author.folded^2",
//                        "description", "description.folded"
                    ],
                    "operator" => "and",
                ]],
                ['match' => [
                    'update_date' => '2014-12-28'
                ]]
            ]
        ];

        $params['from'] = $from;
        $params['size'] = 9;

        $result = $this->di->getEs()->search($params);
//        var_dump($result);
//        die();
//        foreach($result['hits']['hits'] as $bok){
//            var_dump($bok);
//        }
        $this->view->total_result = $result['hits']['total'];
        $this->view->books = $result['hits']['hits'];

        $this->view->query = $title_book;
        $this->view->from = $from;
        $this->view->current_page = ceil($from / 9) + 1;
    }


    public function get(){
        $params['index'] = 'book';
        $params['type'] = 'tiki';
        $params['body'] = [
            'query_string' => [
                'match' => [
                    'link' => 'http://tiki.vn/u-is-for-undertow-kinsey-millhone-mystery.html?ref=c8.c9.c206.c207.c320.c785.'
                ]
            ],
        ];
        $result = $this->di->getEs()->search($params);
    }
}

<div class="col-md-12 col-sm-12">
    {% for idx, item in books %}
        {% if loop.first %}
            <div class="row">
        {% elseif (loop.index - 1) % 3 == 0 %}
            </div><div class="row">
        {% endif %}
        <div class="col-md-4 col-sm-6 col-xs-12">
            {{ partial('../../Book/View/partial/customItem', ['item': item['_source'], 'idx': idx, 'index': item['_id']]) }}
        </div>
        {% if loop.last %}
            </div>
        {% endif %}
    {% endfor %}
</div>
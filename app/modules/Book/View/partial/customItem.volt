<div class="product-item">
    <div class="pi-img-wrapper">
        {% if item['book_image'] is defined %}
            <img src="{{item['book_image']|default('') }}" class="img-responsive" alt="image">
        {% else %}
            <img src="/metronic/frontend/layout/img/empty-icon.png" class="img-responsive" alt="image">
        {% endif %}
        <div>
            <a href="{{ "#product-pop-up" ~ idx }}" class="btn btn-default fancybox-fast-view">View</a>
        </div>
    </div>
    <h3><a href="/book/{{index}}">{{ item['title'] }}-{{ item['author'] }}</a></h3>
    <div class="pi-price">{% if item['sale_off'] != 0 %}{{ item['sale_off'] }}{% else %}{{ item['reg_price'] }}{% endif %}
    </div>



    <a href="" class="btn btn-default add2cart">Add to monitor</a>

    <div id="{{ "product-pop-up" ~ idx }}" style="width: 700px; display: none;">
        <div class="product-page product-pop-up">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-3">
                    <div class="product-main-image" style="position: relative; overflow: hidden;">
                        {#<img src="{{ item['book_image'] }}" alt="Item image" class="img-responsive">#}
                        {% if item['book_image']|default('') %}
                            <img src="{{ item['book_image']|default('') }}" class="img-responsive" alt="image">
                        {% else %}
                            <img src="/metronic/frontend/layout/img/empty-icon.png" class="img-responsive" alt="image">
                        {% endif %}
                        {#<img src="{{ item['book_image'] }}" class="zoomImg" style="position: absolute; top: -281.454545454546px; left: -91.6363636363636px; opacity: 0; width: 600px; height: 800px; border: none; max-width: none;">#}
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-9" style="padding-top: 10px">
                    <h1>{{ item['title'] }}</h1>
                    <div class="price-availability-block clearfix">
                        <div class="price">
                            <strong>{{ item['sale_off']}}<span>VND</span></strong>
                            <em><span>{{ item['old_price'] }}</span>VND</em>
                        </div>
                        {#<div class="availability">#}
                            {#Availability: <strong>In Stock</strong>#}
                        {#</div>#}
                    </div>
                    <div class="description dotdotdot-3line">
                        <p>{{ item['description'] }}</p>
                    </div>
                    {#<div class="product-page-options">#}
                        {#<div class="pull-left">#}
                            {#<label class="control-label">Size:</label>#}
                            {#<select class="form-control input-sm">#}
                                {#<option>L</option>#}
                                {#<option>M</option>#}
                                {#<option>XL</option>#}
                            {#</select>#}
                        {#</div>#}
                        {#<div class="pull-left">#}
                            {#<label class="control-label">Color:</label>#}
                            {#<select class="form-control input-sm">#}
                                {#<option>Red</option>#}
                                {#<option>Blue</option>#}
                                {#<option>Black</option>#}
                            {#</select>#}
                        {#</div>#}
                    {#</div>#}
                    <div class="product-page-cart">
                        {#<div class="product-quantity">#}
                            {#<div class="input-group bootstrap-touchspin input-group-sm"><span class="input-group-btn"><button class="btn quantity-down bootstrap-touchspin-down" type="button"><i class="fa fa-angle-down"></i></button></span><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="product-quantity" type="text" value="1" readonly="" name="product-quantity" class="form-control input-sm" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn"><button class="btn quantity-up bootstrap-touchspin-up" type="button"><i class="fa fa-angle-up"></i></button></span></div>#}
                        {#</div>#}
                        <a href="/book/{{index}}"  class="btn btn-default">View details</a>
                        <a href="{{ item['link'] }}" target="_blank" class="btn btn-default">View Page</a>
                    </div>
                </div>

                {#<div class="sticker sticker-sale"></div>#}
            </div>
        </div>
    </div>
</div>
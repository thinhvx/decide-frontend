{% extends "../../Base/View/layouts/frontend.volt" %}

{% block content %}
    <style>
        #chart {
            position: relative;
            left: 40px;
            display: block;
        }
        #y_axis {
            position: absolute;
            top: 0;
            bottom: 0;
            width: 40px;
        }
        #x_axis {
            position: relative;
            left: 40px;
            height: 40px;
        }
        #legend {
            display: inline-block;
            vertical-align: top;
            margin: 0 0 0 10px;
        }
    </style>
    <script type="text/javascript" src="/external/rickshaw/vendor/d3.v2.js"></script>
    <script type="text/javascript" src="/external/rickshaw/rickshaw.js"></script>
    <div class="col-md-12 col-sm-12">
        <div class="product-page">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="product-main-image" style="position: relative; overflow: hidden;">
                        <img src="{{ item_data['book_image'] }}">
                    </div>
                </div>
                <div class="col-md-9 col-sm-9">
                    <h1>{{ item_data['title'] }}</h1>
                    <div class="price-availability-block clearfix">
                        <div class="price">
                            <strong> {% if item_data['sale_off'] != 0 %}{{ item_data['sale_off'] }}{% else %}{{ item_data['reg_price'] }}{% endif %}<span></span>VND</strong>
                            <em><span>{{ item_data['old_price'] }}</span><span>VND</span></em>
                        </div>
                        <div class="availability">
                            Author: <strong>{{ item_data['author'] }}</strong>
                        </div>
                    </div>
                    <div class="description">
                        <p>
                            {{ item_data['description'] }}
                        </p>
                    </div>
                    <div class="product-page-cart">
                        <button class="btn btn-primary" type="submit"><a href="{{ item_data['link'] }} "  target="_blank">Go to page</a></button>
                    </div>
                    <div class="review">
                        <input type="range" value="4" step="0.25" id="backing4" style="display: none;">
                        <div class="rateit" data-rateit-backingfld="#backing4" data-rateit-resetable="false" data-rateit-ispreset="true" data-rateit-min="0" data-rateit-max="5">
                            <button id="rateit-reset-2" data-role="none" class="rateit-reset" aria-label="reset rating" aria-controls="rateit-range-2" style="display: none;"></button><div id="rateit-range-2" class="rateit-range" tabindex="0" role="slider" aria-label="rating" aria-owns="rateit-reset-2" aria-valuemin="0" aria-valuemax="5" aria-valuenow="4" aria-readonly="false" style="width: 80px; height: 16px;"><div class="rateit-selected rateit-preset" style="height: 16px; width: 64px;"></div><div class="rateit-hover" style="height: 16px;"></div></div></div>
                        <a href="#">7 reviews</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#" target="_blank">Write a review</a>
                    </div>
                    <ul class="social-icons">
                        <li><a class="facebook" data-original-title="facebook" href="#"></a></li>
                        <li><a class="twitter" data-original-title="twitter" href="#"></a></li>
                        <li><a class="googleplus" data-original-title="googleplus" href="#"></a></li>
                        <li><a class="evernote" data-original-title="evernote" href="#"></a></li>
                        <li><a class="tumblr" data-original-title="tumblr" href="#"></a></li>
                    </ul>

                </div>
                <div class="product-page-content">
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active"><a href="#Price_Chart" data-toggle="tab" aria-expanded="false">Price table</a></li>
                        <li><a href="#Information" data-toggle="tab">Information</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade active in" class="tab-pane fade" id="Price_Chart">
                            <div>
                                <div class="row">
                                    <ul class="list-unstyled">
                                        {% for index, each in another_result %}
                                        <li>
                                            <a href="{{ each[0]["_source"]['link']}}" target="_blank" class="btn btn-default btn-sm" role="button">
                                                [{{ index }}]
                                            </a> {{ each[0]["_source"]['title'] }}
                                        </li>
                                        {% endfor %}
                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div id="chart_container">
                                            <div id="y_axis"></div>
                                            <div id="chart"></div>
                                            <div id="x_axis"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="legend" style="position:absolute;right: 0px; left: inherit" ></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="Information">
                            <table class="datasheet">
                                <tbody>
                                <tr>
                                    {% set decoded = item_data['information']|json_decode %}
                                    <th colspan="2">Information</th>
                                </tr>
                                {% for key, value in decoded %}
                                <tr>
                                    <td class="datasheet-features-type">{{ key }}</td>
                                    <td>{{ value }}</td>
                                </tr>
                                {% endfor %}
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div class="sticker sticker-sale"></div>
            </div>
        </div>
    </div>
    <script>
        var palette = new Rickshaw.Color.Palette();
        var graph = new Rickshaw.Graph( {
            element: document.getElementById("chart"),
            renderer: 'line',
            height: 300,
            width: 800,
            series: [
                {% for index, each in another_result %}
                {
                    name: '[{{ index }}] -  {{ each[0]['_type'] }}' ,
                    data: [
                        {% for each_record in each %}
                            {
                                x: (new Date('{{ each_record['_source']['update_date']}}')).getTime() ,
                                y: {% if each_record['_source']['sale_off'] != 0 %}
                                        {{ each_record['_source']['sale_off'] }}
                                        {% set last_price = each_record['_source']['sale_off']  %}
                                   {% else %}
                                        {{ each_record['_source']['reg_price'] }}
                                        {% set last_price = each_record['_source']['reg_price']  %}
                                   {% endif %}
                            },
                        {% if loop.last %}
                        {
                            x: (new Date()).getTime() ,
                            y: {{ last_price }}
                        },
                        {% endif %}
                        {% endfor %}
                    ],
                    color: palette.color()
                },
                {% endfor %}
            ]
        } );

        var x_ticks = new Rickshaw.Graph.Axis.X( {
            graph: graph,
            orientation: 'bottom',
            element: document.getElementById('x_axis'),
            tickFormat: function(x){
                var x = new Date(x);
                return x.getDate() + "-" + (x.getMonth()+1) + "-" + x.getFullYear()
            }
        });
//
        var y_ticks = new Rickshaw.Graph.Axis.Y( {
            graph: graph,
            orientation: 'left',
            tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
            element: document.getElementById('y_axis')
        });
//
        var legend = new Rickshaw.Graph.Legend( {
            element: document.querySelector('#legend'),
            graph: graph
        } );

        graph.render();
    </script>
{% endblock %}
{% extends "../../Base/View/layouts/frontend.volt" %}
{% block content %}
    <div class="col-md-12 col-sm-12">
        <h1>Query: {{ query }}</h1>
        <h1>Total matching result: {{ total_result }}</h1>
    </div>

    <div class="col-md-12 col-sm-12">
        {% for idx, item in books %}
            {% if loop.first %}
                <div class="row">
            {% elseif (loop.index - 1) % 3 == 0 %}
                </div><div class="row">
            {% endif %}
            <div class="col-md-4 col-sm-6 col-xs-12">
                {{ partial('../../Book/View/partial/customItem', ['item': item['_source'], 'idx': idx, 'index': item['_id']]) }}
            </div>
            {% if loop.last %}
                </div>
            {% endif %}
        {% endfor %}
    </div>

    <div class="row" style="margin-top: 20px">
        <div class="col-md-4 col-sm-4 items-info">Items {{ from }} to {{ from + 9 }} of {{ total_result }}  total</div>
        <div class="col-md-8 col-sm-8">
            <ul class="pagination pull-right">
                {% if from > 9 %}
                    <li><a href="/book/search.html?q={{ query }}&from={{ from - 9 }}">«</a></li>
                {% elseif from == 9 %}
                    <li><a href="//book/search.html?q={{ query }}">«</a></li>
                {% endif %}
                <li><span>{{ current_page }}</span></li>
                {% if from + 9 < total_result %}
                <li><a href="/book/search.html?q={{ query }}&from={{ from + 9 }}">»</a></li>
                {% endif %}
            </ul>
        </div>
    </div>
{% endblock %}
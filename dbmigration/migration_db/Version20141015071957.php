<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141015071957 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $connection = $this->connection;
        $result = $connection->fetchAll('SELECT * FROM  users');
        foreach($result as $user){
            $this->insertProfileUser($user['id'], $user['username'], $connection);
        }
    }

    public function insertProfileUser($userid, $username, $connection){
        $count = count($this->connection->fetchAll("Select * FROM profile WHERE user_id = ${userid}"));
        if ($count == 0){
            $connection->insert('profile', array(
                'user_id' => $userid,
                'firstname' => $username,
                'lastname' => ' ',
            ));
        }
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}

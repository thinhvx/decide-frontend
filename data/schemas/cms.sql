-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 13, 2014 at 11:44 PM
-- Server version: 5.5.37-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `devdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE IF NOT EXISTS `access` (
  `object` varchar(55) NOT NULL,
  `action` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `value` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`object`,`action`,`role_id`),
  KEY `fki-access-roles-role_id-id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`object`, `action`, `role_id`, `value`) VALUES
('AdminArea', 'access', 1, 'allow'),
('AdminArea', 'access', 2, 'deny'),
('AdminArea', 'access', 3, 'deny'),
('\\Blog\\Model\\Post', 'blog_count', 1, NULL),
('\\Blog\\Model\\Post', 'blog_footer', 1, 'true'),
('\\Blog\\Model\\Post', 'create', 1, 'allow'),
('\\Blog\\Model\\Post', 'delete', 1, 'allow'),
('\\Blog\\Model\\Post', 'edit', 1, 'allow'),
('\\Blog\\Model\\Post', 'index', 1, 'allow'),
('\\Blog\\Model\\Post', 'view', 1, 'allow'),
('\\Core\\Model\\Page', 'page_footer', 1, NULL),
('\\Core\\Model\\Page', 'show_views', 1, 'deny');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `status` int(4) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-comment-post-post_id-id` (`post_id`),
  KEY `fki-comment-users-user_id-id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `content`, `url`, `status`, `post_id`, `user_id`, `creation_date`, `modified_date`, `parent_id`) VALUES
(1, 'dads', NULL, 1, 12, 1, '2014-08-11 07:54:48', NULL, NULL),
(2, 'fdsfdsfdsf', NULL, 1, 12, 1, '2014-08-11 07:54:51', NULL, NULL),
(3, 'fdfdsfds', NULL, 1, 12, 1, '2014-08-11 07:54:55', NULL, NULL),
(4, '1', NULL, 1, 12, 1, '2014-08-11 07:55:08', NULL, NULL),
(5, '2', NULL, 1, 12, 1, '2014-08-11 07:55:10', NULL, NULL),
(6, '3', NULL, 1, 12, 1, '2014-08-11 07:55:40', NULL, NULL),
(7, '4', NULL, 1, 12, 1, '2014-08-11 08:01:39', NULL, NULL),
(8, '6', NULL, 1, 12, 1, '2014-08-11 08:01:43', NULL, NULL),
(9, 'ffd', NULL, 1, 12, 1, '2014-08-11 08:56:19', NULL, NULL),
(10, 'Tháº±ng nÃ y Ä‘Ãºng lÃ  má»›i chá»‰ thá»±c táº­p sinh (intern) vá» marketing, nÃªn chÃ©m vá»› chÃ©m váº©n. Pháº§n trÃªn rÃµ rÃ ng nÃ³ Ä‘Æ°a ra biá»ƒu Ä‘á»“ PHP lÃ  TOP, pháº§n bÃªn dÆ°á»›i nÃ³ láº¡i ghi lÃ  trÃ¡nh xa PHP ??? Tráº» trÃ¢u quÃ¡ :)', NULL, 1, 13, 1, '2014-08-13 21:58:27', NULL, 0),
(11, 'xu hÆ°á»›ng 2015 theo tháº§y sáº½ nghiÃªng vá» tháº±ng nÃ o áº¡ :(', NULL, 1, 13, 1, '2014-08-13 21:58:46', NULL, 10),
(12, 'JAV (AD, IOs) vá»›i PHP giá» thÃ¬ Ä‘Ãºng lÃ  song kiáº¿m há»£p bÃ­ch rá»“i :)) Chá»— em lÃ m cÃ²n vá»«a sÃ¡t nháº­p 2 Ä‘á»™i vÃ o 1 ngá»“i cÃ¹ng táº§ng vÃ¬ sau nÃ y lÃ m 70-85% dá»± Ã¡n liÃªn quan Ä‘áº¿n nhau cÃ³ gÃ¬ "máº¯ng" nhau cho tiá»‡n :))', NULL, 1, 13, 1, '2014-08-13 21:58:58', NULL, 10),
(13, 'I dont understand, please explain', NULL, 1, 13, 4, '2014-08-13 22:06:10', NULL, 10),
(14, 'Great post, I think ', NULL, 1, 12, 4, '2014-08-13 22:21:01', NULL, 10);

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `widget_id` int(11) NOT NULL,
  `widget_order` int(5) NOT NULL,
  `layout` varchar(50) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-content-widgets-widget_id-id` (`widget_id`),
  KEY `fki-content-pages-page_id-id` (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `page_id`, `widget_id`, `widget_order`, `layout`, `params`) VALUES
(9, 1, 3, 1, 'middle', '{"logo":"assets\\/img\\/core\\/pe_logo.png","show_title":null,"show_auth":"1","roles":null,"content_id":"9"}'),
(10, 1, 2, 2, 'middle', '{"logo":"assets\\/img\\/core\\/pe_logo.png","title":null,"class":null,"menu":"Default menu","menu_id":"1","roles":null}'),
(12, 4, 2, 1, 'left', '{"title":"Our team","class":null,"menu":null,"menu_id":null,"roles":null,"content_id":"12"}'),
(14, 4, 1, 1, 'middle', '{"title":null,"html_en":"<div id=\\"mainContent\\" style=\\"margin: 10px 0px 0px; padding: 0px; outline: none 0px; float: none; clear: right; color: rgb(51, 51, 51); font-family: ''Lucida Grande'', ''Lucida Sans Unicode'', Verdana, Arial, Helvetica, sans-serif; font-size: 12.800000190734863px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);\\">\\r\\n<div style=\\"margin: 0px; padding: 0px; outline: none 0px;\\">\\r\\n<p><strong>Use your Java skills to create end-to-end applications that span card readers on embedded devices to back-end systems.<\\/strong><\\/p>\\r\\n\\r\\n<p>JavaFX has already proven to be an excellent platform running on the Raspberry Pi. If you are working on client-side Java development on embedded devices, you probably know that JavaFX and the Raspberry Pi make for a great combination. In a number of cases, embedded devices are used in kiosk environments. A card reader is connected to an embedded device, and users have to scan a card to initiate an action\\u2014for example, to be allowed to enter a room, pay for something, or gain some credits.<\\/p>\\r\\n\\r\\n<p>There are different types of card readers, different types of cards, and different protocols. In this article, we look at how Java SE 8, which is available for the Raspberry Pi, can be used to communicate with a near field communication (NFC) card reader, show a status message on a screen, or send information to a back-end system.<\\/p>\\r\\n\\r\\n<p><strong>Note:<\\/strong>\\u00a0The source code for the examples shown in this article is available at\\u00a0<a href=\\"https:\\/\\/bitbucket.org\\/johanvos\\/javafx-pi-nfc\\" style=\\"margin: 0px; padding: 0px; text-decoration: underline; color: rgb(46, 80, 107); font-weight: bold;\\">https:\\/\\/bitbucket .org\\/johanvos\\/javafx-pi-nfc<\\/a>.<\\/p>\\r\\n\\r\\n<h3>Components of Our System<\\/h3>\\r\\n\\r\\n<p>The NFC specification defines a number of standards for the near field communication between devices, including radio communication between devices that are close to each other. Various cards are equipped with a chipset. In this article, we will use a\\u00a0<a href=\\"http:\\/\\/en.wikipedia.org\\/wiki\\/MIFARE\\" style=\\"margin: 0px; padding: 0px; text-decoration: underline; color: rgb(46, 80, 107); font-weight: bold;\\">MIFARE DESFire card<\\/a>.\\u00a0<\\/p>\\r\\n\\r\\n<div class=\\"c44 c44v0\\" style=\\"margin: 0px; padding: 0px; outline: none 0px;\\">\\r\\n<div class=\\"c44w1\\" style=\\"margin: 0px; padding: 0px; outline: none 0px;\\">\\r\\n<p>You can use your existing Java skills\\u00a0to create end-to-end applications that span different environments.<\\/p>\\r\\n<\\/div>\\r\\n<\\/div>\\r\\n\\r\\n<p>A card reader needs to be chosen that works out of the box on the Raspberry Pi. The USB-based ACR122U card reader from Advanced Card Systems Ltd. is a widely used device that can easily be connected to the Raspberry Pi and allows it to read MIFARE cards.<\\/p>\\r\\n\\r\\n<p><strong>Note:<\\/strong>\\u00a0It is recommended that a powered USB hub be used for connecting the card reader to the Raspberry Pi, because card readers consume a lot of power.<\\/p>\\r\\n\\r\\n<p>In order to have visual feedback, we need to attach a screen to the Raspberry Pi. A variety of screens are available, including a regular HDMI monitor and smaller screens suited for cars, kiosks, and copiers. Although this is the setup we will use in this article, different setups are possible.<\\/p>\\r\\n\\r\\n<h3>Set Up the Raspberry Pi<\\/h3>\\r\\n\\r\\n<p>In order to run the code provided for this project, you need a working setup. We will use an image for the Raspberry Pi that is available for download\\u00a0<a href=\\"http:\\/\\/downloads.raspberrypi.org\\/raspbian\\/images\\/raspbian-2013-12-24\\/2013-12-20-wheezy-raspbian.zip\\" style=\\"margin: 0px; padding: 0px; text-decoration: underline; color: rgb(46, 80, 107); font-weight: bold;\\">here<\\/a>.<\\/p>\\r\\n\\r\\n<p>In order to have the ACR122U card reader work with this image, you have to install two additional packages. Run the following commands to do this:<\\/p>\\r\\n\\r\\n<pre>\\r\\nsudo apt-get update\\r\\nsudo apt-get install libpcsclite1 pcscd\\r\\n<\\/pre>\\r\\n\\r\\n<h3>Install Java<\\/h3>\\r\\n\\r\\n<p>Download\\u00a0<a href=\\"http:\\/\\/www.oracle.com\\/technetwork\\/java\\/javase\\/downloads\\/index.html\\" style=\\"margin: 0px; padding: 0px; text-decoration: underline; color: rgb(46, 80, 107); font-weight: bold;\\">Java SE 8<\\/a>\\u00a0and install on the Raspberry Pi.<\\/p>\\r\\n\\r\\n<p>Java SE comes with a package,\\u00a0<code>javax.smartcardio<\\/code>, that allows developers to communicate with smart card readers. By default, this package will look for a personal computer\\/smart card (PC\\/SC) implementation on the system.<\\/p>\\r\\n\\r\\n<p>Now that you have installed the required Linux packages on your Raspberry Pi, the only remaining task is that we have to point the Java Virtual Machine to the location of the\\u00a0<code>pcsclite<\\/code>\\u00a0library.<\\/p>\\r\\n\\r\\n<p>This is done by setting the system property\\u00a0<code>sun.security.smartcardio.library<\\/code>\\u00a0to the location of the\\u00a0<code>pcsclite<\\/code>\\u00a0library as follows:<\\/p>\\r\\n\\r\\n<pre>\\r\\njava -Dsun.security.smartcardio\\r\\n.library=\\/path\\/to\\/libpcsclite.so\\r\\n<\\/pre>\\r\\n\\r\\n<h3>The JavaFX Application<\\/h3>\\r\\n\\r\\n<p>Making a connection with a card reader and reading cards are operations that can take some time. Hence, it is not a good idea to execute those tasks at the JavaFX application thread level. Instead, we will create a dedicated thread that will deal with the card reader and uses the standard JavaFX way to update the user interface.\\u00a0<\\/p>\\r\\n\\r\\n<div class=\\"c44 c44v0\\" style=\\"margin: 0px; padding: 0px; outline: none 0px;\\">\\r\\n<div class=\\"c44w1\\" style=\\"margin: 0px; padding: 0px; outline: none 0px;\\">\\r\\n<p>In a kiosk, a card reader is connected to an embedded device, and\\u00a0users scan a card to initiate an action.<\\/p>\\r\\n<\\/div>\\r\\n<\\/div>\\r\\n\\r\\n<p><strong>Creating the user interface.<\\/strong>\\u00a0We will create a very simple user interface that shows the identifier of the latest successfully scanned card. This identifier is stored in\\u00a0<code>StringProperty latestId<\\/code>, as follows:<\\/p>\\r\\n\\r\\n<pre>\\r\\nprivate StringProperty latestId = \\r\\nnew SimpleStringProperty(\\" --- \\")\\r\\n<\\/pre>\\r\\n\\r\\n<p>Our simple user interface contains only an\\u00a0<code>HBox<\\/code>\\u00a0with a label containing some static information followed by a label containing the identifier of the latest scanned card. It is constructed as shown in Listing 1.<\\/p>\\r\\n\\r\\n<pre>\\r\\nHBox hbox = new HBox(3);\\r\\nLabel info = new Label(\\"Last checkin by: \\");\\r\\nLabel latestCheckinLabel = new Label();\\r\\nlatestCheckinLabel.textProperty().bind(latestId);\\r\\nhbox.getChildren().addAll(info, latestCheckinLabel);\\r\\nScene scene = new Scene(hbox, 400, 250);\\r\\nprimaryStage.setScene(scene);\\r\\nprimaryStage.show();\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 1<\\/strong><\\/p>\\r\\n\\r\\n<p><strong>Communicating with the card reader.<\\/strong>\\u00a0The thread responsible for the communication with the card reader is created in a function called<code>doCardReaderCommunication<\\/code>, which is called in the\\u00a0<code>start()<\\/code>\\u00a0method of the JavaFX application. A JavaFX\\u00a0<code>Task<\\/code>\\u00a0is created, assigned to a new thread, and started, as follows:<\\/p>\\r\\n\\r\\n<pre>\\r\\nTask task = new Task() { \\r\\n   \\u2026 \\r\\n};\\r\\nThread thread = new Thread(task);\\r\\nthread.start();\\r\\n<\\/pre>\\r\\n\\r\\n<p>This\\u00a0<code>Task<\\/code>\\u00a0performs the following work:\\u00a0<\\/p>\\r\\n\\r\\n<ol>\\r\\n\\t<li>It detects a card reader.<\\/li>\\r\\n\\t<li>It detects a card being recognized by the card reader.<\\/li>\\r\\n\\t<li>It reads the identifier from the card.<\\/li>\\r\\n\\t<li>It updates the value of\\u00a0<code>latestId<\\/code>.<\\/li>\\r\\n\\t<li>It waits for the card to be removed.<\\/li>\\r\\n\\t<li>It goes back to Step 2.\\u00a0<\\/li>\\r\\n<\\/ol>\\r\\n\\r\\n<p><strong>Detecting a card reader.<\\/strong>\\u00a0Before we can access a card reader we need a\\u00a0<code>TerminalFactory<\\/code>. The\\u00a0<code>javax.microcard<\\/code>\\u00a0package provides a static method for getting a default\\u00a0<code>TerminalFactory<\\/code>, and it allows us to create more-specific\\u00a0<code>TerminalFactory<\\/code>\\u00a0instances for different types of card readers. The default\\u00a0<code>TerminalFactory<\\/code>\\u00a0will use the PC\\/SC stack that we installed on the Raspberry Pi, and it is obtained by calling the code shown in\\u00a0<strong>Listing 2<\\/strong>. Using the code shown in\\u00a0<strong>Listing 3<\\/strong>, we can query\\u00a0<code>terminalFactory<\\/code>\\u00a0to detect the card readers that are installed.<\\/p>\\r\\n\\r\\n<pre>\\r\\nTerminalFactory terminalFactory = TerminalFactory.getDefault();\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 2<\\/strong><\\/p>\\r\\n\\r\\n<pre>\\r\\nList&lt;CardTerminal&gt; cardTerminalList = \\r\\nterminalFactory.terminals().list();\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 3<\\/strong><\\/p>\\r\\n\\r\\n<p>In this case, our setup was successful, and\\u00a0<code>cardTerminal List<\\/code>\\u00a0contains one element. This\\u00a0<code>CardTerminal<\\/code>\\u00a0is obtained by running the code shown in\\u00a0<strong>Listing 4<\\/strong>.<\\/p>\\r\\n\\r\\n<pre>\\r\\nCardTerminal cardTerminal = cardTerminalList.get(0);\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 4<\\/strong><\\/p>\\r\\n\\r\\n<p><strong>Detecting cards.<\\/strong>\\u00a0Once we have a reference to the card reader, we can start reading cards. We will do this using the infinite loop shown in\\u00a0<strong>Listing 5<\\/strong>. In real-world cases, a dummy card could be used to stop reading.<\\/p>\\r\\n\\r\\n<pre>\\r\\nwhile (true) {\\r\\n    cardTerminal.waitForCardPresent(0);\\r\\n    handleCard(cardTerminal);\\r\\n    cardTerminal.waitForCardAbsent(0);\\r\\n}\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 5<\\/strong><\\/p>\\r\\n\\r\\n<p>The thread will block until the reader detects a card. Once a card is detected, we will handle it as discussed in the next section. Then, the thread will block again until the card is removed.<\\/p>\\r\\n\\r\\n<p><strong>Handling cards.<\\/strong>\\u00a0The\\u00a0<code>handleCard<\\/code>\\u00a0method is called when we know that a card is presented to the card reader. Note that a card can be removed during the process of reading, and proper exception handling is required.<\\/p>\\r\\n\\r\\n<p>A connection to the card is obtained by calling the code shown in\\u00a0<strong>Listing 6<\\/strong>.<\\/p>\\r\\n\\r\\n<pre>\\r\\nCard card = cardTerminal.connect(\\"*\\");\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 6<\\/strong><\\/p>\\r\\n\\r\\n<p>Before we can read the identifier on the card, we need to know what type of card we are reading. Different card types use different addresses to store the identifier. In our simple demo, we will limit ourselves to MIFARE DESFire cards, but the example can easily be extended to other card types.<\\/p>\\r\\n\\r\\n<p>Information on the type of card we are reading can be obtained by inspecting the ATR (answer-to-reset) bytes of the card as follows:<\\/p>\\r\\n\\r\\n<pre>\\r\\nATR atr = card.getATR();\\r\\n<\\/pre>\\r\\n\\r\\n<p>We compare the bytes in this\\u00a0<code>atr<\\/code>\\u00a0object with the bytes we expect on a DESFire card, as follows:<\\/p>\\r\\n\\r\\n<pre>\\r\\nArrays.equals(\\r\\ndesfire, atr.getBytes());\\r\\n<\\/pre>\\r\\n\\r\\n<p>As shown in\\u00a0<strong>Listing 7<\\/strong>,\\u00a0<code>desfire<\\/code>\\u00a0is a static byte array containing the information we expect.<\\/p>\\r\\n\\r\\n<pre>\\r\\nstatic byte[] desfire = \\r\\nnew byte[]{0x3b, (byte) 0x81, (byte) 0x80,\\r\\n        0x01, (byte) 0x80, (byte) 0x80};\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 7<\\/strong><\\/p>\\r\\n\\r\\n<p>If we have a supported card type (that is, a DESFire card), we can query the identifier. In order to do so, we need to transmit an application protocol data unit (APDU) command over the basic logic channel of the card, and then read the response. Using the\\u00a0<code>javax .microcard<\\/code>\\u00a0package, this is done as shown in\\u00a0<strong>Listing 8<\\/strong>.<\\/p>\\r\\n\\r\\n<pre>\\r\\nCardChannel channel = card.getBasicChannel();\\r\\nCommandAPDU command = new CommandAPDU(getAddress);\\r\\nResponseAPDU response = channel.transmit(command);\\r\\nbyte[] uidBytes = response.getData();\\r\\nfinal String uid = readable(uidBytes);\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 8<\\/strong><\\/p>\\r\\n\\r\\n<p>The\\u00a0<code>getAddress<\\/code>\\u00a0parameter we are passing is a static byte array that defines how to read the identifier for a DESFire card, and it is declared as shown in\\u00a0<strong>Listing 9<\\/strong>. Different card types might require a different byte array.<\\/p>\\r\\n\\r\\n<pre>\\r\\nstatic byte[] getAddress = \\r\\nnew byte[]{(byte) 0xff, (byte) 0xca, 0, 0, 0};\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 9<\\/strong><\\/p>\\r\\n\\r\\n<p>The function\\u00a0<code>readable(byte[])<\\/code>\\u00a0translates the byte array into a more human-readable format by converting each byte to its hexadecimal\\u00a0<code>String<\\/code>\\u00a0representation (see\\u00a0<strong>Listing 10<\\/strong>).<\\/p>\\r\\n\\r\\n<pre>\\r\\nprivate static String readable(byte[] src) {\\r\\n        String answer = \\"\\";\\r\\n        for (byte b : src) {\\r\\n            answer = answer + String.format(\\"%02X\\", b);\\r\\n        }\\r\\n        return answer;\\r\\n}\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 10<\\/strong>\\u00a0<\\/p>\\r\\n\\r\\n<p>Now that we have a readable identifier for the card, we need to show the identifier in the user interface. Because the identifier is obtained on a thread different from the JavaFX application thread (that is, from the communication thread), we need to make sure that we don\\u2019t change the\\u00a0<code>latestId StringProperty<\\/code>\\u00a0directly from the communication thread. Rather, we need to put the change on the JavaFX application thread as shown here.<\\/p>\\r\\n\\r\\n<pre>\\r\\nPlatform.runLater(new Runnable() {\\r\\n    public void run() {\\r\\n        latestId.setValue(uid);\\r\\n    }\\r\\n});\\r\\n<\\/pre>\\r\\n\\r\\n<p>This separation of concerns also guarantees that the communication with the card reader is not disturbed by activity in the user interface and vice versa.<\\/p>\\r\\n\\r\\n<p><strong>Sending the data to a back end.<\\/strong>\\u00a0In many cases, visual feedback is required when a reader scans a card. In other cases, the information needs to be sent to a back-end system. In the next section, we will send data to the back end using a very simple client-server module by which identifiers are sent from the Raspberry Pi to a Java EE 7 back end and then visualized on a web page. Sending data from a JavaFX application to a REST-based back-end system is easily done using\\u00a0<a href=\\"http:\\/\\/javafxdata.org\\/\\" style=\\"margin: 0px; padding: 0px; text-decoration: underline; color: rgb(46, 80, 107); font-weight: bold;\\">DataFX<\\/a>.<\\/p>\\r\\n\\r\\n<p>The code shown in\\u00a0<strong>Listing 11<\\/strong>\\u00a0will send the identifier\\u00a0<code>uid<\\/code>\\u00a0to a REST endpoint at http:\\/\\/ 192.168.1.6:8080\\/webmonitor\\/ rest\\/card\\/checkin.<\\/p>\\r\\n\\r\\n<pre>\\r\\nprivate void sendToBackend(String uid) {\\r\\n      RestSource restSource = RestSourceBuilder.create()\\r\\n              .host(\\"http:\\/\\/192.168.1.6:8080\\")\\r\\n              .path(\\"webmonitor\\")\\r\\n              .path(\\"rest\\/card\\/checkin\\")\\r\\n              .formParam(\\"id\\", uid)\\r\\n              .build();\\r\\n      ObjectDataProvider odp = ObjectDataProviderBuilder.create()\\r\\n              .dataReader(restSource).build();\\r\\n      odp.retrieve();\\r\\n  }\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 11<\\/strong><\\/p>\\r\\n\\r\\n<p>Note that we will use the\\u00a0<code>POST<\\/code>\\u00a0method, because a form parameter is specified in the REST request. This method should be called whenever a new identifier is read. Because DataFX deals with the threading, we have to call this method from the JavaFX application thread. Hence, we can do this in the same code block in which we set the value of the\\u00a0<code>latestId<\\/code>\\u00a0property (see\\u00a0<strong>Listing 12<\\/strong>).<\\/p>\\r\\n\\r\\n<pre>\\r\\nPlatform.runLater(new Runnable() {\\r\\n    public void run() {\\r\\n        latestId.setValue(uid);\\r\\n        sendToBackend(uid);\\r\\n    }\\r\\n});\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 12<\\/strong><\\/p>\\r\\n\\r\\n<p>In real-world applications, the server might send additional information, such as the real name of the user of the card, back to the client. The client application can use this information to make the user interface more personal.<\\/p>\\r\\n\\r\\n<h3>The Back End<\\/h3>\\r\\n\\r\\n<p>We will now create a very simple back end that accepts the REST requests and shows them on a web page. Because it is not assumed that the user of the web page knows when cards are being scanned, it makes sense to dynamically update the web page whenever a card has been scanned. This can easily be done leveraging the WebSocket API in Java EE 7. It is surprising how little code is required for doing this.\\u00a0<\\/p>\\r\\n\\r\\n<div class=\\"c44 c44v0\\" style=\\"margin: 0px; padding: 0px; outline: none 0px;\\">\\r\\n<div class=\\"c44w1\\" style=\\"margin: 0px; padding: 0px; outline: none 0px;\\">\\r\\n<p>It\\u2019s easy to create\\u00a0an embedded Java application, enrich it with a simple JavaFX user interface, connect it to a card reader, connect it to a Java EE system, and visualize the information using HTML5.<\\/p>\\r\\n<\\/div>\\r\\n<\\/div>\\r\\n\\r\\n<p>We will start with the web page, which is a simple HTML page with a division named\\u00a0<code>checkins<\\/code>\\u00a0that will contain the list of identifiers and a time stamp indicating when each card scan happened.<\\/p>\\r\\n\\r\\n<p>When the HTML page is loaded, a WebSocket is created, pointing to our simple back end. The content of the\\u00a0<code>checkins<\\/code>\\u00a0division is populated whenever a message arrives over the WebSocket. This is easily achieved using the code shown in\\u00a0<strong>Listing 13<\\/strong>.<\\/p>\\r\\n\\r\\n<pre>\\r\\nfunction openConnection() {\\r\\n    connection = \\r\\nnew WebSocket(&#39;ws:\\/\\/localhost:8080\\/webmonitor\\/endpoint&#39;);\\r\\n\\r\\n    connection.onmessage = function(evt) {\\r\\n        var date = new Date();\\r\\n        var chld = document.createElement(\\"p\\");\\r\\n        chld.innerHTML = date + \\" :  \\" + evt.data;\\r\\n        var messages = document.getElementById(\\"checkins\\");\\r\\n        messages.appendChild(chld);\\r\\n    };\\r\\n}\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 13<\\/strong><\\/p>\\r\\n\\r\\n<p>Note that the HTML page will open a WebSocket toward localhost:8080\\/webmonitor\\/endpoint, while the JavaFX application will push identifiers to localhost:8080\\/webmonitor\\/rest\\/card\\/checkin. The glue for this is our single-file back-end system, implemented in a class named\\u00a0<code>CardHandler<\\/code>.<\\/p>\\r\\n\\r\\n<p>Because\\u00a0<code>CardHandler<\\/code>\\u00a0provides a REST endpoint, it extends\\u00a0<code>javax .ws.rs.core.Application<\\/code>, and it contains an<code>@ApplicationPath<\\/code>\\u00a0annotation specifying that the REST interface is located at the\\u00a0<code>rest<\\/code>\\u00a0value. The handler itself is registered at a path<code>card<\\/code>, and the particular method for receiving identifiers is associated with the path\\u00a0<code>checkin<\\/code>. This method also takes a form parameter named\\u00a0<code>id<\\/code>, which contains the identifier (see\\u00a0<strong>Listing 14<\\/strong>).<\\/p>\\r\\n\\r\\n<pre>\\r\\n@ApplicationPath(\\"rest\\")\\r\\n@Path(\\"card\\")\\r\\npublic class CardHandler extends Application {\\r\\n\\r\\n    @Path(\\"checkin\\")\\r\\n    @POST\\r\\n    public Response checkin(\\r\\n@FormParam(\\"id\\") String id) throws IOException {\\r\\n        System.out.println(\\"Received card with id \\" + id);\\r\\n        return Response.ok().build();\\r\\n    }\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 14<\\/strong><\\/p>\\r\\n\\r\\n<p>If we want to send this information to WebSocket clients, for example, to the simple web page we created in the previous section, we need to register a WebSocket endpoint as well. We can leverage\\u00a0<code>CardHandler<\\/code>\\u00a0for this, because a class can have multiple annotations. Doing so, we put all the back-end code into a single file. This is probably not the best idea for a more complex production system, but by doing it here, we can see how easy it is to create an enterprise application in Java, combining REST and WebSockets.<\\/p>\\r\\n\\r\\n<p>When the WebSocket endpoint receives a connection request, the created session is stored in a\\u00a0<code>Set<\\/code>. When the connection is closed, the session is removed from the\\u00a0<code>Set<\\/code>, as shown in\\u00a0<strong>Listing 15<\\/strong>.<\\/p>\\r\\n\\r\\n<pre>\\r\\n@ServerEndpoint(value=\\"\\/endpoint\\")\\r\\npublic class CardHandler extends Application {\\r\\n\\r\\n    private static Set&lt;Session&gt; sessions = new HashSet&lt;&gt;();\\r\\n    \\r\\n    @OnOpen\\r\\n    public void onOpen (Session s) {\\r\\n        sessions.add(s);\\r\\n    }\\r\\n    \\r\\n    @OnClose\\r\\n    public void onClose (Session s) {\\r\\n        sessions.remove(s);\\r\\n    }\\r\\n\\r\\n}\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 15<\\/strong><\\/p>\\r\\n\\r\\n<p>We can now modify the\\u00a0<code>checkin<\\/code>\\u00a0method created in\\u00a0<strong>Listing 14<\\/strong>, and send the identifier to the connected WebSocket clients, as shown in<strong>Listing 16<\\/strong>.<\\/p>\\r\\n\\r\\n<pre>\\r\\n   @Path(\\"checkin\\")\\r\\n    @POST\\r\\n    public Response checkin(@FormParam(\\"id\\") String id) throws IOException {\\r\\n        System.out.println(\\"Received card with id \\" + id);\\r\\n        System.out.println(\\"sessions = \\"+sessions);\\r\\n        for (Session session: sessions) {\\r\\n            session.getBasicRemote().sendText(id);\\r\\n        }\\r\\n        return Response.ok().build();\\r\\n    }\\r\\n<\\/pre>\\r\\n\\r\\n<p><strong>Listing 16<\\/strong><\\/p>\\r\\n\\r\\n<h3>Conclusion<\\/h3>\\r\\n\\r\\n<p>In this article, we saw how easy it is to create an embedded Java application, enrich it with a simple JavaFX user interface, connect it to external hardware (a card reader), connect it to a Java EE system, and visualize the information using HTML5.<\\/p>\\r\\n\\r\\n<p>The underlying systems all use the same Java SE code. As a consequence, developers can use their existing Java skills to create end-to-end applications that span different environments.<\\/p>\\r\\n\\r\\n<p>Originally published in the March\\/April 2014 issue of\\u00a0<em>Java Magazine<\\/em>.\\u00a0<a href=\\"http:\\/\\/www.oracle.com\\/technetwork\\/java\\/javamagazine\\/index.html\\" style=\\"margin: 0px; padding: 0px; text-decoration: underline; color: rgb(46, 80, 107); font-weight: bold;\\" target=\\"_blank\\">Subscribe today<\\/a>.<\\/p>\\r\\n\\r\\n<p><strong>About the author<\\/strong><\\/p>\\r\\n\\r\\n<table align=\\"left\\" cellspacing=\\"5\\" style=\\"background-color:rgb(249, 250, 251); border-collapse:collapse; margin:0px 0px 20px; padding:0px\\">\\r\\n\\t<tbody>\\r\\n\\t\\t<tr>\\r\\n\\t\\t\\t<td>\\r\\n\\t\\t\\t<p><img alt=\\"vos-headshot\\" src=\\"http:\\/\\/www.oracle.com\\/ocom\\/groups\\/public\\/@otn\\/documents\\/digitalasset\\/2179022.jpg\\" style=\\"border:0px solid; margin:0px; padding:0px\\" \\/><\\/p>\\r\\n\\r\\n\\t\\t\\t<div class=\\"caption\\" style=\\"margin: 0px auto; padding: 0px; outline: none 0px; text-align: center;\\">vos-headshot<\\/div>\\r\\n\\r\\n\\t\\t\\t<p>\\u00a0<\\/p>\\r\\n\\t\\t\\t<\\/td>\\r\\n\\t\\t<\\/tr>\\r\\n\\t<\\/tbody>\\r\\n<\\/table>\\r\\n\\r\\n<p><strong>Johan Vos<\\/strong>\\u00a0started working with Java in 1995. He is a cofounder of LodgON, where he is working on Java-based solutions for social networking software. An enthusiast of both embedded and enterprise development, Vos focuses on end-to-end Java using JavaFX and Java EE.<\\/p>\\r\\n\\r\\n<p>\\u00a0<\\/p>\\r\\n\\r\\n<p>\\u00a0<\\/p>\\r\\n\\r\\n<p>\\u00a0<\\/p>\\r\\n\\r\\n<p>\\u00a0<\\/p>\\r\\n\\r\\n<p>(1)\\u00a0Originally published in the\\u00a0March\\/April 2014\\u00a0Edition of Java Magazine\\u00a0<br \\/>\\r\\n(2) Copyright \\u00a9 [2014] Oracle.<\\/p>\\r\\n<\\/div>\\r\\n<\\/div>\\r\\n\\r\\n<div class=\\"article-author\\" style=\\"margin: 10px 0px 20px; padding: 0px; outline: none 0px; text-align: right; font-style: italic; color: rgb(51, 51, 51); font-family: ''Lucida Grande'', ''Lucida Sans Unicode'', Verdana, Arial, Helvetica, sans-serif; font-size: 12.800000190734863px; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);\\">Johan Vos<\\/div>\\r\\n","roles":["3"],"content_id":"14"}'),
(17, 4, 2, 0, 'right', '{"title":"Khanh beo","class":null,"menu":"Our team","menu_id":"2","roles":null}'),
(25, 6, 6, 0, 'middle', '[]'),
(27, 2, 5, 0, 'middle', '[]'),
(28, 5, 4, 0, 'middle', '[]');

-- --------------------------------------------------------

--
-- Table structure for table `issue`
--

CREATE TABLE IF NOT EXISTS `issue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `summary` varchar(1024) NOT NULL,
  `detail` text NOT NULL,
  `assigned_user` int(11) DEFAULT NULL,
  `estimate` int(2) DEFAULT NULL,
  `logged` int(2) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-issue-project-project_id-id` (`project_id`),
  KEY `fki-issue-users-assigned_user-id` (`assigned_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `language` varchar(2) NOT NULL,
  `locale` varchar(5) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `language`, `locale`, `icon`) VALUES
(1, 'English', 'en', 'en_US', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `language_translations`
--

CREATE TABLE IF NOT EXISTS `language_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `scope` varchar(25) DEFAULT NULL,
  `original` text NOT NULL,
  `translated` text NOT NULL,
  `checked` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-language_translations-languages-language_id-id` (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=499 ;

--
-- Dumping data for table `language_translations`
--

INSERT INTO `language_translations` (`id`, `language_id`, `scope`, `original`, `translated`, `checked`) VALUES
(1, 1, NULL, 'Home', 'Home', 0),
(2, 1, NULL, 'Are you really want to delete this item?', 'Are you really want to delete this item?', 0),
(3, 1, NULL, 'Close this window?', 'Close this window?', 0),
(4, 1, NULL, 'PhalconEye', 'PhalconEye', 0),
(5, 1, NULL, 'PhalconEye Home Page', 'PhalconEye Home Page', 0),
(6, 1, NULL, 'Login', 'Login', 0),
(7, 1, NULL, 'Register', 'Register', 0),
(8, 1, NULL, 'Github', 'Github', 0),
(9, 1, NULL, 'Header', 'Header', 0),
(10, 1, NULL, 'Header2', 'Header2', 0),
(11, 1, NULL, 'Left', 'Left', 0),
(12, 1, NULL, 'Left2', 'Left2', 0),
(13, 1, NULL, 'Right', 'Right', 0),
(14, 1, NULL, 'Right2', 'Right2', 0),
(15, 1, NULL, 'Content', 'Content', 0),
(16, 1, NULL, 'Content2', 'Content2', 0),
(17, 1, NULL, 'Use you email or username to login.', 'Use you email or username to login.', 0),
(18, 1, NULL, 'Password', 'Password', 0),
(19, 1, NULL, 'Enter', 'Enter', 0),
(20, 1, NULL, 'Welcome, ', 'Welcome, ', 0),
(21, 1, NULL, 'Admin panel', 'Admin panel', 0),
(22, 1, NULL, 'Logout', 'Logout', 0),
(23, 1, NULL, 'Dashboard', 'Dashboard', 0),
(24, 1, NULL, 'Manage', 'Manage', 0),
(25, 1, NULL, 'Users and Roles', 'Users and Roles', 0),
(26, 1, NULL, 'Pages', 'Pages', 0),
(27, 1, NULL, 'Menus', 'Menus', 0),
(28, 1, NULL, 'Languages', 'Languages', 0),
(29, 1, NULL, 'Files', 'Files', 0),
(30, 1, NULL, 'Packages', 'Packages', 0),
(31, 1, NULL, 'Settings', 'Settings', 0),
(32, 1, NULL, 'System', 'System', 0),
(33, 1, NULL, 'Performance', 'Performance', 0),
(34, 1, NULL, 'Access Rights', 'Access Rights', 0),
(35, 1, NULL, 'Back to site', 'Back to site', 0),
(36, 1, NULL, 'Debug mode', 'Debug mode', 0),
(37, 1, 'core', 'ID', 'ID', 0),
(38, 1, 'core', 'Title', 'Title', 0),
(39, 1, 'core', 'Url', 'Url', 0),
(40, 1, 'core', 'Layout', 'Layout', 0),
(41, 1, 'core', 'Controller', 'Controller', 0),
(42, 1, NULL, 'Are you really want to delete this page?', 'Are you really want to delete this page?', 0),
(43, 1, NULL, 'Browse', 'Browse', 0),
(44, 1, NULL, 'Create new page', 'Create new page', 0),
(45, 1, NULL, 'Actions', 'Actions', 0),
(46, 1, NULL, 'Filter', 'Filter', 0),
(47, 1, NULL, 'Reset', 'Reset', 0),
(48, 1, NULL, 'Edit', 'Edit', 0),
(49, 1, NULL, 'Not Found', 'Not Found', 0),
(50, 1, NULL, 'Page not saved! Dou you want to leave?', 'Page not saved! Dou you want to leave?', 0),
(51, 1, NULL, 'Error while saving...', 'Error while saving...', 0),
(52, 1, NULL, 'Save (NOT  SAVED)', 'Save (NOT  SAVED)', 0),
(53, 1, NULL, 'Save', 'Save', 0),
(54, 1, NULL, 'Footer', 'Footer', 0),
(55, 1, NULL, 'If you switch to new layout you will lose some widgets, are you shure?', 'If you switch to new layout you will lose some widgets, are you shure?', 0),
(56, 1, NULL, 'Manage page', 'Manage page', 0),
(57, 1, NULL, 'Change layout', 'Change layout', 0),
(58, 1, NULL, 'Select layout type for current page', 'Select layout type for current page', 0),
(59, 1, NULL, 'Saving...', 'Saving...', 0),
(60, 1, 'core', 'Wrong controller name. Example: NameController->someAction', 'Wrong controller name. Example: NameController->someAction', 0),
(61, 1, NULL, 'Page Editing', 'Page Editing', 0),
(62, 1, NULL, 'Edit Page', 'Edit Page', 0),
(63, 1, NULL, 'Edit this page.', 'Edit this page.', 0),
(64, 1, NULL, 'Description', 'Description', 0),
(65, 1, NULL, 'Keywords', 'Keywords', 0),
(66, 1, NULL, 'Cancel', 'Cancel', 0),
(67, 1, NULL, 'Page Creation', 'Page Creation', 0),
(68, 1, NULL, 'Create new page.', 'Create new page.', 0),
(69, 1, NULL, 'Page will be available under http://localhost/page/[URL NAME]', 'Page will be available under http://localhost/page/[URL NAME]', 0),
(70, 1, NULL, 'Controller and action name that will handle this page. Example: NameController->someAction', 'Controller and action name that will handle this page. Example: NameController->someAction', 0),
(71, 1, NULL, 'Roles', 'Roles', 0),
(72, 1, NULL, 'If no value is selected, will be allowed to all (also as all selected).', 'If no value is selected, will be allowed to all (also as all selected).', 0),
(73, 1, NULL, 'Admin', 'Admin', 0),
(74, 1, NULL, 'User', 'User', 0),
(75, 1, NULL, 'Guest', 'Guest', 0),
(76, 1, NULL, 'Create', 'Create', 0),
(77, 1, NULL, 'Menu', 'Menu', 0),
(78, 1, NULL, 'Select menu that will be rendered.', 'Select menu that will be rendered.', 0),
(79, 1, NULL, 'Menu css class', 'Menu css class', 0),
(80, 1, NULL, 'Start typing to see menus variants', 'Start typing to see menus variants', 0),
(81, 1, NULL, 'Close', 'Close', 0),
(82, 1, 'core', 'HTML block, for:', 'HTML block, for:', 0),
(83, 1, NULL, 'HtmlBlock', 'HtmlBlock', 0),
(84, 1, NULL, 'HTML (EN)', 'HTML (EN)', 0),
(85, 1, NULL, 'Delete', 'Delete', 0),
(86, 1, NULL, 'Settings for header of you site.', 'Settings for header of you site.', 0),
(87, 1, NULL, 'Logo image (url)', 'Logo image (url)', 0),
(88, 1, NULL, 'Select file', 'Select file', 0),
(89, 1, NULL, 'Show site title', 'Show site title', 0),
(90, 1, NULL, 'Show authentication links (logo, register, logout, etc)', 'Show authentication links (logo, register, logout, etc)', 0),
(91, 1, 'core', 'Menu title', 'Menu title', 0),
(92, 1, NULL, 'Create new menu', 'Create new menu', 0),
(93, 1, NULL, 'Menu Creation', 'Menu Creation', 0),
(94, 1, NULL, 'Create new menu.', 'Create new menu.', 0),
(95, 1, NULL, 'Name', 'Name', 0),
(96, 1, NULL, 'Manage menu', 'Manage menu', 0),
(97, 1, NULL, 'Add new item', 'Add new item', 0),
(98, 1, NULL, 'Saved...', 'Saved...', 0),
(99, 1, NULL, 'Remove', 'Remove', 0),
(100, 1, NULL, 'Create new menu item', 'Create new menu item', 0),
(101, 1, NULL, 'This menu item will be available under menu or parent menu item.', 'This menu item will be available under menu or parent menu item.', 0),
(102, 1, NULL, 'Target', 'Target', 0),
(103, 1, NULL, 'Link type', 'Link type', 0),
(104, 1, NULL, 'Default link', 'Default link', 0),
(105, 1, NULL, 'Opens the linked document in a new window or tab', 'Opens the linked document in a new window or tab', 0),
(106, 1, NULL, 'Opens the linked document in the parent frame', 'Opens the linked document in the parent frame', 0),
(107, 1, NULL, 'Opens the linked document in the full body of the window', 'Opens the linked document in the full body of the window', 0),
(108, 1, NULL, 'Select url type', 'Select url type', 0),
(109, 1, NULL, 'System page', 'System page', 0),
(110, 1, NULL, 'Do not type url with starting slash... Example: "somepage/url/to?param=1"', 'Do not type url with starting slash... Example: "somepage/url/to?param=1"', 0),
(111, 1, NULL, 'Page', 'Page', 0),
(112, 1, NULL, 'Start typing to see pages variants.', 'Start typing to see pages variants.', 0),
(113, 1, NULL, 'OnClick', 'OnClick', 0),
(114, 1, NULL, 'Type JS action that will be performed when this menu item is selected.', 'Type JS action that will be performed when this menu item is selected.', 0),
(115, 1, NULL, 'Tooltip', 'Tooltip', 0),
(116, 1, NULL, 'Tooltip position', 'Tooltip position', 0),
(117, 1, NULL, 'Top', 'Top', 0),
(118, 1, NULL, 'Bottom', 'Bottom', 0),
(119, 1, NULL, 'Select icon', 'Select icon', 0),
(120, 1, NULL, 'Icon position', 'Icon position', 0),
(121, 1, NULL, 'Choose the language in which the menu item will be displayed.                    If no one selected - will be displayed at all.', 'Choose the language in which the menu item will be displayed.                    If no one selected - will be displayed at all.', 0),
(122, 1, NULL, 'English', 'English', 0),
(123, 1, NULL, 'Is enabled', 'Is enabled', 0),
(124, 1, NULL, 'Items: ', 'Items: ', 0),
(125, 1, NULL, 'Edit menu item', 'Edit menu item', 0),
(126, 1, NULL, 'About Us', 'About Us', 0),
(127, 1, NULL, 'Khanh', 'Khanh', 0),
(128, 1, NULL, 'Do you really want to delete this item?', 'Do you really want to delete this item?', 0),
(129, 1, NULL, 'Clear cache', 'Clear cache', 0),
(130, 1, 'user', 'Username', 'Username', 0),
(131, 1, 'user', 'Email', 'Email', 0),
(132, 1, 'user', 'Role', 'Role', 0),
(133, 1, 'user', 'Creation Date', 'Creation Date', 0),
(134, 1, NULL, 'Users', 'Users', 0),
(135, 1, NULL, 'Are you really want to delete this user?', 'Are you really want to delete this user?', 0),
(136, 1, NULL, 'Create new user', 'Create new user', 0),
(137, 1, NULL, 'Create new role', 'Create new role', 0),
(138, 1, NULL, 'Register you account!', 'Register you account!', 0),
(139, 1, NULL, 'Register your account!', 'Register your account!', 0),
(140, 1, NULL, 'You will use your email address to login.', 'You will use your email address to login.', 0),
(141, 1, NULL, 'Passwords must be at least 6 characters in length.', 'Passwords must be at least 6 characters in length.', 0),
(142, 1, NULL, 'RepeatPassword', 'RepeatPassword', 0),
(143, 1, NULL, 'Enter your password again for confirmation.', 'Enter your password again for confirmation.', 0),
(144, 1, NULL, 'System settings', 'System settings', 0),
(145, 1, NULL, 'All system settings here.', 'All system settings here.', 0),
(146, 1, NULL, 'Site name', 'Site name', 0),
(147, 1, NULL, 'Theme', 'Theme', 0),
(148, 1, NULL, 'Default', 'Default', 0),
(149, 1, NULL, 'Light', 'Light', 0),
(150, 1, NULL, 'Default language', 'Default language', 0),
(151, 1, NULL, 'Auto detect', 'Auto detect', 0),
(152, 1, NULL, 'Performance settings', 'Performance settings', 0),
(153, 1, NULL, 'Cache prefix', 'Cache prefix', 0),
(154, 1, NULL, 'Example: "pe_"', 'Example: "pe_"', 0),
(155, 1, NULL, 'Cache lifetime', 'Cache lifetime', 0),
(156, 1, NULL, 'This determines how long the system will keep cached data before                    reloading it from the database server.                    A shorter cache lifetime causes greater database server CPU usage,                    however the data will be more current.', 'This determines how long the system will keep cached data before                    reloading it from the database server.                    A shorter cache lifetime causes greater database server CPU usage,                    however the data will be more current.', 0),
(157, 1, NULL, 'Cache adapter', 'Cache adapter', 0),
(158, 1, NULL, 'Cache type. Where cache will be stored.', 'Cache type. Where cache will be stored.', 0),
(159, 1, NULL, 'File', 'File', 0),
(160, 1, NULL, 'Memcached', 'Memcached', 0),
(161, 1, NULL, 'APC', 'APC', 0),
(162, 1, NULL, 'Mongo', 'Mongo', 0),
(163, 1, NULL, 'Files location', 'Files location', 0),
(164, 1, NULL, 'Memcached host', 'Memcached host', 0),
(165, 1, NULL, 'Memcached port', 'Memcached port', 0),
(166, 1, NULL, 'Create a persistent connection to memcached?', 'Create a persistent connection to memcached?', 0),
(167, 1, NULL, 'A MongoDB connection string', 'A MongoDB connection string', 0),
(168, 1, NULL, 'Mongo database name', 'Mongo database name', 0),
(169, 1, NULL, 'Mongo collection in the database', 'Mongo collection in the database', 0),
(170, 1, NULL, 'All system cache will be cleaned.', 'All system cache will be cleaned.', 0),
(171, 1, NULL, 'Packages management - Modules', 'Packages management - Modules', 0),
(172, 1, NULL, 'Are you really want to remove this package? Once removed, it can not be restored.', 'Are you really want to remove this package? Once removed, it can not be restored.', 0),
(173, 1, NULL, 'Modules', 'Modules', 0),
(174, 1, NULL, 'Themes', 'Themes', 0),
(175, 1, NULL, 'Widgets', 'Widgets', 0),
(176, 1, NULL, 'Plugins', 'Plugins', 0),
(177, 1, NULL, 'Libraries', 'Libraries', 0),
(178, 1, NULL, 'Upload new package', 'Upload new package', 0),
(179, 1, NULL, 'Create new package', 'Create new package', 0),
(180, 1, NULL, 'Packages management - Widgets', 'Packages management - Widgets', 0),
(181, 1, NULL, 'No packages', 'No packages', 0),
(182, 1, 'core', 'Name must be in lowercase and contains only letters.', 'Name must be in lowercase and contains only letters.', 0),
(183, 1, 'core', 'Version must be in correct format: 1.0.0 or 1.0.0.0', 'Version must be in correct format: 1.0.0 or 1.0.0.0', 0),
(184, 1, NULL, 'Package Creation', 'Package Creation', 0),
(185, 1, NULL, 'Create new package.', 'Create new package.', 0),
(186, 1, NULL, 'Package type', 'Package type', 0),
(187, 1, NULL, 'Module', 'Module', 0),
(188, 1, NULL, 'Plugin', 'Plugin', 0),
(189, 1, NULL, 'Widget', 'Widget', 0),
(190, 1, NULL, 'Library', 'Library', 0),
(191, 1, NULL, 'Version', 'Version', 0),
(192, 1, NULL, 'Type package version. Ex.: 0.5.7', 'Type package version. Ex.: 0.5.7', 0),
(193, 1, NULL, 'Author', 'Author', 0),
(194, 1, NULL, 'Who create this package? Identify yourself!', 'Who create this package? Identify yourself!', 0),
(195, 1, NULL, 'Website', 'Website', 0),
(196, 1, NULL, 'Where user will look for new version?', 'Where user will look for new version?', 0),
(197, 1, NULL, 'Header comments', 'Header comments', 0),
(198, 1, NULL, 'This text will be placed in each file of package. Use comment block /**  **/.', 'This text will be placed in each file of package. Use comment block /**  **/.', 0),
(199, 1, NULL, 'Widget information', 'Widget information', 0),
(200, 1, NULL, 'Is related to module?', 'Is related to module?', 0),
(201, 1, NULL, 'No', 'No', 0),
(202, 1, NULL, 'Core', 'Core', 0),
(203, 1, NULL, 'Is Paginated?', 'Is Paginated?', 0),
(204, 1, NULL, 'If this enabled - widget will has additional control                    enabled for allowed per page items count selection in admin form', 'If this enabled - widget will has additional control                    enabled for allowed per page items count selection in admin form', 0),
(205, 1, NULL, 'Is ACL controlled?', 'Is ACL controlled?', 0),
(206, 1, NULL, 'If this enabled - widget will has additional control                    enabled for allowed roles selection in admin form', 'If this enabled - widget will has additional control                    enabled for allowed roles selection in admin form', 0),
(207, 1, NULL, 'Admin form', 'Admin form', 0),
(208, 1, NULL, 'Does this widget have some controlling form?', 'Does this widget have some controlling form?', 0),
(209, 1, NULL, 'Action', 'Action', 0),
(210, 1, NULL, 'Form class', 'Form class', 0),
(211, 1, NULL, 'Enter existing form class', 'Enter existing form class', 0),
(212, 1, NULL, 'Enabled?', 'Enabled?', 0),
(213, 1, NULL, 'Menu Editing', 'Menu Editing', 0),
(214, 1, NULL, 'Edit Menu', 'Edit Menu', 0),
(215, 1, NULL, 'Edit this menu.', 'Edit this menu.', 0),
(216, 1, 'core', 'Language', 'Language', 0),
(217, 1, 'core', 'Locale', 'Locale', 0),
(218, 1, 'core', 'Icon', 'Icon', 0),
(219, 1, NULL, 'Create new language', 'Create new language', 0),
(220, 1, NULL, 'Compile languages', 'Compile languages', 0),
(221, 1, NULL, 'Import...', 'Import...', 0),
(222, 1, NULL, 'No icon', 'No icon', 0),
(223, 1, NULL, 'Export', 'Export', 0),
(224, 1, NULL, 'Files management', 'Files management', 0),
(225, 1, NULL, 'Install new package', 'Install new package', 0),
(226, 1, NULL, 'Select package you want to install (zip extension).', 'Select package you want to install (zip extension).', 0),
(227, 1, NULL, 'Package', 'Package', 0),
(228, 1, NULL, 'Upload', 'Upload', 0),
(229, 1, NULL, 'Test', 'Test', 0),
(230, 1, NULL, 'Events', 'Events', 0),
(231, 1, NULL, 'Disable', 'Disable', 0),
(232, 1, NULL, 'Uninstall', 'Uninstall', 0),
(233, 1, NULL, 'Export Package', 'Export Package', 0),
(234, 1, NULL, 'Select package dependency (not necessarily).', 'Select package dependency (not necessarily).', 0),
(235, 1, NULL, 'Khanh beo', 'Khanh beo', 0),
(236, 1, NULL, 'Blog', 'Blog', 0),
(237, 1, NULL, 'Module settings', 'Module settings', 0),
(238, 1, NULL, 'This module has no settings...', 'This module has no settings...', 0),
(239, 1, NULL, 'Available resources', 'Available resources', 0),
(240, 1, NULL, 'Resource name', 'Resource name', 0),
(241, 1, NULL, 'Options', 'Options', 0),
(242, 1, NULL, 'FAQ', 'FAQ', 0),
(243, 1, NULL, 'User Details', 'User Details', 0),
(244, 1, NULL, 'Role id', 'Role id', 0),
(245, 1, NULL, 'Modified date', 'Modified date', 0),
(246, 1, NULL, 'Back', 'Back', 0),
(247, 1, NULL, 'Packages management - Libraries', 'Packages management - Libraries', 0),
(248, 1, NULL, 'Edit package', 'Edit package', 0),
(249, 1, NULL, 'Edit this package.', 'Edit this package.', 0),
(250, 1, NULL, 'Create new post', 'Create new post', 0),
(251, 1, NULL, 'Post Creation', 'Post Creation', 0),
(252, 1, NULL, 'Create new post.', 'Create new post.', 0),
(253, 1, NULL, 'Tags', 'Tags', 0),
(254, 1, NULL, 'No items', 'No items', 0),
(255, 1, 'blog', 'Post title', 'Post title', 0),
(256, 1, NULL, 'Posts', 'Posts', 0),
(257, 1, NULL, 'Are you really want to delete this post?', 'Are you really want to delete this post?', 0),
(259, 1, NULL, 'Edit Post', 'Edit Post', 0),
(260, 1, NULL, 'Edit this post.', 'Edit this post.', 0),
(262, 1, NULL, 'Items count', 'Items count', 0),
(263, 1, NULL, 'Header_image', 'Header_image', 0),
(264, 1, NULL, 'Workshop', 'Workshop', 0),
(265, 1, NULL, 'Add new', 'Add new', 0),
(266, 1, NULL, 'Event', 'Event', 0),
(267, 1, NULL, 'Class', 'Class', 0),
(268, 1, NULL, 'Edit package events', 'Edit package events', 0),
(269, 1, NULL, '                Edit events.<br/>                In "Event" field write event name. Example: dispatch:beforeDispatchLoop<br/>                In "Class" field write plugin class with namespace.                Example: Plugin\\SomePlugin\\Some                ', '                Edit events.<br/>                In "Event" field write event name. Example: dispatch:beforeDispatchLoop<br/>                In "Class" field write plugin class with namespace.                Example: Plugin\\SomePlugin\\Some                ', 0),
(270, 1, NULL, 'Team', 'Team', 0),
(271, 1, NULL, 'Packages management - Plugins', 'Packages management - Plugins', 0),
(272, 1, NULL, 'Packages management - Themes', 'Packages management - Themes', 0),
(273, 1, 'core', 'Export with translations', 'Export with translations', 0),
(274, 1, 'core', 'Work Log', 'Work Log', 0),
(275, 1, 'user', 'Incorrect email or password!', 'Incorrect email or password!', 0),
(276, 1, 'user', 'Field password must be at least 6 characters long', 'Field password must be at least 6 characters long', 0),
(277, 1, 'user', 'Field repeatPassword must be at least 6 characters long', 'Field repeatPassword must be at least 6 characters long', 0),
(278, 1, 'worklog', 'Project name', 'Project name', 0),
(279, 1, 'worklog', 'Projects', 'Projects', 0),
(280, 1, 'worklog', 'Worklog', 'Worklog', 0),
(281, 1, 'worklog', 'Create new project', 'Create new project', 0),
(282, 1, 'worklog', 'Project Creation', 'Project Creation', 0),
(285, 1, 'worklog', 'Add Member', 'Add Member', 0),
(286, 1, 'worklog', 'Add member to project', 'Add member to project', 0),
(287, 1, 'worklog', 'Add member to project Test project', 'Add member to project Test project', 0),
(288, 1, 'worklog', 'Select User', 'Select User', 0),
(289, 1, 'worklog', 'Create new project.', 'Create new project.', 0),
(290, 1, 'worklog', 'Daily Worklog', 'Daily Worklog', 0),
(291, 1, 'worklog', 'Log Work', 'Log Work', 0),
(292, 1, 'worklog', 'Select Project', 'Select Project', 0),
(293, 1, 'worklog', 'Test project', 'Test project', 0),
(294, 1, 'worklog', 'Select issur', 'Select issur', 0),
(295, 1, 'worklog', 'Time (Format: 1d 1h 1m)', 'Time (Format: 1d 1h 1m)', 0),
(296, 1, 'worklog', 'Date', 'Date', 0),
(297, 1, 'worklog', 'Comment', 'Comment', 0),
(298, 1, 'worklog', 'Submit', 'Submit', 0),
(299, 1, 'worklog', 'Select issue (Tam thoi de cho vui thoi ngen ^^)', 'Select issue (Tam thoi de cho vui thoi ngen ^^)', 0),
(304, 1, 'worklog', 'Type', 'Type', 0),
(305, 1, 'worklog', 'Office', 'Office', 0),
(306, 1, 'worklog', 'Remote', 'Remote', 0),
(310, 1, 'worklog', 'Select issue', 'Select issue', 0),
(311, 1, 'worklog', 'Minute (Format: 60m)', 'Minute (Format: 60m)', 0),
(313, 1, 'worklog', 'Looged Date', 'Looged Date', 0),
(314, 1, 'worklog', 'Logged Date', 'Logged Date', 0),
(315, 1, 'worklog', 'Minute', 'Minute', 0),
(316, 1, 'worklog', 'Message', 'Message', 0),
(317, 1, 'worklog', 'Logged', 'Logged', 0),
(318, 1, 'user', '<button data-provider=''facebook'' type=''button''>Login with Facebook</button>', '<button data-provider=''facebook'' type=''button''>Login with Facebook</button>', 0),
(319, 1, 'core', 'Page will be available under http://geekup.dev:8888/page/[URL NAME]', 'Page will be available under http://geekup.dev:8888/page/[URL NAME]', 0),
(320, 1, 'user', 'Field ''%s'' is required!', 'Field ''%s'' is required!', 0),
(321, 1, 'user', 'Field ''Login'' is required!', 'Field ''Login'' is required!', 0),
(322, 1, 'user', 'Field ''%s'' can not be empty!', 'Field ''%s'' can not be empty!', 0),
(323, 1, 'user', 'Field ''Login'' can not be empty!', 'Field ''Login'' can not be empty!', 0),
(324, 1, 'user', 'Login field', 'Login field', 0),
(325, 1, 'workshop', 'Workshop title', 'Workshop title', 0),
(326, 1, 'workshop', 'start date', 'start date', 0),
(327, 1, 'workshop', 'end date', 'end date', 0),
(328, 1, 'workshop', 'Workshops', 'Workshops', 0),
(329, 1, 'workshop', 'Create new workshop', 'Create new workshop', 0),
(330, 1, 'core', 'Edit Access', 'Edit Access', 0),
(331, 1, 'core', 'Editing access rights of "%currentObject%", for:', 'Editing access rights of "%currentObject%", for:', 0),
(332, 1, 'core', 'Access', 'Access', 0),
(333, 1, 'core', 'ACTION_ADMINAREA_ACCESS_DESCRIPTION', 'ACTION_ADMINAREA_ACCESS_DESCRIPTION', 0),
(334, 1, 'worklog', 'Logged Hours', 'Logged Hours', 0),
(335, 1, 'workshop', 'Workshop Creation', 'Workshop Creation', 0),
(336, 1, 'workshop', 'Create new workshop.', 'Create new workshop.', 0),
(337, 1, 'workshop', 'Workshop name', 'Workshop name', 0),
(338, 1, 'workshop', 'Project', 'Project', 0),
(339, 1, 'user', ' Forgot Password', ' Forgot Password', 0),
(340, 1, 'user', 'Field ''Email'' is required!', 'Field ''Email'' is required!', 0),
(341, 1, 'user', 'Field ''Email'' can not be empty!', 'Field ''Email'' can not be empty!', 0),
(342, 1, 'user', 'Forgot password', 'Forgot password', 0),
(343, 1, 'user', 'Enter your email to reset your password', 'Enter your email to reset your password', 0),
(344, 1, 'user', 'Send', 'Send', 0),
(345, 1, 'user', 'Field email must be an email address', 'Field email must be an email address', 0),
(346, 1, 'user', 'Could not found user with this email: ngtrieuvi92@gmail.com', 'Could not found user with this email: ngtrieuvi92@gmail.com', 0),
(347, 1, 'user', 'Could not found any user match with  email: ngtrieuvi92@gmail.com', 'Could not found any user match with  email: ngtrieuvi92@gmail.com', 0),
(348, 1, 'user', 'Opps,we have some error, please! try again', 'Opps,we have some error, please! try again', 0),
(349, 1, 'user', 'reset is required', 'reset is required', 0),
(350, 1, 'user', 'Your email has been sent ', 'Your email has been sent ', 0),
(351, 1, 'user', 'Reset password', 'Reset password', 0),
(352, 1, 'user', 'Enter your new password', 'Enter your new password', 0),
(353, 1, 'user', 'New Password', 'New Password', 0),
(354, 1, 'user', 'Confirm Password', 'Confirm Password', 0),
(355, 1, 'user', 'Could not found any user match with  email: daa@gamil.com', 'Could not found any user match with  email: daa@gamil.com', 0),
(356, 1, 'user', 'Forgot your password?', 'Forgot your password?', 0),
(357, 1, 'user', 'Enter your email address to reset your password. You may need to check your spam folder or unblock no-reply@geekup.vn.', 'Enter your email address to reset your password. You may need to check your spam folder or unblock no-reply@geekup.vn.', 0),
(358, 1, 'user', 'Could not found any user match with  email: test@gmail.com', 'Could not found any user match with  email: test@gmail.com', 0),
(359, 1, 'user', 'Please enter a new password for your  account.', 'Please enter a new password for your  account.', 0),
(360, 1, 'user', 'An e-mail has been sent to ngtrieuvi92@gmail.com with further instructions. ', 'An e-mail has been sent to ngtrieuvi92@gmail.com with further instructions. ', 0),
(361, 1, 'user', 'The passwords don''t match, please try again.', 'The passwords don''t match, please try again.', 0),
(362, 1, 'user', 'No account with that e-mail address exists.', 'No account with that e-mail address exists.', 0),
(363, 1, 'blog', 'Manage Comment', 'Manage Comment', 0),
(364, 1, 'blog', 'Comments', 'Comments', 0),
(365, 1, 'blog', 'New Posts', 'New Posts', 0),
(366, 1, 'blog', 'Status', 'Status', 0),
(367, 1, 'blog', 'Unpublish', 'Unpublish', 0),
(368, 1, 'blog', 'Publish', 'Publish', 0),
(369, 1, 'blog', 'Leave a Comment', 'Leave a Comment', 0),
(370, 1, 'blog', 'Post a Comment', 'Post a Comment', 0),
(371, 1, 'worklog', 'Manage Members', 'Manage Members', 0),
(372, 1, 'worklog', 'Manage members of project [Test project]', 'Manage members of project [Test project]', 0),
(373, 1, 'blog', 'Post_id', 'Post_id', 0),
(374, 1, 'blog', 'List Posts', 'List Posts', 0),
(375, 1, 'blog', 'Some legend, if needed', 'Some legend, if needed', 0),
(376, 1, 'blog', 'Somddsdsed', 'Somddsdsed', 0),
(377, 1, 'user', 'Sign up for newsletter', 'Sign up for newsletter', 0),
(378, 1, 'user', 'Your personal details', 'Your personal details', 0),
(379, 1, 'user', 'User name', 'User name', 0),
(380, 1, 'user', 'First name', 'First name', 0),
(381, 1, 'user', 'Last name', 'Last name', 0),
(382, 1, 'user', 'Your password', 'Your password', 0),
(383, 1, 'user', 'Repeat Password', 'Repeat Password', 0),
(384, 1, 'core', 'LogWork', 'LogWork', 0),
(386, 1, 'geekup', 'Forgot password?', 'Forgot password?', 0),
(387, 1, 'user', 'Field ''User name'' is required!', 'Field ''User name'' is required!', 0),
(388, 1, 'user', 'Field ''User name'' can not be empty!', 'Field ''User name'' can not be empty!', 0),
(389, 1, 'user', 'Field ''Username'' is required!', 'Field ''Username'' is required!', 0),
(390, 1, 'user', 'Field ''Username'' can not be empty!', 'Field ''Username'' can not be empty!', 0),
(391, 1, 'user', 'subscribe_newsletter is required', 'subscribe_newsletter is required', 0),
(392, 1, 'geekup', 'Email address', 'Email address', 0),
(393, 1, 'core', 'Show_views', 'Show_views', 0),
(394, 1, 'core', 'ACTION__CORE_MODEL_PAGE_SHOW_VIEWS_DESCRIPTION', 'ACTION__CORE_MODEL_PAGE_SHOW_VIEWS_DESCRIPTION', 0),
(395, 1, 'core', 'Page_footer', 'Page_footer', 0),
(396, 1, 'core', 'OPTION__CORE_MODEL_PAGE_PAGE_FOOTER_DESCRIPTION', 'OPTION__CORE_MODEL_PAGE_PAGE_FOOTER_DESCRIPTION', 0),
(397, 1, 'user', 'Is default?', 'Is default?', 0),
(398, 1, 'user', 'Are you really want to delete this role?', 'Are you really want to delete this role?', 0),
(399, 1, 'user', 'Role Creation', 'Role Creation', 0),
(400, 1, 'user', 'Create new role.', 'Create new role.', 0),
(401, 1, 'user', 'Is Default', 'Is Default', 0),
(402, 1, 'user', 'Content Manager', 'Content Manager', 0),
(403, 1, 'user', 'Member', 'Member', 0),
(404, 1, 'core', 'ACTION__BLOG_MODEL_POST_CREATE_DESCRIPTION', 'ACTION__BLOG_MODEL_POST_CREATE_DESCRIPTION', 0),
(405, 1, 'core', 'ACTION__BLOG_MODEL_POST_EDIT_DESCRIPTION', 'ACTION__BLOG_MODEL_POST_EDIT_DESCRIPTION', 0),
(406, 1, 'core', 'ACTION__BLOG_MODEL_POST_DELETE_DESCRIPTION', 'ACTION__BLOG_MODEL_POST_DELETE_DESCRIPTION', 0),
(407, 1, 'core', 'View', 'View', 0),
(408, 1, 'core', 'ACTION__BLOG_MODEL_POST_VIEW_DESCRIPTION', 'ACTION__BLOG_MODEL_POST_VIEW_DESCRIPTION', 0),
(409, 1, 'core', 'Index', 'Index', 0),
(410, 1, 'core', 'ACTION__BLOG_MODEL_POST_INDEX_DESCRIPTION', 'ACTION__BLOG_MODEL_POST_INDEX_DESCRIPTION', 0),
(411, 1, 'core', 'Blog_footer', 'Blog_footer', 0),
(412, 1, 'core', 'OPTION__BLOG_MODEL_POST_BLOG_FOOTER_DESCRIPTION', 'OPTION__BLOG_MODEL_POST_BLOG_FOOTER_DESCRIPTION', 0),
(413, 1, 'core', 'Blog_count', 'Blog_count', 0),
(414, 1, 'core', 'OPTION__BLOG_MODEL_POST_BLOG_COUNT_DESCRIPTION', 'OPTION__BLOG_MODEL_POST_BLOG_COUNT_DESCRIPTION', 0),
(415, 1, 'core', 'Utopia', 'Utopia', 0),
(416, 1, 'core', 'Master Universities', 'Master Universities', 0),
(417, 1, 'core', 'People', 'People', 0),
(418, 1, 'core', 'Master Degrees', 'Master Degrees', 0),
(419, 1, 'core', 'Master Companies', 'Master Companies', 0),
(420, 1, 'core', 'List Post', 'List Post', 0),
(421, 1, 'core', 'Write A New Posts', 'Write A New Posts', 0),
(422, 1, 'user', 'User Editing', 'User Editing', 0),
(423, 1, 'user', 'Edit User', 'Edit User', 0),
(424, 1, 'user', 'Edit this user.', 'Edit this user.', 0),
(425, 1, 'user', 'Select user role', 'Select user role', 0),
(426, 1, 'user', 'Role Editing', 'Role Editing', 0),
(427, 1, 'user', 'Edit Role', 'Edit Role', 0),
(428, 1, 'user', 'Edit this role.', 'Edit this role.', 0),
(429, 1, 'core', 'Content_Manager', 'Content_Manager', 0),
(430, 1, 'user', 'Profile', 'Profile', 0),
(431, 1, 'base', 'Phone Number', 'Phone Number', 0),
(432, 1, 'base', 'Interests', 'Interests', 0),
(433, 1, 'base', 'Occupation', 'Occupation', 0),
(434, 1, 'base', 'About me', 'About me', 0),
(435, 1, 'base', 'Web Url', 'Web Url', 0),
(436, 1, 'base', 'Avatar', 'Avatar', 0),
(437, 1, 'blog', 'Category', 'Category', 0),
(438, 1, 'blog', 'Create new Category', 'Create new Category', 0),
(439, 1, 'blog', 'Tag', 'Tag', 0),
(440, 1, 'blog', 'Create new Tag', 'Create new Tag', 0),
(441, 1, 'blog', 'Short description', 'Short description', 0),
(442, 1, 'blog', 'Thumbnail', 'Thumbnail', 0),
(444, 1, 'blog', 'Post name', 'Post name', 0),
(445, 1, 'blog', 'Category Creation', 'Category Creation', 0),
(446, 1, 'blog', 'Create new Category.', 'Create new Category.', 0),
(447, 1, 'blog', 'iOS', 'iOS', 0),
(449, 1, 'core', 'Internal Server Error', 'Internal Server Error', 0),
(450, 1, 'blog', 'Sencha', 'Sencha', 0),
(451, 1, 'user', 'Field password is required', 'Field password is required', 0),
(455, 1, 'base', 'Minute (Format: 4h 60m | 4h 60 |4h60 ...)', 'Minute (Format: 4h 60m | 4h 60 |4h60 ...)', 0),
(456, 1, 'base', 'Minute (Format: 4h 60m | 4h 60 |4h60)', 'Minute (Format: 4h 60m | 4h 60 |4h60)', 0),
(457, 1, 'user', 'An e-mail has been sent to ngmng204@gmail.com with further instructions. ', 'An e-mail has been sent to ngmng204@gmail.com with further instructions. ', 0),
(458, 1, 'core', 'GEEKUp Blog', 'GEEKUp Blog', 0),
(459, 1, 'blog', 'Tag Creation', 'Tag Creation', 0),
(460, 1, 'blog', 'Create new Tag.', 'Create new Tag.', 0),
(461, 1, 'workshop', 'List Workshops', 'List Workshops', 0),
(462, 1, 'workshop', 'List Projects', 'List Projects', 0),
(463, 1, 'workshop', 'name is required', 'name is required', 0),
(464, 1, 'workshop', 'tags is required', 'tags is required', 0),
(465, 1, 'workshop', 'description is required', 'description is required', 0),
(466, 1, 'workshop', 'Edit Workshop', 'Edit Workshop', 0),
(467, 1, 'workshop', 'Edit this workshop.', 'Edit this workshop.', 0),
(474, 1, 'base', 'minute is required', 'minute is required', 0),
(476, 1, 'workshop', 'ngtrieuvi92', 'ngtrieuvi92', 0),
(477, 1, 'workshop', 'Geekup', 'Geekup', 0),
(478, 1, 'workshop', 'Ngan', 'Ngan', 0),
(479, 1, 'workshop', 'sunny', 'sunny', 0),
(480, 1, 'workshop', 'Nguyá»…n', 'Nguyá»…n', 0),
(481, 1, 'workshop', 'rainy', 'rainy', 0),
(482, 1, 'workshop', 'kid1612', 'kid1612', 0),
(483, 1, 'base', 'Newsletter', 'Newsletter', 0),
(484, 1, 'blog', 'Tag name', 'Tag name', 0),
(485, 1, 'base', 'Ping2Buzz', 'Ping2Buzz', 0),
(486, 1, 'base', 'vnVitamin', 'vnVitamin', 0),
(487, 1, 'base', 'WifiSharing', 'WifiSharing', 0),
(489, 1, 'blog', 'Phonegap', 'Phonegap', 0),
(491, 1, 'user', 'Value of field: ''username'' is already present in another record', 'Value of field: ''username'' is already present in another record', 0),
(492, 1, 'user', 'Value of field: ''email'' is already present in another record', 'Value of field: ''email'' is already present in another record', 0),
(493, 1, 'blog', 'Hardware', 'Hardware', 0),
(494, 1, 'blog', 'Ubuntu', 'Ubuntu', 0),
(495, 1, 'blog', 'Linux', 'Linux', 0),
(496, 1, 'blog', 'Rasberry pi', 'Rasberry pi', 0),
(497, 1, 'blog', 'Android', 'Android', 0),
(498, 1, 'blog', 'Phalcon', 'Phalcon', 0);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`) VALUES
(1, 'Default menu'),
(2, 'Our team');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `onclick` varchar(255) DEFAULT NULL,
  `target` varchar(10) DEFAULT NULL,
  `tooltip` varchar(255) DEFAULT NULL,
  `tooltip_position` varchar(10) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `icon_position` varchar(10) NOT NULL,
  `languages` varchar(150) DEFAULT NULL,
  `roles` varchar(150) DEFAULT NULL,
  `is_enabled` tinyint(1) DEFAULT NULL,
  `item_order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-menu_items-menus-menu_id-id` (`menu_id`),
  KEY `fki-menu_items-menu_items-parent_id-id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `title`, `menu_id`, `parent_id`, `page_id`, `url`, `onclick`, `target`, `tooltip`, `tooltip_position`, `icon`, `icon_position`, `languages`, `roles`, `is_enabled`, `item_order`) VALUES
(3, 'Khanh', 2, NULL, NULL, '/page/khanh', NULL, '_top', NULL, 'top', NULL, 'left', NULL, '["3"]', 1, 0),
(4, 'About Us', 1, NULL, 4, NULL, NULL, NULL, NULL, 'top', NULL, 'left', NULL, '["3"]', 0, 3),
(6, 'LogWork', 1, NULL, NULL, 'worklog', NULL, NULL, NULL, 'top', NULL, 'left', NULL, '["1","4","5"]', 1, 2),
(7, 'Blog', 1, NULL, NULL, 'blog', NULL, NULL, NULL, 'top', NULL, 'left', NULL, NULL, 1, 1),
(8, 'Home', 1, NULL, NULL, 'workshop', NULL, NULL, NULL, 'top', NULL, 'left', NULL, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `type` varchar(64) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text,
  `version` varchar(32) NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `is_system` tinyint(1) NOT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `type`, `title`, `description`, `version`, `author`, `website`, `enabled`, `is_system`, `data`) VALUES
(1, 'core', 'module', 'Core', 'PhalconEye Core', '0.4.0', 'PhalconEye Team', 'http://phalconeye.com/', 1, 1, '{"widgets":[{"name":"Footer","module":"core","description":"Footer Page","is_paginated":"1","is_acl_controlled":0,"admin_form":"action","enabled":true}]}'),
(2, 'user', 'module', 'Users', 'PhalconEye Users', '0.4.0', 'PhalconEye Team', 'http://phalconeye.com/', 1, 1, NULL),
(5, 'blog', 'module', 'Blog', NULL, '0.1', NULL, NULL, 1, 0, '{"events":[],"widgets":[{"name":"Blog","module":"blog","description":"Blog widget.","is_paginated":"1","is_acl_controlled":0,"admin_form":"action","enabled":true},{"name":"Sidebar","module":"blog","description":"Sidebar widget.","is_paginated":"1","is_acl_controlled":0,"admin_form":"action","enabled":true}]}'),
(6, 'blog', 'widget', 'Blog', NULL, '0.1', NULL, NULL, 1, 0, '{"module":"blog","widget_id":"4"}'),
(7, 'workshop', 'module', 'Workshop', '- Portfolio\r\n- Project info\r\n', '0.1', NULL, NULL, 1, 0, '{"events":[],"widgets":[]}'),
(8, 'team', 'module', 'Team', '- Our team\r\n- Bio\r\n- Profile\r\n- Contact', '0.1', NULL, NULL, 1, 0, '{"events":[],"widgets":[]}'),
(9, 'worklog', 'module', 'Work Log', 'A small work log', '0.1', 'Van Nguyen', NULL, 1, 0, '{"events":[],"widgets":[]}'),
(10, 'footer', 'widget', 'Footer', 'Footer Page', '0.1', 'Vi Nguyen', NULL, 1, 0, '{"module":"core","widget_id":"5"}'),
(11, 'sidebar', 'widget', 'BlogSideBar', NULL, '0.1', 'Vi Nguyen', NULL, 1, 0, '{"module":"blog","widget_id":"6"}');

-- --------------------------------------------------------

--
-- Table structure for table `package_dependencies`
--

CREATE TABLE IF NOT EXISTS `package_dependencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `dependency_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-package_dependencies-packages-package_id-id` (`package_id`),
  KEY `fki-package_dependencies-packages-dependency_id-id` (`dependency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `package_dependencies`
--

INSERT INTO `package_dependencies` (`id`, `package_id`, `dependency_id`) VALUES
(1, 6, 5),
(2, 10, 1),
(3, 11, 5);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `type` varchar(25) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `layout` varchar(50) NOT NULL,
  `controller` varchar(50) DEFAULT NULL,
  `roles` varchar(150) DEFAULT NULL,
  `view_count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `type`, `url`, `description`, `keywords`, `layout`, `controller`, `roles`, `view_count`) VALUES
(1, 'Header', 'header', NULL, 'Header content', '', 'middle', NULL, NULL, 0),
(2, 'Footer', 'footer', NULL, 'Footer content', '', 'middle', NULL, NULL, 0),
(3, 'Home', 'home', '/', 'GEEKUp Blog', 'BLog', 'middle', NULL, NULL, 0),
(4, 'About Us', NULL, 'aboutus', NULL, NULL, 'right_middle_left_bottom', NULL, NULL, 0),
(5, 'Blog', NULL, 'blog', NULL, NULL, 'middle', 'Blog\\Controller\\IndexController->indexAction', NULL, 0),
(6, 'BlogSideBar', 'blogsideBar', NULL, NULL, NULL, 'middle', NULL, '["1","2","3","4","5"]', 0);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `content` text NOT NULL,
  `short_description` text,
  `tags` varchar(255) NOT NULL,
  `status` int(4) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `header_image` varchar(255) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `cat_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-post-users-author_id-id` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `title`, `content`, `short_description`, `tags`, `status`, `author_id`, `header_image`, `creation_date`, `modified_date`, `slug`, `cat_id`) VALUES
(11, 'Review: Ubuntu and an Intel NUC', '<p>Â </p>\r\n\r\n<p><span style="font-family:ubuntu,arial,sans-serif">Last week, IÂ </span><a href="https://plus.google.com/u/0/+DustinKirkland/posts/gxX2na4AiA6" style="color: rgb(85, 136, 170); text-decoration: none; font-family: Ubuntu, Arial, sans-serif;">posed a question on Google+</a><span style="font-family:ubuntu,arial,sans-serif">, looking for suggestions on a minimal physical format,</span><a href="https://en.wikipedia.org/wiki/X86" style="color: rgb(85, 136, 170); text-decoration: none; font-family: Ubuntu, Arial, sans-serif;">x86</a><span style="font-family:ubuntu,arial,sans-serif">Â machine. Â I was looking for something like aÂ </span><a href="http://raspberrypi.org/" style="color: rgb(85, 136, 170); text-decoration: none; font-family: Ubuntu, Arial, sans-serif;">Raspberry Pi</a><span style="font-family:ubuntu,arial,sans-serif">Â (of which I already have one), but really it had to be x86.</span><br />\r\n<br />\r\n<span style="font-family:ubuntu,arial,sans-serif">I was aware of a few options out there, but I was very fortunately introduced to one spectacular little box...theÂ </span><a href="https://en.wikipedia.org/wiki/Next_Unit_of_Computing" style="color: rgb(85, 136, 170); text-decoration: none; font-family: Ubuntu, Arial, sans-serif;">Intel NUC</a><span style="font-family:ubuntu,arial,sans-serif">!</span><br />\r\n<br />\r\n<span style="font-family:ubuntu,arial,sans-serif">The unboxing experience is nothing short of pure marketing genius!</span></p>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif; text-align: center;">Â </div>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif; text-align: center;"><iframe frameborder="0" height="225" src="http://www.youtube.com/embed/iiVNjV-h8Uc" width="400"></iframe></div>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif; text-align: center;">Â </div>\r\n\r\n<p><span style="font-family:ubuntu,arial,sans-serif">The "NUC" stands forÂ </span><a href="http://www.intel.com/content/www/us/en/motherboards/desktop-motherboards/nuc.html" style="color: rgb(85, 136, 170); text-decoration: none; font-family: Ubuntu, Arial, sans-serif;">Intel&#39;s Next Unit of Computing</a><span style="font-family:ubuntu,arial,sans-serif">. Â It&#39;s a compact little device, that ships barebones. Â YouÂ </span><span style="font-family:ubuntu,arial,sans-serif">need to add DDR3 memory (up to 16GB), an mSATA hard drive (if you want to boot locally), and an mSATA WiFi card (if you want wireless networking).</span><br />\r\n<br />\r\n<span style="font-family:ubuntu,arial,sans-serif">The physical form factor of all models is identical:</span><br />\r\nÂ </p>\r\n\r\n<ul>\r\n	<li>4.6" x 4.4" x 1.6"</li>\r\n	<li>11.7cm x 11.2cm x 4.1cm</li>\r\n</ul>\r\n\r\n<p><br />\r\n<span style="font-family:ubuntu,arial,sans-serif">There are 3 different processor options:</span><br />\r\nÂ </p>\r\n\r\n<ul>\r\n	<li><a href="http://ark.intel.com/products/56056/" style="color: rgb(85, 136, 170); text-decoration: none;">Celeron 847</a></li>\r\n	<li><a href="http://ark.intel.com/products/65697/" style="color: rgb(85, 136, 170); text-decoration: none;">i3 3217U</a></li>\r\n	<li><a href="http://ark.intel.com/products/64903/" style="color: rgb(85, 136, 170); text-decoration: none;">i5 3427U</a></li>\r\n</ul>\r\n\r\n<p><br />\r\n<span style="font-family:ubuntu,arial,sans-serif">And there are three different peripheral setups:</span><br />\r\nÂ </p>\r\n\r\n<ul>\r\n	<li>HDMI 1.4a (x2) + USB 2.0 (x3)Â + Gigabit ethernet</li>\r\n	<li>HDMI 1.4a (x1) +Â Thunderbolt supporting DisplayPort 1.1a (x1) + USB 2.0 (x3)</li>\r\n	<li>HDMI 1.4a (x1) +Â Mini DisplayPort 1.1a (x2) + USB 2.0 (x2); USB 3.0 (x1)</li>\r\n</ul>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif;">I ended up buying 3 of these last week, and reworked my audio/video and baby monitoring setup in the house last week. Â I bought 2 ofÂ <a href="http://www.amazon.com/gp/product/B0093LINVK/" style="color: rgb(85, 136, 170); text-decoration: none;">these</a>Â (i3Â + Ethernet)Â , and 1 ofÂ <a href="http://www.amazon.com/gp/product/B0093LINT2/" style="color: rgb(85, 136, 170); text-decoration: none;">these</a>Â (i3Â + Thunderbolt)</div>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif;">Â </div>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif;"><em>Quite simply, I couldn&#39;t be happier with these little devices!</em></div>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif;">Â </div>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif;">I used one of these to replace the dedicated audio/video PC (an x201 Thinkpad) hooked up in my theater. Â The x201 was a beefy machine, with plenty of CPU and video capability. Â But it was pretty bulky, rather noisy, and drew too much power.</div>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif;">Â </div>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif;">And the other two are Baby-buntu baby monitors, asÂ <a href="http://blog.dustinkirkland.com/2012/03/baby-buntu.html" style="color: rgb(85, 136, 170); text-decoration: none;">previously blogged here</a>, replacing a real piece-of-crap Lenovo Q100 (Atom +Â SiS307DV and all the horror maligned with that sick chip set).</div>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif;">Â </div>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif;">All 3 are now running Ubuntu 13.10, spectacularly I might add! Â All of the hardware cooperated perfectly.</div>\r\n\r\n<p>Â </p>\r\n\r\n<div class="separator" style="font-family: Ubuntu, Arial, sans-serif; clear: both; text-align: center;"><a href="http://3.bp.blogspot.com/-dL2Vy5wA6lc/UoaGUNGCYGI/AAAAAAAAhqY/ezHpvCoav5w/s1600/IMG_20131106_130118.jpg" style="color: rgb(85, 136, 170); text-decoration: none; margin-left: 1em; margin-right: 1em;"><img src="http://3.bp.blogspot.com/-dL2Vy5wA6lc/UoaGUNGCYGI/AAAAAAAAhqY/ezHpvCoav5w/s320/IMG_20131106_130118.jpg" style="border-bottom-left-radius:10px; border-bottom-right-radius:10px; border-top-left-radius:10px; border-top-right-radius:10px; border:1px solid rgb(204, 204, 204); box-shadow:rgb(136, 136, 136) 10px 10px 5px; height:217px; padding:4px; width:320px" /></a></div>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif;">Â </div>\r\n\r\n<p>Â </p>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif;">Here are the two views that I really wanted Amazon to show me, as I was buying the device...what the inside looks like! Â You can see two mSATA ports and red/black WiFi antenna leads on the left, and two DDR3 slots on the right.</div>\r\n\r\n<div style="font-family: Ubuntu, Arial, sans-serif;">Â </div>\r\n\r\n<div class="separator" style="font-family: Ubuntu, Arial, sans-serif; clear: both; text-align: center;"><a href="http://4.bp.blogspot.com/-CsTchUKP9sM/UoaGTGv05uI/AAAAAAAAhqU/E1TPAq4M4KY/s1600/IMG_20131106_123544.jpg" style="color: rgb(85, 136, 170); text-decoration: none; margin-left: 1em; margin-right: 1em;"><img src="http://4.bp.blogspot.com/-CsTchUKP9sM/UoaGTGv05uI/AAAAAAAAhqU/E1TPAq4M4KY/s320/IMG_20131106_123544.jpg" style="border-bottom-left-radius:10px; border-bottom-right-radius:10px; border-top-left-radius:10px; border-top-right-radius:10px; border:1px solid rgb(204, 204, 204); box-shadow:rgb(136, 136, 136) 10px 10px 5px; height:285px; padding:4px; width:320px" /></a></div>\r\n\r\n<p><br />\r\n<span style="font-family:ubuntu,arial,sans-serif">On the left, you can now see a 24GB mSATA SSD, and beneath it (not visible) is an Intel Centrino Advanced-N 6235 WiFi adapter. Â On the right, I have two 8GB DDR3 memory modules.</span><br />\r\n<br />\r\n<span style="font-family:ubuntu,arial,sans-serif">Note, to get wireless working properly I did have to:</span><br />\r\nÂ </p>\r\n\r\n<pre>\r\necho "options iwlwifi 11n_disable=1" | sudo tee -a /etc/modprobe.d/iwlwifi.conf</pre>\r\n\r\n<p>Â </p>\r\n\r\n<div class="separator" style="font-family: Ubuntu, Arial, sans-serif; clear: both; text-align: center;"><a href="http://2.bp.blogspot.com/-0yv-oJqqgVw/UoaGQCFpxLI/AAAAAAAAhqE/HQupuPGJQ-k/s1600/IMG_20131106_123925.jpg" style="color: rgb(85, 136, 170); text-decoration: none; margin-left: 1em; margin-right: 1em;"><img src="http://2.bp.blogspot.com/-0yv-oJqqgVw/UoaGQCFpxLI/AAAAAAAAhqE/HQupuPGJQ-k/s320/IMG_20131106_123925.jpg" style="border-bottom-left-radius:10px; border-bottom-right-radius:10px; border-top-left-radius:10px; border-top-right-radius:10px; border:1px solid rgb(204, 204, 204); box-shadow:rgb(136, 136, 136) 10px 10px 5px; height:279px; padding:4px; width:320px" /></a></div>\r\n\r\n<p><br />\r\n<span style="font-family:ubuntu,arial,sans-serif">The BIOS is really super fancy :-) Â There&#39;s a mouse and everything. Â I made a few minor tweaks, to the boot order, assigned 512MB of memory to the display adapter, and configured it to power itself back on at any power loss.</span><br />\r\nÂ </p>\r\n\r\n<div class="separator" style="font-family: Ubuntu, Arial, sans-serif; clear: both; text-align: center;"><a href="http://3.bp.blogspot.com/-Z16roD4bItw/UoaGQLJQ8BI/AAAAAAAAhqA/KCUmIw5yUro/s1600/IMG_20131106_125402.jpg" style="color: rgb(85, 136, 170); text-decoration: none; margin-left: 1em; margin-right: 1em;"><img src="http://3.bp.blogspot.com/-Z16roD4bItw/UoaGQLJQ8BI/AAAAAAAAhqA/KCUmIw5yUro/s320/IMG_20131106_125402.jpg" style="border-bottom-left-radius:10px; border-bottom-right-radius:10px; border-top-left-radius:10px; border-top-right-radius:10px; border:1px solid rgb(204, 204, 204); box-shadow:rgb(136, 136, 136) 10px 10px 5px; height:215px; padding:4px; width:320px" /></a></div>\r\n\r\n<p><br />\r\n<span style="font-family:ubuntu,arial,sans-serif">Speaking of power, it sustains about 10 watts of power, at idle, which costs me about $11/year in electricity.</span><br />\r\nÂ </p>\r\n\r\n<div class="separator" style="font-family: Ubuntu, Arial, sans-serif; clear: both; text-align: center;"><a href="http://4.bp.blogspot.com/-zdBvXez9gOo/UoaJrkntPFI/AAAAAAAAhqs/WuYkAs0dTF4/s1600/IMG_20131106_132012.jpg" style="color: rgb(85, 136, 170); text-decoration: none; margin-left: 1em; margin-right: 1em;"><img src="http://4.bp.blogspot.com/-zdBvXez9gOo/UoaJrkntPFI/AAAAAAAAhqs/WuYkAs0dTF4/s320/IMG_20131106_132012.jpg" style="border-bottom-left-radius:10px; border-bottom-right-radius:10px; border-top-left-radius:10px; border-top-right-radius:10px; border:1px solid rgb(204, 204, 204); box-shadow:rgb(136, 136, 136) 10px 10px 5px; height:320px; padding:4px; width:299px" /></a></div>\r\n\r\n<p><br />\r\n<span style="font-family:ubuntu,arial,sans-serif">Some of you might be interested in some rough disk IO statistics...</span><br />\r\nÂ </p>\r\n\r\n<pre>\r\nkirkland@living:~âŸ« sudo hdparm -Tt /dev/sda\r\n/dev/sda:\r\n Timing cached reads:   11306 MB in  2.00 seconds = 5657.65 MB/sec\r\n Timing buffered disk reads: 1478 MB in  3.00 seconds = 492.32 MB/sec\r\n</pre>\r\n\r\n<p><br />\r\n<span style="font-family:ubuntu,arial,sans-serif">And theÂ </span><a href="http://manpg.es/lshw" style="color: rgb(85, 136, 170); text-decoration: none; font-family: Ubuntu, Arial, sans-serif;">lshw</a><span style="font-family:ubuntu,arial,sans-serif">Â output...</span><br />\r\nÂ </p>\r\n\r\n<pre>\r\n    description: Desktop Computer\r\n    product: (To be filled by O.E.M.)\r\n    width: 64 bits\r\n    capabilities: smbios-2.7 dmi-2.7 vsyscall32\r\n    configuration: boot=normal chassis=desktop family=To be filled by O.E.M. sku=To be filled by O.E.M. uuid=[redacted]\r\n  *-core\r\n       description: Motherboard\r\n       product: D33217CK\r\n       vendor: Intel Corporation\r\n       physical id: 0\r\n       version: G76541-300\r\n       serial: [redacted]\r\n     *-firmware\r\n          description: BIOS\r\n          vendor: Intel Corp.\r\n          physical id: 0\r\n          version: GKPPT10H.86A.0025.2012.1011.1534\r\n          date: 10/11/2012\r\n          size: 64KiB\r\n          capacity: 6336KiB\r\n          capabilities: pci upgrade shadowing cdboot bootselect socketedrom edd int13floppy1200 int13floppy720 int13floppy2880 int5printscreen int14serial int17printer acpi usb biosbootspecification uefi\r\n     *-cache:0\r\n             width: 32 bits\r\n             clock: 66MHz\r\n             capabilities: storage msi pm ahci_1.0 bus_master cap_list\r\n             configuration: driver=ahci latency=0\r\n             resources: irq:40 ioport:f0b0(size=8) ioport:f0a0(size=4) ioport:f090(size=8) ioport:f080(size=4) ioport:f060(size=32) memory:f6906000-f69067ff\r\n        *-serial UNCLAIMED\r\n             description: SMBus\r\n             product: 7 Series/C210 Series Chipset Family SMBus Controller\r\n             vendor: Intel Corporation\r\n             physical id: 1f.3\r\n             bus info: pci@0000:00:1f.3\r\n             version: 04\r\n             width: 64 bits\r\n             clock: 33MHz\r\n             configuration: latency=0\r\n             resources: memory:f6905000-f69050ff ioport:f040(size=32)\r\n     *-scsi\r\n          physical id: 1\r\n          logical name: scsi0\r\n          capabilities: emulated\r\n        *-disk\r\n             description: ATA Disk\r\n             product: BP4 mSATA SSD\r\n             physical id: 0.0.0\r\n             bus info: scsi@0:0.0.0\r\n             logical name: /dev/sda\r\n             version: S8FM\r\n             serial: [redacted]\r\n             size: 29GiB (32GB)\r\n             capabilities: gpt-1.00 partitioned partitioned:gpt\r\n             configuration: ansiversion=5 guid=be0ab026-45c1-4bd5-a023-1182fe75194e sectorsize=512\r\n           *-volume:0\r\n                description: Windows FAT volume\r\n                vendor: mkdosfs\r\n                physical id: 1\r\n                bus info: scsi@0:0.0.0,1\r\n                logical name: /dev/sda1\r\n                logical name: /boot/efi\r\n                version: FAT32\r\n                serial: 2252-bc3f\r\n                size: 486MiB\r\n                capacity: 486MiB\r\n                capabilities: boot fat initialized\r\n                configuration: FATs=2 filesystem=fat mount.fstype=vfat mount.options=rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro state=mounted\r\n           *-volume:1\r\n                description: EXT4 volume\r\n                vendor: Linux\r\n                physical id: 2\r\n                bus info: scsi@0:0.0.0,2\r\n                logical name: /dev/sda2\r\n                logical name: /\r\n                version: 1.0\r\n                serial: [redacted]\r\n                size: 25GiB\r\n                capabilities: journaled extended_attributes large_files huge_files dir_nlink recover extents ext4 ext2 initialized\r\n                configuration: created=2013-11-06 13:01:57 filesystem=ext4 lastmountpoint=/ modified=2013-11-12 15:38:33 mount.fstype=ext4 mount.options=rw,relatime,errors=remount-ro,data=ordered mounted=2013-11-12 15:38:33 state=mounted\r\n           *-volume:2\r\n                description: Linux swap volume\r\n                vendor: Linux\r\n                physical id: 3\r\n                bus info: scsi@0:0.0.0,3\r\n                logical name: /dev/sda3\r\n                version: 1\r\n                serial: [redacted]\r\n                size: 3994MiB\r\n                capacity: 3994MiB\r\n                capabilities: nofs swap initialized\r\n                configuration: filesystem=swap pagesize=4095\r\n</pre>\r\n\r\n<p><br />\r\n<span style="font-family:ubuntu,arial,sans-serif">It also supports:Â </span><a href="https://en.wikipedia.org/wiki/X86_virtualization#Processor" style="color: rgb(85, 136, 170); text-decoration: none; font-family: Ubuntu, Arial, sans-serif;">virtualization technology</a><span style="font-family:ubuntu,arial,sans-serif">,Â </span><a href="https://en.wikipedia.org/wiki/Sleep_mode" style="color: rgb(85, 136, 170); text-decoration: none; font-family: Ubuntu, Arial, sans-serif;">S3/S4/S5 sleep states</a><span style="font-family:ubuntu,arial,sans-serif">,Â </span><a href="https://en.wikipedia.org/wiki/Wake-on-LAN" style="color: rgb(85, 136, 170); text-decoration: none; font-family: Ubuntu, Arial, sans-serif;">Wake-on-LAN</a><span style="font-family:ubuntu,arial,sans-serif">, andÂ </span><a href="https://en.wikipedia.org/wiki/Preboot_Execution_Environment" style="color: rgb(85, 136, 170); text-decoration: none; font-family: Ubuntu, Arial, sans-serif;">PXE boot</a><span style="font-family:ubuntu,arial,sans-serif">. Â Sadly, it does not supportÂ </span><a href="https://en.wikipedia.org/wiki/Intelligent_Platform_Management_Interface" style="color: rgb(85, 136, 170); text-decoration: none; font-family: Ubuntu, Arial, sans-serif;">IPMI</a><span style="font-family:ubuntu,arial,sans-serif">Â :-(</span><br />\r\n<br />\r\n<span style="font-family:ubuntu,arial,sans-serif">Finally, it&#39;s worth noting that I bought the model with the i3 for a specific purpose... Â These three machines all have full virtualization capabilities (KVM). Â Which means these little boxes, with their dual-core hyper-threaded CPUs and 16GB of RAM are about to becomeÂ </span><a href="http://docs.openstack.org/developer/nova/" style="color: rgb(85, 136, 170); text-decoration: none; font-family: Ubuntu, Arial, sans-serif;">Nova</a><span style="font-family:ubuntu,arial,sans-serif">Â compute nodes in my localÂ </span><a href="http://openstack.org/" style="color: rgb(85, 136, 170); text-decoration: none; font-family: Ubuntu, Arial, sans-serif;">OpenStack</a><span style="font-family:ubuntu,arial,sans-serif">Â cluster ;-) Â </span><em>That</em><span style="font-family:ubuntu,arial,sans-serif">Â will be a separate blog post ;-)</span></p>\r\n', 'Last week, I posed a question on Google+, looking for suggestions on a minimal physical format, x86 machine.  I was looking for something like a Raspberry Pi (of which I already have one), but really it had to be x86.\r\n', 'Ubuntu,Hardware,Linux', 1, 1, '', '2014-08-11 07:26:52', '2014-08-11 07:27:33', 'review-ubuntu-and-an-intel-nuc', 3),
(12, 'Xu hÆ°á»›ng ng&ocirc;n ngá»¯ láº­p tr&igrave;nh nÄƒm 2014', '<p dir="ltr">Xu hÆ°á»›ng cá»§a ngÃ´n ngá»¯ láº­p trÃ¬nh trong 2014 lÃ  gÃ¬? ÄÃ³ lÃ  cÃ¢u há»i Ä‘Æ°á»£c nhiá»u developer quan tÃ¢m Ä‘áº¿n. Qua bÃ i viáº¿t nÃ y, ITviec hy vá»ng sáº½ giÃºp cÃ¡c báº¡n tÃ¬m Ä‘Æ°á»£c cÃ¢u tráº£ lá»i.</p>\r\n\r\n<p>TrÆ°á»›c khi Ä‘i tÃ¬m ngÃ´n ngá»¯ láº­p trÃ¬nh hot nháº¥t, chÃºng ta hÃ£y dÃ nh má»™t chÃºt thá»i gian Ä‘á»ƒ ngÃ³ qua cÃ¡c sá»‘ liá»‡u sau Ä‘Ã¢y.</p>\r\n\r\n<p>Â </p>\r\n\r\n<p>Äáº§u tiÃªn lÃ  biá»ƒu Ä‘á»“ cá»§aÂ <a href="http://jobstractor.com/" style="text-decoration: none; color: rgb(234, 30, 48);">Job Tractor</a>Â trong 2013:</p>\r\n', 'Helen: Today we&rsquo;re delighted to have a guest post from 17-year-old student Arne Baeyen', 'Rasberry pi,Hardware', 1, 1, '1407940777.jpg', '2014-08-11 07:29:54', '2014-08-13 21:39:37', 'xu-hng-ngocircn-ng-lp-trigravenh-nm-2014', 7),
(13, '3 l&yacute; do báº¡n kh&ocirc;ng nháº¥t thiáº¿t pháº£i trá»Ÿ th&agrave;nh má»™t Manager', '<p style="text-align:justify"><strong><em>â€œAi cÅ©ng muá»‘n trá»Ÿ thÃ nhÂ </em></strong><strong><em>má»™t</em></strong><strong><em>Â Manager, nhÆ°ng khÃ´ng pháº£i ai cÅ©ng phÃ¹ há»£p vá»›i cÃ´ng viá»‡c quáº£n lÃ½. Nhiá»u báº¡n sau khi rá»i bá» vá»‹ trÃ­ ká»¹ thuáº­t sang lÃ m quáº£n lÃ½ cáº£m tháº¥y khÃ´ng cÃ²n há»©ng thÃº hoáº·c chá»‰ Ä‘Æ¡n giáº£n lÃ  khÃ´ng tháº¥y phÃ¹ há»£p vá»›i cÃ´ng viá»‡c nÃ y. Há» láº½ ra nÃªn tiáº¿p tá»¥c lÃ m ká»¹ thuáº­t vÃ  trá»Ÿ thÃ nhÂ </em></strong><strong><em>má»™t</em></strong><strong><em>Â chuyÃªn gia</em></strong><strong><em>, má»™t Tech Expert.â€</em></strong></p>\r\n\r\n<p style="text-align:justify"><strong>Sebastien Auligny -Â </strong><strong>Tá»•ng GiÃ¡m Ä‘á»‘c</strong><strong>Â khu vá»±c ÄÃ´ng Nam Ã</strong><strong>Â cá»§a Gameloft</strong></p>\r\n\r\n<p style="text-align:justify">Click here for theÂ <a href="http://blog.itviec.com/3-reasons-why-you-may-not-want-to-be-a-manager/" style="text-decoration: none; color: rgb(234, 30, 48);" target="_blank">English version</a>Â of this post.</p>\r\n\r\n<p style="text-align:justify">NgÃ nh cÃ´ng nghiá»‡p game tháº¿ giá»›i Ä‘ang bÃ¹ng ná»• vá»›i tá»•ng doanh thu lÃªn Ä‘áº¿n 100 tá»· USD trong 2013.</p>\r\n\r\n<p style="text-align:justify">Gameloft lÃ  má»™t cÃ¡i tÃªn lá»›n trong lÄ©nh vá»±c nÃ y vá»›i hÆ¡n má»™t triá»‡u lÆ°á»£t game táº£i vá» hÃ ng ngÃ y trÃªn toÃ n tháº¿ giá»›i. Gameloft Viá»‡t Nam cÃ³ vÄƒn phÃ²ng táº¡i ThÃ nh phá»‘ Há»“ ChÃ­ Minh, ÄÃ  Náºµng vÃ  HÃ  Ná»™i vá»›i hÆ¡n 1,600 nhÃ¢n viÃªn.</p>\r\n\r\n<p style="text-align:justify">ITviec Ä‘Ã£ cÃ³ cuá»™c phá»ng váº¥n vá»›i Ã”ng Sebastien Auligny, Tá»•ng GiÃ¡m Ä‘á»‘c khu vá»±c ÄÃ´ng Nam Ã cá»§a Gameloft, vá» cÃ´ng nghiá»‡p game, cÃ´ng viá»‡c quáº£n lÃ½ vÃ  ká»¹ thuáº­t trong ngÃ nh.</p>\r\n\r\n<p style="text-align:justify">Â </p>\r\n\r\n<p style="text-align:justify"><strong>Anh Ä‘Ã£ tá»«ng nÃ³i â€œkhÃ´ng pháº£i ai cÅ©ng nÃªn trá»Ÿ thÃ nh Manager.â€ VÃ¬ saoÂ </strong><strong>láº¡iÂ </strong><strong>nhÆ° váº­y?</strong></p>\r\n\r\n<p style="text-align:justify">Ai cÅ©ng muá»‘n trá»Ÿ thÃ nh 1 â€œManagerâ€, nhÆ°ng khÃ´ng pháº£i ai cÅ©ng phÃ¹ há»£p vá»›i cÃ´ng viá»‡c quáº£n lÃ½. Nhiá»u báº¡n sau khi rá»i bá» vá»‹ trÃ­ ká»¹ thuáº­t sang lÃ m quáº£n lÃ½ cáº£m tháº¥y khÃ´ng cÃ²n há»©ng thÃº hoáº·c chá»‰ Ä‘Æ¡n giáº£n lÃ  khÃ´ng tháº¥y phÃ¹ há»£p vá»›i cÃ´ng viá»‡c nÃ y.</p>\r\n\r\n<p style="text-align:justify"><strong><em>Â </em></strong>CÃ³ ba lÃ½ do giáº£i thÃ­ch cho váº¥n Ä‘á» nÃ y:</p>\r\n\r\n<p style="text-align:justify"><em><strong>1. Â Trá»Ÿ thÃ nh Manager Ä‘á»“ng nghÄ©a vá»›i viá»‡c bá» qua ráº¥t nhiá»u vá»‹ trÃ­ cÅ©ng nhÆ° cÆ¡ há»™i tuyá»‡t vá»i khÃ¡c.</strong></em></p>\r\n\r\n<p style="text-align:justify">CÃ³ nhiá»u vá»‹ trÃ­ cao cáº¥p thÃ­ch há»£p vá»›i Â má»™t chuyÃªn gia ká»¹ thuáº­t (Tech Expert) hÆ¡n lÃ  má»™t Manager.</p>\r\n\r\n<p style="text-align:justify">Táº¡i Gameloft, cÃ³ nhiá»u vá»‹ trÃ­ dÃ nh riÃªng cho Tech Expert trong nhá»¯ng lÄ©nh vá»±c liÃªn quan Ä‘áº¿n game feature, phÃ¢n tÃ­ch game data, láº­p trÃ¬nh 3D, v.vâ€¦ CÃ¡c Manager khÃ´ng cÃ²n thá»i gian Ä‘á»ƒ Ä‘i phÃ¡t triá»ƒn cÃ¡c ká»¹ nÄƒng cáº§n thiáº¿t cho cÃ¡c cÃ´ng viá»‡c trÃªn.</p>\r\n\r\n<p style="text-align:justify"><em><strong>2.</strong>Â Â <strong>Nhiá»u báº¡n lÃ m ká»¹ thuáº­tÂ </strong><strong>há»©ng thÃº vá»›iÂ </strong><strong>chuyÃªn mÃ´nÂ </strong><strong>hÆ¡n lÃ  quáº£n lÃ½ con ngÆ°á»i.</strong></em></p>\r\n\r\n<p style="text-align:justify">Má»™t Manager cáº§n dÃ nh sá»± táº­p trung nhiá»u hÆ¡n vÃ o con ngÆ°á»i thay vÃ¬ ká»¹ thuáº­t. Thá»­ thÃ¡ch vÃ  cÃ´ng viá»‡c hÃ ng ngÃ y lÃ  ráº¥t khÃ¡c nhau.</p>\r\n\r\n<p style="text-align:justify">Cháº³ng thÃº vá»‹ gÃ¬ khi pháº£i Ä‘Æ°a nháº­n xÃ©t khÃ´ng tá»‘t vá» hiá»‡u quáº£ cÃ´ng viá»‡c cá»§a má»™t cÃ¡ nhÃ¢n nÃ o Ä‘Ã³ hoáº·c lÃ  quáº£n lÃ½ xung Ä‘á»™t giá»¯a cÃ¡c thÃ nh viÃªn trong nhÃ³m cá»§a mÃ¬nh.Ráº¥t nhiá»u báº¡n lÃ m ká»¹ thuáº­t cháº³ng thÃ­ch thÃº gÃ¬ cÃ´ng viá»‡c nÃ y.</p>\r\n\r\n<p style="text-align:justify"><em><strong>3.</strong>Â <strong>Manager pháº£i bá» ráº¥t nhiá»u thá»i gian vÃ o cÃ¡c váº¥n Ä‘á» nhÆ° chi tiáº¿t thÃ´ng tin khÃ¡ch hÃ ng, ngÃ¢n sÃ¡ch, bÃ¡o cÃ¡o vÃ  há»™i há»p.</strong></em></p>\r\n\r\n<p style="text-align:justify">Vá»›i nhiá»u ngÆ°á»i, cÃ´ng viá»‡c kinh doanh khÃ´ng há» thÃº vá»‹ báº±ng viá»‡c cÃ¹ng vá»›i cÃ¡c Ä‘á»“ng nghiá»‡p khÃ¡c láº­p trÃ¬nh game hay ná»— lá»±c Ä‘áº©y lÃ¹i má»i giá»›i háº¡n ká»¹ thuáº­t.</p>\r\n\r\n<p style="text-align:justify">XÃ©t cho cÃ¹ng, Ä‘iá»u quan trá»ng lÃ  báº¡n yÃªu thÃ­ch cÃ´ng viá»‡c gÃ¬ vÃ  báº¡n cÃ³ kháº£ nÄƒng á»Ÿ lÄ©nh vá»±c nÃ o.</p>\r\n\r\n<p style="text-align:justify">Äá»«ng nghÄ© ráº±ng trá»Ÿ thÃ nh Manager lÃ  con Ä‘Æ°á»ng duy nháº¥t Ä‘á»ƒ phÃ¡t triá»ƒn sá»± nghiá»‡p. Báº¡n hoÃ n toÃ n cÃ³ thá»ƒ thÃ nh cÃ´ng vá»›i lá»±a chá»n trá»Ÿ thÃ nh Tech Expert trong 1 lÄ©nh vá»±c chuyÃªn mÃ´n nÃ o Ä‘Ã³.</p>\r\n\r\n<p style="text-align:justify"><img alt="Inside HAN 2 [1024x768]" class="alignnone size-full wp-image-3299" src="http://blog.itviec.com/wp-content/uploads/2014/08/Inside-HAN-2-1024x768-e1407150988857.jpg" style="border-bottom-left-radius:4px; border-bottom-right-radius:4px; border-top-left-radius:4px; border-top-right-radius:4px; border:1px solid rgb(233, 233, 233); box-sizing:border-box; height:auto; max-width:100%; padding:4px; width:1021px" /></p>\r\n\r\n<p style="text-align:justify"><strong>LÃ m tháº¿ nÃ o Ä‘á»ƒ trá»Ÿ thÃ nhÂ </strong><strong>má»™t</strong><strong>Â Tech Expert?</strong></p>\r\n\r\n<p style="text-align:justify">Tech Expert lÃ  ngÆ°á»i Ä‘Ã£ phÃ¡t triá»ƒn kháº£ nÄƒng ká»¹ thuáº­t cá»§a mÃ¬nh á»Ÿ trÃ¬nh Ä‘á»™ cao trong má»™t chuyÃªn mÃ´n nháº¥t Ä‘á»‹nh. Ráº¥t nhiá»u cÃ´ng ty IT cáº§n Ä‘iá»u nÃ y.</p>\r\n', 'Gameloft l&agrave; má»™t c&aacute;i t&ecirc;n lá»›n trong lÄ©nh vá»±c n&agrave;y vá»›i hÆ¡n má»™t triá»‡u lÆ°á»£t game táº£i vá» h&agrave;ng ng&agrave;y tr&ecirc;n to&agrave;n tháº¿ giá»›i. Gameloft Viá»‡t Nam c&oacute; vÄƒn ph&ograve;ng táº¡i Th&agrave;nh phá»‘ Há»“ Ch&iacute; Minh, Ä&agrave; Náºµng v&agrave; H&agrave; Ná»™i vá»›i hÆ¡n 1,600 nh&acirc;n vi&ecirc;n.', 'Phonegap', 1, 1, '1407940609.jpg', '2014-08-13 21:36:49', NULL, '3-lyacute-do-bn-khocircng-nht-thit-phi-tr-thagravenh-mt-manager', 1);

-- --------------------------------------------------------

--
-- Table structure for table `post_category`
--

CREATE TABLE IF NOT EXISTS `post_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `post_category`
--

INSERT INTO `post_category` (`id`, `title`, `slug`) VALUES
(1, 'iOS', 'ios'),
(2, 'Sencha', 'sencha'),
(3, 'Hardware', 'hardware'),
(4, 'Ubuntu', 'ubuntu'),
(5, 'Linux', 'linux'),
(6, 'Linux', 'linux'),
(7, 'Rasberry pi', 'rasberry-pi'),
(8, 'Android', 'android');

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE IF NOT EXISTS `post_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`id`, `title`, `slug`) VALUES
(1, 'Phonegap', 'phonegap'),
(2, 'Ubuntu', 'ubuntu'),
(3, 'Rasberry pi', 'rasberry-pi'),
(4, 'Android', 'android'),
(5, 'Hardware', 'hardware'),
(6, 'Linux', 'linux'),
(7, 'iOS', 'ios'),
(8, 'Phalcon', 'phalcon');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phonenumber` varchar(255) DEFAULT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `interest` varchar(255) DEFAULT NULL,
  `about_me` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `fb_avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_firstname` (`firstname`),
  KEY `fki-profile-users-user_id-id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `user_id`, `firstname`, `lastname`, `phonenumber`, `occupation`, `website`, `interest`, `about_me`, `avatar`, `creation_date`, `modified_date`, `fb_avatar`) VALUES
(1, 1, 'Geekup', 'Teamsite', '99421321312', 'dsjfnvdjf', 'fdsgvdfgv', 'werfjwdsf', 'fdsgvdcx', '', '2014-08-08 06:25:33', '2014-08-11 08:28:02', NULL),
(2, 4, 'Geekup', 'Boss', NULL, NULL, NULL, NULL, NULL, '1407556225.jpg', '2014-08-08 08:15:39', '2014-08-13 22:05:47', 'https://graph.facebook.com/1454335281517880/picture?type=large'),
(3, 5, 'Ngan', 'Nguyen', NULL, NULL, NULL, NULL, NULL, NULL, '2014-08-08 08:16:31', NULL, NULL),
(4, 6, 'tuoi', 'nguyen', '0933100418', NULL, NULL, 'Sprort', 'Quality Controll', '', '2014-08-08 10:02:08', '2014-08-13 16:15:00', NULL),
(5, 7, 'Nguyá»…n', 'VÄƒn', NULL, NULL, NULL, NULL, NULL, NULL, '2014-08-08 10:29:09', NULL, NULL),
(6, 8, 'Ngan', 'Ngueyn', NULL, NULL, NULL, NULL, NULL, NULL, '2014-08-08 10:31:04', NULL, NULL),
(7, 9, 'Khanh', 'Phan', NULL, NULL, NULL, NULL, NULL, NULL, '2014-08-08 11:48:19', NULL, NULL),
(8, 2, 'Vi', 'Nguyen', '0979316629', NULL, NULL, 'hardware', NULL, '', '2014-08-08 14:02:02', NULL, NULL),
(9, 10, 'geek', 'up', NULL, NULL, NULL, NULL, NULL, NULL, '2014-08-08 23:22:33', NULL, NULL),
(10, 11, 'Ngan', 'Nguyen', NULL, NULL, NULL, NULL, NULL, NULL, '2014-08-09 04:47:25', NULL, NULL),
(11, 12, 'Ngan', 'Nguyen', NULL, NULL, NULL, NULL, NULL, NULL, '2014-08-09 04:51:57', NULL, NULL),
(12, 13, 'Rainy', 'Nguyen', NULL, NULL, NULL, NULL, NULL, NULL, '2014-08-09 04:56:25', NULL, NULL),
(13, 14, 'Rainy', 'Nguyá»…n', NULL, NULL, NULL, NULL, NULL, NULL, '2014-08-09 13:17:58', NULL, 'https://graph.facebook.com/799879753397588/picture?type=large');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `deleted` int(1) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `public` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `name`, `created_by`, `deleted`, `creation_date`, `modified_date`, `public`) VALUES
(1, 'Test project', 1, 0, '2014-07-10 19:34:16', '2014-07-10 19:34:16', 0),
(2, 'Ping2Buzz', 1, 0, '2014-08-08 23:01:35', '2014-08-08 23:01:35', 0),
(3, 'vnVitamin', 1, 0, '2014-08-08 23:01:48', '2014-08-08 23:01:48', 0),
(4, 'WifiSharing', 1, 0, '2014-08-08 23:01:58', '2014-08-08 23:01:58', 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_member`
--

CREATE TABLE IF NOT EXISTS `project_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-project_member-project-project_id-id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `type` varchar(10) NOT NULL,
  `undeletable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `is_default`, `type`, `undeletable`) VALUES
(1, 'Admin', 'Administrator.', 0, 'admin', 1),
(2, 'User', 'Default user role.', 1, 'user', 1),
(3, 'Guest', 'Guest role.', 0, 'guest', 1),
(4, 'Content_Manager', 'Content Manager', 0, 'user', 0),
(5, 'Member', 'Member', 0, 'user', 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `name` varchar(60) NOT NULL,
  `value` varchar(250) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`name`, `value`) VALUES
('system_default_language', 'en'),
('system_theme', 'default'),
('system_title', 'GEEKUp');

-- --------------------------------------------------------

--
-- Table structure for table `subscribe`
--

CREATE TABLE IF NOT EXISTS `subscribe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `social_id` varchar(255) DEFAULT NULL,
  `activate_key` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_username` (`username`),
  KEY `ix_email` (`email`),
  KEY `fki-users-roles-role_id-id` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `username`, `password`, `email`, `creation_date`, `modified_date`, `provider`, `social_id`, `activate_key`, `status`) VALUES
(1, 1, 'admin', '$2a$08$BEiupvXiPSwwez59p2M.cuq.JkcZRU6yT32BGUSvC/wr.7ygPc4DS', 'admin@geekup.dev', '2014-06-26 09:56:06', NULL, NULL, NULL, NULL, 1),
(2, 2, 'test', '$2a$08$qqtT/kQTrY28lofdXqkvEe/XM5XtyPQkAVMDC72H/QOiIsIvaF/Ly', 'ngtrieuvi92@gmail.com', '2014-07-10 10:51:11', '2014-08-08 13:50:27', NULL, NULL, NULL, 1),
(3, 5, 'ngtrieuvi92', '$2a$08$c0JWQsWz8Py7FNtoTrckpOplGv7nET801Ngh4a.U/KYsixkNIHEa6', 'ngtrieuvi92@outlook.com', '2014-08-05 07:13:16', '2014-08-06 22:50:55', NULL, NULL, NULL, 1),
(4, 2, 'Geekup', '$2a$08$ABBj7NLTmTxHWWzVngSwn.rJhvKxovFK1uSoDFdZLC53QRCsiDuR2', 'geekup.vn@gmail.com', '2014-08-08 08:15:39', '2014-08-08 23:21:38', '', '1454335281517880', NULL, 1),
(5, 2, 'Ngan', '$2a$08$BEiupvXiPSwwez59p2M.cuq.JkcZRU6yT32BGUSvC/wr.7ygPc4DS', 'kukuoc204@gmail.com', '2014-08-08 08:16:31', NULL, '', '272425159626829', NULL, 1),
(6, 5, 'sunny', '$2a$08$BEiupvXiPSwwez59p2M.cuq.JkcZRU6yT32BGUSvC/wr.7ygPc4DS', 'tuoi.nt@geekup.vn', '2014-08-08 10:02:08', '2014-08-08 22:54:35', NULL, NULL, NULL, 1),
(7, 5, 'Nguyá»…n', '$2a$08$BEiupvXiPSwwez59p2M.cuq.JkcZRU6yT32BGUSvC/wr.7ygPc4DS', 'vansunny12@gmail.com', '2014-08-08 10:29:09', '2014-08-08 22:54:24', '', '902493976433089', NULL, 1),
(8, 5, 'rainy', '$2a$08$f1Gyg8fvNYDAV7KRyBX0FeIqQxPLg/uNLhugu6Yrc2Qx69lX8Byfu', 'ngmng@gmail.com', '2014-08-08 10:31:04', '2014-08-09 03:02:54', NULL, NULL, NULL, 1),
(9, 2, 'kid1612', '$2a$08$2cdNt54j2X0ZtLbQHpqIrOrntdV00xN7n7dB8qEG2XKXU2P8.N1sa', 'khanhphan1612@gmail.com', '2014-08-08 11:48:19', NULL, NULL, NULL, NULL, 1),
(10, 2, 'geekup1', '$2a$08$wbB2r0KagKWzjKGqKcvad.y1cpTxydxMcW8.xmVbbOqZNgydHCp8y', 'geekup@outlook.com', '2014-08-08 23:22:33', NULL, NULL, NULL, NULL, 1),
(11, 2, 'ngannguyen', '$2a$08$jbqrQVOtaouYHYz39s71CehJjnxlviw/O72zg/4BK7bN9PS0i/N.G', 'ngmng2d04@gmail.com', '2014-08-09 04:47:25', NULL, NULL, NULL, 'fd5bd75c051aa904b39eb07425298c67', 0),
(12, 2, 'rainynguyem', '$2a$08$ZMAgzgYUXYbroG.qqyBKZerdlIvBbQkpCyeiUm2WZtLgtV9Ofy/PG', 'ngmng20dsf@gmail.com', '2014-08-09 04:51:57', '2014-08-09 05:03:34', NULL, NULL, '3ef59d757cb33709f6a31430f9259c00', 1),
(13, 2, 'kukuoc', '$2a$08$sb1cIguCf3RM4z.8u.H2.O4DyaP4dsT2DkJPHOuuJPH7TAHQbGJ1e', 'ngmng204@gmail.com', '2014-08-09 04:56:25', '2014-08-09 05:09:43', NULL, NULL, '891809d7c141079ac4c82e52849b136c', 1),
(14, 2, 'ngmng204@yahoo.com', NULL, 'ngmng204@yahoo.com', '2014-08-09 13:17:58', NULL, '', '799879753397588', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_reset_password_request`
--

CREATE TABLE IF NOT EXISTS `user_reset_password_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `reset` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-user_reset_password_request-users-user_id-id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `user_reset_password_request`
--

INSERT INTO `user_reset_password_request` (`id`, `creation_date`, `modified_date`, `user_id`, `token`, `email`, `reset`) VALUES
(44, '2014-07-31 06:06:13', '2014-07-31 06:14:46', 2, '5dffa53fd0232131bfd5ae7aedd430ae', 'ngtrieuvi92@gmail.com', 1),
(45, '2014-07-31 09:50:54', NULL, 2, '099a1aa3410140bff60b3074dd4661c8', 'ngtrieuvi92@gmail.com', 0),
(46, '2014-08-02 04:08:58', '2014-08-02 04:09:41', 2, '6a2917a6196be149585696978d0fe57a', 'ngtrieuvi92@gmail.com', 1),
(47, '2014-08-08 13:18:42', NULL, 8, 'e14796f44c224706f8c5c78de5a5eabf', 'ngmng204@gmail.com', 0),
(48, '2014-08-08 13:37:27', '2014-08-08 13:50:27', 2, '575879ddda5d60a732e1b624bddfd648', 'ngtrieuvi92@gmail.com', 1),
(49, '2014-08-08 13:38:00', NULL, 8, 'f20d4ca5433b682f8846bd1ba251ad37', 'ngmng204@gmail.com', 0),
(50, '2014-08-08 13:43:02', NULL, 8, '8a876e7748973f650b86752496259e66', 'ngmng204@gmail.com', 0),
(51, '2014-08-08 13:45:08', NULL, 2, 'c5b64fe20ffb25753752caed7e7e9b06', 'ngtrieuvi92@gmail.com', 0),
(52, '2014-08-08 13:48:38', NULL, 2, '4fcc52404f635418902d748609544385', 'ngtrieuvi92@gmail.com', 0),
(53, '2014-08-08 13:56:30', NULL, 8, '1d71369dbc2ae734e5cf1b2e711def8f', 'ngmng204@gmail.com', 0),
(54, '2014-08-08 22:19:47', NULL, 4, 'fe8ca6f4312da830150955f416072317', 'geekup.vn@gmail.com', 0),
(55, '2014-08-08 23:20:44', NULL, 2, '72cee0b40e99f74cfd4817007380ff4f', 'ngtrieuvi92@gmail.com', 0),
(56, '2014-08-08 23:21:21', '2014-08-08 23:21:38', 4, '241eb027e506f970102e1231858f837a', 'geekup.vn@gmail.com', 1),
(57, '2014-08-09 03:02:07', '2014-08-09 03:02:54', 8, '55e4afb0daa7853fe2cbe6537b1e5c47', 'ngmng204@gmail.com', 1),
(58, '2014-08-09 04:52:51', NULL, 12, '3a6175adc5009f29694017f2ab514011', 'ngmng204@gmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE IF NOT EXISTS `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `module` varchar(64) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_paginated` tinyint(1) NOT NULL,
  `is_acl_controlled` tinyint(1) NOT NULL,
  `admin_form` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `widgets`
--

INSERT INTO `widgets` (`id`, `name`, `module`, `description`, `is_paginated`, `is_acl_controlled`, `admin_form`, `enabled`) VALUES
(1, 'HtmlBlock', 'core', 'Insert any HTML of you choice', 0, 1, 'action', 1),
(2, 'Menu', 'core', 'Render menu', 0, 1, '\\Core\\Form\\Admin\\Widget\\Menu', 1),
(3, 'Header', 'core', 'Settings for header of you site.', 0, 1, '\\Core\\Form\\Admin\\Widget\\Header', 1),
(4, 'Blog', 'blog', 'Blog widget.', 1, 0, 'action', 1),
(5, 'Footer', 'core', 'Footer Page', 1, 0, 'action', 1),
(6, 'Sidebar', 'blog', 'Sidebar widget.', 1, 0, 'action', 1);

-- --------------------------------------------------------

--
-- Table structure for table `workshop`
--

CREATE TABLE IF NOT EXISTS `workshop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-workshop-project-project_id-id` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `workshop`
--

INSERT INTO `workshop` (`id`, `name`, `url`, `start_date`, `end_date`, `tags`, `project_id`, `description`, `status`, `thumbnail`) VALUES
(1, 'fdsfsdfsdfsd', 'fdsfsd', '2014-09-08', '2014-09-08', 'fdsfdsfsd', 1, 'fdsfdsfsd', 3, NULL),
(2, 'ping2buzz_backend', 'ping2buzz_backend', '2014-09-08', '2014-09-08', 'ping2buzz_b', 1, 'ping2buzz_backend', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `work_log`
--

CREATE TABLE IF NOT EXISTS `work_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `issue_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `minute` int(11) NOT NULL,
  `type` int(1) DEFAULT NULL,
  `message` text,
  `creation_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fki-work_log-issue-issue_id-id` (`issue_id`),
  KEY `fki-work_log-project-project_id-id` (`project_id`),
  KEY `fki-work_log-users-user_id-id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `work_log`
--

INSERT INTO `work_log` (`id`, `project_id`, `issue_id`, `user_id`, `date`, `minute`, `type`, `message`, `creation_date`, `modified_date`) VALUES
(1, 1, NULL, 1, '2014-07-12', 60, 1, NULL, '2014-07-12 07:42:13', '2014-07-12 07:42:13'),
(2, 1, NULL, 1, '2014-09-08', 360, 1, 'fixbug teamsite', '2014-08-08 14:11:34', '2014-08-08 14:11:34'),
(3, 1, NULL, 1, '2014-09-08', 360, 1, NULL, '2014-08-08 22:03:46', '2014-08-08 22:03:46'),
(4, 1, NULL, 7, '2014-09-08', 40, 1, NULL, '2014-08-08 22:58:27', '2014-08-08 22:58:27'),
(5, 4, NULL, 8, '2014-10-08', 300, 1, 'Login form ', '2014-08-08 23:05:10', '2014-08-08 23:05:10'),
(6, 1, NULL, 8, '2014-11-08', 40, 1, NULL, '2014-08-08 23:06:47', '2014-08-08 23:06:47'),
(7, 1, NULL, 7, '2014-09-08', 40, 1, NULL, '2014-08-08 23:13:44', '2014-08-08 23:13:44'),
(8, 1, NULL, 1, '2014-09-08', 100, 1, NULL, '2014-08-09 00:07:46', '2014-08-09 00:07:46'),
(9, 4, NULL, 6, '1970-01-01', 2, 1, 'Verify', '2014-08-13 15:56:11', '2014-08-13 15:56:11');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `access`
--
ALTER TABLE `access`
  ADD CONSTRAINT `fk-access-roles-role_id-id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `fk-comment-post-post_id-id` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `fk-comment-users-user_id-id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `fk-content-pages-page_id-id` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`),
  ADD CONSTRAINT `fk-content-widgets-widget_id-id` FOREIGN KEY (`widget_id`) REFERENCES `widgets` (`id`);

--
-- Constraints for table `issue`
--
ALTER TABLE `issue`
  ADD CONSTRAINT `fk-issue-project-project_id-id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `fk-issue-users-assigned_user-id` FOREIGN KEY (`assigned_user`) REFERENCES `users` (`id`);

--
-- Constraints for table `language_translations`
--
ALTER TABLE `language_translations`
  ADD CONSTRAINT `fk-language_translations-languages-language_id-id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `fk-menu_items-menus-menu_id-id` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`),
  ADD CONSTRAINT `fk-menu_items-menu_items-parent_id-id` FOREIGN KEY (`parent_id`) REFERENCES `menu_items` (`id`);

--
-- Constraints for table `package_dependencies`
--
ALTER TABLE `package_dependencies`
  ADD CONSTRAINT `fk-package_dependencies-packages-dependency_id-id` FOREIGN KEY (`dependency_id`) REFERENCES `packages` (`id`),
  ADD CONSTRAINT `fk-package_dependencies-packages-package_id-id` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `fk-post-users-author_id-id` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk-profile-users-user_id-id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `project_member`
--
ALTER TABLE `project_member`
  ADD CONSTRAINT `fk-project_member-project-project_id-id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk-users-roles-role_id-id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_reset_password_request`
--
ALTER TABLE `user_reset_password_request`
  ADD CONSTRAINT `fk-user_reset_password_request-users-user_id-id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `workshop`
--
ALTER TABLE `workshop`
  ADD CONSTRAINT `fk-workshop-project-project_id-id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

--
-- Constraints for table `work_log`
--
ALTER TABLE `work_log`
  ADD CONSTRAINT `fk-work_log-issue-issue_id-id` FOREIGN KEY (`issue_id`) REFERENCES `issue` (`id`),
  ADD CONSTRAINT `fk-work_log-project-project_id-id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `fk-work_log-users-user_id-id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

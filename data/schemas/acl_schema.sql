# CREATE TABLE roles (name varchar(150) not null, description text, primary key(name));
CREATE TABLE access_list (roles_name varchar(150) not null, resources_name varchar(150) not null, access_name varchar(150) not null, allowed int(3) not null, primary key(roles_name, resources_name, access_name));
CREATE TABLE resources (name varchar(150) not null, description text, primary key(name));
CREATE TABLE resources_accesses (resources_name varchar(150) not null, access_name varchar(150) not null, primary key(resources_name, access_name));
CREATE TABLE roles_inherits (roles_name varchar(150) not null, roles_inherit varchar(150) not null, primary key(roles_name, roles_inherit));

INSERT INTO `access_list` VALUES('Admin', '*', '*', 1);
